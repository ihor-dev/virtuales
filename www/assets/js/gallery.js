var gallery_id = "";
//Галерея-------------!
/**
 * Вставляет контент в редактор
 */
function tinyInsertContent(html){
    tinyMCE.execCommand("mceInsertContent",0,html);
}

//-=========ВКЛАДКА:АЛЬБОМЫ============-


//Добавить альбом
function add_gallery(){
    $("#add_gallery button").click(function(){
        var name = $("input[name=gallery_name]").val();

        if(name == "" || name == undefined){
            $(".f-input-help").html("Поле должно быть заполнено!").css({
                "color":"red"
            });
            $("input[name=gallery_name]").focus();
            return false;
        }else{
            $(".f-input-help").html("");
        }
        

        $.ajax({
            url     : "/ajax_send/gallery_add",
            type	: 'POST',
            async	: true,
            data	: "name="+name,
            success: function(data){
                if(data==1){
                    message('Новый альбом добавлен!',"green");
                    $("input[name=gallery_name]").val("").focus();
                    get_all_galleries();
                }
            }
        })
    });
}

//загрузить все альбомы
function get_all_galleries(){
    $.ajax({
        url     : "/ajax_send/gallery_get-all",
        type	: 'POST',
        async	: true,
        dataType: "json",
        success: function(data){
            $(".galleries-items").html("");
            $.each(data.galleries,function(i,k){
                var content = '<div class="g-row items no_left_margin" rel="'+k.id+'">';
                content += '<div class="g-5 g-ie"><input type="text" class="g-4" rel="'+k.id+'" name="gr_name" value="'+k.name+'"/></div>';
                content += '<div class="g-05 g-ie"><span class="f-bu del_gallery" rel="'+k.id+'">X</span></div>';
                content += '</div>';//
                $(".galleries-items").append(content);
            });
            
            //инициализация обновления альбома
            gallery_upd();
            
            //Инициализация удаления альбома
            gallery_del();                 
        }
    })
}

//обновить альбом
function gallery_upd(){
    $("input[name=gr_name]").change(function(){
        var id = $(this).attr("rel");
        var name = $(this).val();
        var data = "id="+id+"&name="+name;
        $.ajax({
            url     : "/ajax_send/gallery_upd",
            type	: 'POST',
            async	: true,
            data	: data,
            success: function(){
                message("Альбом переименован!","green");
            }
        });
    });
}

function gallery_del(){
    $(".del_gallery").click(function(){
        if(confirm("Вы уверены, что хотите удалить альбом?") == false) return false;
        var id = $(this).attr('rel');
        $.ajax({
            url     : "/ajax_send/gallery_del",
            type	: 'POST',
            async	: true,
            data	: "id="+id,
            success: function(){
                message("Альбом успешно уделён!","green");
                $(".galleries-items .items[rel="+id+"]").hide();
            }
        });
        
    });
}
//___________________________________________________//

//-=========ВКЛАДКА:ГАЛЕРЕЯ============-

//Формирования галлерей с картинками в слайдере
function get_galleries(){
    $.ajax({
        url     : "/ajax_send/gallery_get-galleries/",
        type	: 'POST',
        async	: true,
        dataType: "json",
        success: function(data){
            var gal = '';
            var img = '';
            $("#galleries").html("");
            $.each(data.data, function(i,k){
                gal = '<div class="g-row galleries no_left_magrin" rel="'+i+'">'+k.gallery.name+'</div>';
                $("#galleries").append(gal);
                
                //div изображения
                $("#galleries").append("<div class='g-row images no_left_margin hide' rel='"+i+"'></div>");
                //изображения
                $.each(k.images, function(l,m){
                    
                    img = '<div class="g-row no_left_margin img" rel="'+m.id+'">';
                    img+= '<div class=g-5 g-ie>';
                    img+='<img src="/assets/upload/mini/'+m.file+'"/>';
                    img+='<span class="f-bu-ie insert_img" path="'+m.file+'">Вставить</span>';
                    img+='<span class="del_img f-bu-ie" rel="'+m.id+'">X</span>';
                    img+-'</div></div>';
                    $(".images[rel="+i+"]").append(img);
                });
                
            });
            //инициализация слайдера
            slider("galleries", "images", "rel");
            //инициализация удаления картинки
            delete_image();
           
        }
    });
}

//добавить изображение в редактор
function insert_image(){  
    $(".insert_img").live('click', function(){
        var img = $(this).attr("path");
        var fulname = "<img src='/assets/upload/"+img+"'/>";
        tinyInsertContent(fulname);
        message("Изображение добавлено!","green");
    });
}

//удалить изображение
function delete_image(){
    $(".del_img").live('click',function(){
        if(confirm("Удалить изображение?")==false) return false;
        var id = $(this).attr('rel');
        $.ajax({
            url     : "/ajax_send/gallery_del-img",
            type	: 'POST',
            async	: true,
            data        : "id="+id,
            success: function(data){
                $("div.img[rel="+id+"]").hide();
                message("Изображение удалено","green");
            }
        });
    });
}
//___________________________________________________//

//-=========ВКЛАДКА:ЗАГРУЗИТЬ============-
//Все галлереи для списка галлерей в загрузке изображения
function all_galleries_p1(){
    $.ajax({
        url     : "/ajax_send/gallery_get-all",
        type	: 'POST',
        async	: true,
        dataType: "json",
        success: function(data){
            var content = "<select name='album' class='g-4'>";
            $.each(data.galleries, function(i,k){
                content += "<option value='"+k.id+"'>"+k.name+"</option>";
            });
            content +="</selecet>";
            $("#select_album").append(content);
            uploadify_init();
        }
    });
}
//загрузчик файлов галереи
//function uploadify_init(){
//    //перед инициализацией удаляю старый аплоадиф
//    if($("#uploadifyUploader").width() != null){      
//        $("#uploadify").uploadify('destroy');
//    }
//   
//    var gallery_id = $("select[name=album]").val();  
//   
//    $("#uploadify").uploadify({
//        'swf'       : '/assets/js/upl/uploadify.swf',
//        'uploader'         : '/ajax_send/gallery_add_file/'+gallery_id,
//        'cancelImg'      : '/assets/images/x.png',
//        'folder'         : 'uploads',
//        'queueID'        : 'selected-files',
//        'auto'           : false,
//        'multi'          : true,
//        'onUploadStart'  : function(file){
//        //$("#uploadify").uploadify('settings', 'uploader', '/ajax_send/gallery_add_file/'+ $("select[name=album]").val() ); 
//            
//        },
//        'onComplete'   : function(event,queueID,fileObj,response,data) {
//            message("файл загружен!", "green");
//            $('#response').append(response);
//            
//        //update_file_list();
//        }
//    });
//}
function uploadify_init(){
  
    var gallery_id = $("select[name=album]").val();  
    manualuploader = new qq.FineUploader({
        element: $('#uploadify')[0],
        request: {
            endpoint:  '/ajax_send/gallery_add_file/'+gallery_id
        },
        text: {
            uploadButton: 'Файл',
            cancelButton: 'удалить',
            dragZone: "Перетащите сюда файлы и отпустите!"
        },
        autoUpload: false,
        callbacks: {
            onComplete: function(id, fileName, responseJSON) {
                setTimeout(function(){
                    $(".qq-upload-success").fadeOut(300);
                }, 2000);
            }
        }
    });
}



//__________________________________________________//

/**
 * Галлерея. инициализация.
 * Когда загружено в модальное окно, инициализация всех жс
 */
function galleryInit(){
   
    $.get(
        "/ajax_getmodal/gallery/",
        function(result){
            $("#ajax").html(result);
            
            $("#ajax").modal({
                closeClass: 'simplemodal-overlay',
                minHeight:569,
                onClose: function (dialog) {
                    dialog.data.fadeOut(300, function () {
                        dialog.container.hide('slow', function () {                        
                            $.modal.close();                      
                        });
                    });
                }
            });// END MODAL
            //инициал.тулбара
            toolbarInit();
            
            
            all_galleries_p1();
            
            //инициал. кнопки добавления галереи
            add_gallery();
            
            //Когда выбрана вкладка с альбомами
            $(".bar-button[rel=3]").click(function(){
                get_all_galleries();
            });
            
            //Когда выбрана вкладка с галереями
            $(".bar-button[rel=2]").click(function(){
                get_galleries();
                
            //инициализация слайдера
            // slider("galleries", "images", "rel");
                
                
            });
        });
}

$(document).ready(function(){

    //-------- GALLERY ------------
    $("#insert").click(function(){
        galleryInit();
        
    }); 
    
    //кнопка вставить изображение
    insert_image();
    
    //загрузка выбранных файлов
    $('#uploadify-send').live('click',function(){     
        manualuploader.uploadStoredFiles();
    });
    
    //смена галлереи
    $('select[name=album]').live('change',function(){     
        uploadify_init();
        
    });
    

});

