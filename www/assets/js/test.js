//       JS для прохождения теста
//------------------------------------//

//обрезает первый символ для ключей
function trimm(str){
    if(str==null) return false;
    return str.substring(1, str.length);
}

/*Ajax-запрос вопроса и вариантов ответа  */
function get_quetion(){

    $.ajax({
        url: '/ajax_send/quetion/',
        async: false,
        cache: false,
        type: 'POST',
        dataType: "json",
        success: function(data){           
            if(data.end != "1"){
                //если есть вопросы
                
                $("#qw").append("<p id='quetion'>Вопрос: "+data.content+"</p>");
                $("input[name=id]").val(data.id);

                $("#qw").append("<p>Варианты ответа: </p>");
                $.each(data.answers, function(id, val){
                    $("#qw").append("<p class='var_answ'><label><input class='w20' type='checkbox' value='"+id+"'>"+val+"</label></p>");
                });
            }else{
                //если тест окончен
                $("#qw").append("<p>Тест окончен</p>");
                $('.send').hide();
                $.ajax({
                    url:'/ajax_send/point',
                    cache: false,
                    success: function(data){
                        $("#qw").append("<p id='mark'>Ваша оценка:"+ data +" б.</p>");
                        setTimeout(function(){location.href="/user_points"}, 5000);
                    }
                })//end get point
            }
            
        }
    }); //  END-----!
}

/* ----------- Скрипты для теста ---------*/
$(document).ready(function(){
    
    $("#begin-test").click(function(){
        link = $(this).attr('href');
        location.replace(link);
    });
    
   //Если не страница тестирования - не отправлять запросов
    if($("#begin-test").attr('href') !== undefined)return false;
    
    /* USER: Ajax-запрос вопроса и вариантов ответа  */
    get_quetion();
    
    $(".send").mouseup(function(){
        //очищяю
        $("input[name=ukeys]").val("");
        
        //перебираю каждый вариант ответа, и в скрытое поле через пробел
        //записываю ответы, которые отметил юзер
        $.each($("#qw p input[type=checkbox]"), function(i,k){
            var ukeys    = $("input[name=ukeys]");
            var ukeysVal = $("input[name=ukeys]").val();
            
            if( $(k).attr("checked") == "checked"){
                ukeys.val(ukeysVal + " "+ $(k).val());              
            }
           
        });
        //clear user keys
        $("input[name=ukeys]").val( trimm($("input[name=ukeys]").val()) );
        
        var id   = $("input[name=id]").val();
        var keys = $("input[name=ukeys]").val();
        
        $.ajax({
            url: '/ajax_send/answer/',
            async: false,
            cache: false,
            type: 'POST',
            data: 'id='+id+'&keys='+keys,
            success: function(data){
                $("#qw").html("");
                get_quetion();
            }
        })
    });

});
