
function get_users() {
    do_ajax_json('/ajax_send/users-online', null, function(res) {
        $(".pc_item").remove();
        $(".pc_title").html(" Сейчас на сайте: "+res.count+" человек");//на сайте
        $.each(res.u, function(i, k) {

            if (k.group == null || k.group == "") {
                var group = "нет";
            } else {
                var group = k.group;
            }
            create_block(k.uid, k.name, group, k.photo);
        });
        screenshotPreview();
    });
}



//добавляет блок пользователя
function create_block(id, name, group, photo) {
    var big = photo.replace("-min","");
    var h = '<div class="pc_item">';
    h += '<h2>' + name + '</h2>';
    h += '<img class="screenshot" src="' + photo + '" alt="" rel="'+big+'"/>';
    h += '<p><strong>Группа: </strong>' + group + '<p>';
    h += '<div class="pc_more"></div></div>';

    $("#pc_content").prepend(h);    

    //При наведении
    $("#pc_content").find('.pc_item').hover(
            function() {
                $(this).addClass('selected');
            },
            function() {
                $(this).removeClass('selected');
            }
    ).bind('click', function() {
        //При клике по блоку        
        //window.open($(this).find('.pc_more').html());
    });
    //-----------------

}




$(document).ready(function() {
    //windows resize bug
    $(window).resize(function() {
        //$pc_panel.hide();
        window_w = $(window).width();
        window_h = $(window).height();
        buildPanel();
    });

    $("#panel-btn").click(function() {
        $pc_panel.stop().animate({'right': '0px'}, 300);
        window_w = $(window).width();
        window_h = $(window).height();
        //buildPanel();
        get_users();
    });

    //window width and height
    var window_w = $(window).width();
    var window_h = $(window).height();
    //the main panel div
    var $pc_panel = $('#pc_panel');
    //the wrapper and the content divs
    var $pc_wrapper = $('#pc_wrapper');
    var $pc_content = $('#pc_content');
    //the slider / slider div
    var $pc_slider = $('#pc_slider');
    //the element reference - reaching this element
    //activates the panel
    var $pc_reference = $('#pc_reference');

    var maxWidth, maxHeight, marginL;

    buildPanel();

    function buildPanel() {
        $pc_panel.css({'height': window_h + 'px'});
        hidePanel();
        //we want to display the items in a grid.
        //we need to calculate how much width and height
        //the wrapper should have.
        //we also want to display it centered, so we need to calculate
        //the margin left of the wrapper

        //First, lets see how much of height:
        //maxHeight = Math.floor((window_h-20)/135)*135;
        //20 => pc_titles height
        //135 => 125 of each items height plus its margin (10)
        maxHeight = Math.floor((window_h - 20) / 135) * 135;
        //maxWidth = Math.floor((window_w-35)/220)*220;
        //220 = item width + margins (left and right)
        maxWidth = Math.floor((window_w - 35) / 220) * 220;
        marginL = (window_w - maxWidth) / 2;
        $pc_wrapper.css({
            'width': maxWidth + 20 + 'px',
            'height': maxHeight + 'px',
            'margin-left': marginL + 'px'
        });


    }

    //the panel gets positioned out of the viewport,
    //and ready to be slided out!
    function hidePanel() {
        //165 => 20 pc_title + 120 item + margins
        $pc_panel.css({
            'right': -window_w + 'px',
            'top': window_h - 165 + 'px'
        }).show();
        try {
            //position the slider in the beginning
            slideTop();
        } catch (e) {
        }
        $pc_slider.hide();
        $pc_panel.find('.collapse')
                .addClass('expand')
                .removeClass('collapse');
    }

    //resets the slider by sliding it to the top
    function slideTop() {
        var total_scroll = $pc_content.height() - maxHeight;
        $pc_wrapper.scrollTop(0);
        $pc_slider.slider('option', 'value', total_scroll);

    }

    //when clicking on the expand button,
    //we animate the panel to the size of the window,
    //reset the slider and show it
    $pc_panel.find('.expand').bind('click', function() {
        var $this = $(this);
        $pc_wrapper.hide();
        $pc_panel.stop().animate({'top': '0px'}, 500, function() {
            $this.addClass('collapse').removeClass('expand');
            $pc_wrapper.show();
            slideTop();
            $pc_slider.show();

        });
    })

    //clicking collapse will hide the slider,
    //and minimize the panel
    $pc_panel.find('.collapse').live('click', function() {

        var $this = $(this);
        $pc_wrapper.hide();
        $pc_slider.hide();
        $pc_panel.stop().animate({'top': window_h - 165 + 'px'}, 500, function() {
            $pc_wrapper.show();
            $this.addClass('expand').removeClass('collapse');
        });
    });

    //clicking close will make the panel disappear
    $pc_panel.find('.closep').bind('click', function() {
        $pc_panel.stop().animate({'right': -window_w + 'px'}, 300, function() {
            hidePanel();
        });
    });

    //mouse over the items add class "selected"
    $pc_wrapper.find('.pc_item').hover(
            function() {
                $(this).addClass('selected');
            },
            function() {
                $(this).removeClass('selected');
            }
    ).bind('click', function() {
        window.open($(this).find('.pc_more').html());
    });
});