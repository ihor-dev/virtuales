// ------ СОЗДАНИЕ ПРАКТИЧЕСКОГО ЗАДАНИЯ -----//
function uploadify_ini() {

    var prid = $("input[name=pr_id]").val();
    $("#uploadify_pr").uploadify({
        'swf': '/assets/js/upl/uploadify.swf',
        'uploader': '/ajax_send/upload_files/' + prid,
        'cancelImg': '/assets/images/x.png',
        'folder': 'uploads',
        'queueID': 'selected-filess',
        'auto': false,
        'multi': true,
        'onComplete': function(event, queueID, fileObj, response, data) {
            message(response, "green");
            $('#response').append(response);
        }
    });
}


function jquploader_init() {
    var prid = $("input[name=pr_id]").val();
    manualuploader = new qq.FineUploader({
        element: $('#uploadify_pr')[0],
        request: {
            endpoint: '/ajax_send/upload_files/' + prid
        },
        text: {
            uploadButton: 'Файл'
        },
        autoUpload: false,
        callbacks: {
            onComplete: function(id, fileName, responseJSON) {

            }
        }
    });
}


/**
 * функция заменяет основные спецсимволы и вырезает остальные
 */
function special_chars(str) {
    if (str == null)
        return false;
    str = str.replace(/&nbsp;/g, ' ');
    str = str.replace(/&raquo;/g, '"');
    str = str.replace(/&laquo;/g, '"');
    str = str.replace(/(&\w+;)/g, '');

    return str;
}

function p_valid() {

    if ($("input[name=name]").attr('saved') == "0") {
        //no saved
        $("textarea, .upl-btn").addClass('dissabled');
        message("Сначала введите название!", "red");
        $("input[name=name]").focus();
    } else {
        $("textarea, .upl-btn").removeClass('dissabled');
    }
}
//сохранить или обновить практическое
function save_update_p() {
    var name = "name=" + $("input[name=name]").val();
    if ($("input[name=name]").attr('saved') == "1") {
        //update        
        var text = tinyMCE.get('text_editor').getContent();
        text = special_chars(text);
        var id = $("input[name=pr_id]").val();
        var data = name + "&text=" + text + "&id=" + id;

        do_ajax("/ajax_send/practic_update/", data, function(r) {
            message("Данные обновлены....", "green");
        });
    } else {
        //save
        do_ajax("/ajax_send/practic_save/", name, function(id) {
            $("input[name=pr_id]").val(id)
            message("Сохранено....", "green");

            //инициализация загрузчика для режима сохранения
            jquploader_init();
        });
    }
    $("input[name=name]").attr('saved', 1);
}


// --------------- END ---------------------- //    


$(document).ready(function() {

    // ------ СОЗДАНИЕ ПРАКТИЧЕСКОГО ЗАДАНИЯ -----//
    if ($("textarea#text_editor").val() != undefined) {
        //сбить маркер "сохранено"
        $("input[name=name]").attr('saved', 0);

        //проверка заполнения поля имя
        if ($("input[name=pr_id]").val() !== "")
            $("input[name=name]").attr('saved', 1);


        //инициализация Uploadify для режима обновления практ.задания
        if ($("input[name=pr_id]").val() !== "") {

            // uploadify_ini();
            jquploader_init();
        }


        //ДОБАВИТЬ ИМЯ
        $("input[name=name]").change(function() {
            save_update_p();

            p_valid()

        });

        //textarea, button hover
        $("textarea, .upl-btn").mouseover(function() {
            p_valid();
        });

        //textarea, button click
        $("textarea, .upl-btn").click(function() {
            p_valid();
        });

        //слайдер инициализация
        slider("upl-btn", "upl-area", 'rel');

        //загрузка выбранных файлов
        $('#uploadify-send_pr').click(function() {
            manualuploader.uploadStoredFiles();
        });

        $("button#save").click(function() {
            save_update_p();
        });



        //удалить файл
        $(".delfile").click(function() {
            if (window.confirm("Удалить файл?") == false)
                return false;

            var id = $(this).attr('rel');
            var data = "id=" + id;
            function res() {
                $('div[fileID=' + id + ']').hide(500);
                message("Файл удален!", "green");
            }
            do_ajax("/ajax_send/unlink_pr_file/", data, res);
        });



    }
    // ---------- END --------- //
    //         Практические -------//
    if ($("#add_practik").html() != undefined) {
        //новое задание
        $("#add_practik").click(function() {
            location.replace('/admin_practiks_add');
        });

        //удалить задание
        $(".delete").click(function() {
            if (window.confirm("Вы уверены, что хотите удалить практическое задание?") == false)
                return;
            var id = $(this).attr('pr_id');
            var data = "id=" + id;

            function result(res) {
                if (res == "1") {
                    $("div[rel=" + id + "]").slideUp(400);
                    message("Удалено!", "green");
                }
            }
            do_ajax("/ajax_send/practic_delete/", data, result);
        });

        //сортировка
        $("#sortable").sortable({
            //delay: 1000
        });
        $('#sort').click(function() {
            var ids ="";
            $.each($(".tests-list-item"), function(i,k){
                ids +=" "+$(k).attr('rel');                
            });
            var data = 'ids=' + $.trim(ids);
            do_ajax('/ajax_send/sort_practiks/',data,function(){alert('complete!')},1);
        });
    }


});

