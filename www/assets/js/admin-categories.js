//удаление группы
function delete_group(id){
    
    $.ajax({
        url     : "/ajax_send/cat_del",
        type	: 'POST',
        async	: true,
        data	: "id="+id,
        success: function(data){
            if(data == 1){
                $("tr[rel="+id+"]").fadeOut(500);            
            }
        }
    })
    
}
$(document).ready(function(){
    
    //Обновление данных группы(редакт)
    $("input[name=upd]").click(function(){
        var d = $(".p-1 :input").serialize();
        $.ajax({
            url     : "/ajax_send/cat_upd",
            type    : 'POST',
            async   : true,
            data    : d,
            success: function(data){
            }
        })
    });

    //Новая группа
    $("input[name=add]").click(function(){

        var name = $("input[name=NewName]").val();

        if(name == ""){
            $(".f-input-help").html("Поле должно быть заполнено!").css({
                "color":"red"
            });
            $("input[name=NewName]").focus();
            return false;
        }
       
        $.ajax({
            url     : "/ajax_send/cat_add",
            type	: 'POST',
            async	: true,
            data	: "name="+name,
            success: function(data){
                if(data==1){
                    $(".f-input-help").html('Новая категория добавлена! Можно добавить еще одну').css({
                        "color":"green"
                    });
                    $("input[name=NewName]").val("").focus();
                }
            }
        })
    });
       
                
});

