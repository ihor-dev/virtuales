$(document).ready(function(){
    $('.bar-button').click(function(){
        var type = $(this).attr('rel');
        filter(type);
    });
});

//фильтер контента - отображать/скрывать нужный контент
function filter(type){
    if(type == '0'){
        $('.practik-items').slideDown(400);
    }else{
        $('.practik-items').hide();
        $('div[type='+type+']').slideDown(400);
    }
    
}