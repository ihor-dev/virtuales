function toolbarInit(){
    $("div.p-1").show();
    $(".bar-button").hover(
        function(){
            $(this).addClass('bar-hover');
        },
        function(){
            $(this).removeClass('bar-hover');
        }
        );
    $(".bar-button").click(function(){
        $(".bar-button").removeClass('bar-active');
        $(this).addClass('bar-active');
        var id = $(this).attr('rel');
        $("div.page").hide();
        $("div.p-"+id).fadeIn(400);               
    });
}
$(document).ready(function(){

    //Toolbar
        toolbarInit();
    //End toolbar
});

