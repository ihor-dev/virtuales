//Повторение имени функции! Есть в скриптс! Может из-за этого не работать ажакс!
function do_ajax(url, data, myfunc){
    if(data!= null){
        var data = data
    }
    else{
        var data = ""
    }
    
    $.ajax({
        url: url,
        type: "POST",
        async: false,
        data: data,
        success: function(){
            myfunc
        }
    });
    return true;
}

//очистка формы
function form_clear(){
    if($('#creater').html() == "1"){
        $(":input").val("");
        $(":checkbox").attr('checked',false);

        /* для ФФ: удалить все поля ввода */
        $(".variants input").each(function(){
            $(this).remove
        });
        /* для ФФ: создать только одно */
        $(".variants").html('<input class="g-4" type="text" name="variant[]"><input type="checkbox" name="true[]" value=""><br>');
    }
}

//запрос количества созданных вопросов в тесте
function count_quetions(){
    if( $("p.col-voprosov").html() != null){      
        $("p.col-voprosov").html("Вопросов в тесте: ");
        $.ajax({
            url: '/ajax_send/count-quetions/',
            async: true,
            type: 'POST',
            cache: false,
            success: function(data){
                $("p.col-voprosov").append(data);
            }
        });    
    }
}

//добавить поле
function add_field(type){
    if(type=='cr'){
        $(".variants").append('<input type="text" class="g-4" name="variant[]"/><input type="checkbox"  name="true[]"  /><br/>');      
    }else if(type=='ed'){
        var p = $("#new_field");
        var now  = new Date();
        var date = now.getHours()+"_"+ now.getMinutes()+"_" + now.getSeconds();
        
        //добавляю во временный блок формочку
        p.append('ДОбавить вариант ответа:');
        p.append('<p class="input-p newfield"><input type="text" class="g-4 answers" name="variant[]" rel="'+date+'" value=""/><input type="checkbox" name="true[]" value=""/><span class="delete-input" rel="'+date+'" title="Удалить вариант"></span><span class="f-bu send_added_field">Добавить</span></p>');
        //$(".variants").append('<p class="input-p"><input type="text" class="g-4 answers" name="variant[]" rel="'+date+'" value=""/><input type="checkbox" name="true[]" value=""/><input type="hidden" name="ansid[]" value="edded"/><span class="delete-input" rel="'+date+'" title="Удалить вариант"></span></p>');      
        
        
        delete_field();//инициализация кнопки удаления поля
        save_new_answer();//инициализация кнопки сохранения поля
    }
    
}

//сохранение на сервере нового варианта ответа/ Редактирование!
function save_new_answer(){
    $(".send_added_field").click(function(){
        qw_id   = $("input[name=quetion_id]").val();
        var content = $(".newfield input[type=text]").val();
        var key     = $(".newfield input[type=checkbox]").attr("checked") ? 1 : 0;
        $.ajax({
            url: "/ajax_send/add_new_answer/",
            data: "qw_id="+qw_id+"&content="+content+"&key="+key,
            type: "POST",
            async: false,
            cache: false,
            success: function(data){
                //Очищяю старые данные формы перед обновлением
                $("#new_field").html("");
                $(".variants").html("");
                load_quetions(qw_id, "0");              
               
            }
        });// -- end ajax
    });//-- end click
}

//Загрузка варинатов ответов и правильных ключей / РЕдактирование!
//var status - нужно ли обновлять поле вопроса?
function load_quetions(id, status){
    //Запрос вариантов ответа
    $.ajax({
        url: '/ajax_send/edit_get_answers/',
        async: false,
        type: 'POST',
        data: 'id='+id,
        dataType: "json",
        cache: false,
        success: function(data){
            
            //если нужно обновить вопрос
            if(status == "1"){
                $("textarea").val(data.content);                   
            }
            
            $.each(data.answers, function(i,k){
                $(".variants").append('<p class="input-p"><input type="text" class="g-4 answers" name="variant[]" rel="'+i+'" value="'+k+'"/><input type="hidden" name="ansid[]" value="'+i+'"/><input type="checkbox" name="true[]" value="'+i+'"/><span class="delete-input" rel="'+i+'" title="Удалить вариант"></span></p>'); 
            });
        }  
    });// end load data to form
 
    //запрос правильных ответов
    $.ajax({
        url: '/ajax_send/edit_quetion_keys/',
        async: false,
        type: 'POST',
        data: 'id='+id,
        dataType: "json",
        cache: false,
        success: function(data){                          
            $.each(data, function(i,k){
                $("input[rel="+i+"]").addClass('trueanswers');
            });
        }  
    });// end load data to form
                
}//-end function

//удаление поля
function delete_field(){
    $(".delete-input").mouseup(function(){
        var did = $(this).attr('rel');

        var data = "id="+did;
        do_ajax("/ajax_send/delete_answer/", data, "");
        $("p.input-p:has(span[rel="+did+"])").remove();
                    
    });
}


/* Скрипты для теста */
$(document).ready(function(){
    //очищяю форму
    form_clear();
    
    //созадние теста - добавить название тест
    $(".test-cr").mouseup(function(){
        var name = $("input[name=name]").val();
               
        $.ajax({
            url: '/ajax_send/test-cr/',
            async: false,
            type: 'POST',
            data: 'name='+name,
            success: function(data){
                if(data=="1"){
                    location.replace("/admin/test/create/");
                }
            }
        });
    });
    
    //количество вопросов
    count_quetions();
    
    //добавить поле
    $('.add-field').mouseup(function(){
        add_field('cr');
    })

   
    //отправить данные формы вопроса
    $('.test-add-answ').mouseup(function(){
        var answer = $("textarea[name=answer]").val();
        $('input:checkbox').each(function(i,k){
            $(this).val(i);              
        });
        var data = $(":input").serializeArray();
        do_ajax("/ajax_send/add_answer", data, "");
       
        //очищяю форму
        form_clear();
        
        //количество вопросов
        count_quetions();
    });
    
    //Сохранить тест
    $(".test-save").click(function(){
        if($("textarea[name=quetion]").val() !== undefined)
            $('.test-add-answ').trigger('mouseup');
        
        if( window.confirm("Закончить редактирование теста?") )
            location.replace("/admin/test/view");

    });
    
    
    //подсветка правильных вариантов ответов ответов
    if( $("#preview").html()=="1"){
        var id = $("input[name=test_id]").val();
        $.ajax({
            url: '/ajax_send/get_true_keys/',
            async: false,
            type: 'POST',
            data: 'id='+id,
            dataType: "json",
            success: function(data){
                $.each(data.keys, function(i,k){
                    $("#id-"+k).addClass('trueanswers');
                })
            }
        });
    }//----end
    
    
    $(".button-delete").mouseup(function(){
        if(! window.confirm("Удалить вопрос?") ) return false;

        var id =$(this).attr('rel');
              
        $.ajax({
            url: '/ajax_send/delete_quetion/',
            async: false,
            type: 'POST',
            data: 'id='+id,
            success: function(data){
               
            }
        });
        $("div.test-item:has(button[rel="+id+"])").fadeOut();
    });
    
    /* ----------------------------------------------------------------------------------------------------------*/    
    //РЕдактирование вопроса
    $(".button-edit").mouseup(function(){
        //id вопроса
        id = $(this).attr('rel');    
       
        
               
        $.get(
            "/ajax_getmodal/edit_quetion/",
            function(result){
                $("#ajax").html(result);
                
                $("input[name=quetion_id]").val(id);// ----ID ВОПРОСА!!!!-----------
                
                $("#ajax").modal({
                    closeClass: 'simplemodal-overlay',
                    minHeight:569,
                    onClose: function (dialog) {
                        dialog.data.fadeOut(300, function () {
                            dialog.container.hide('slow', function () {                        
                                $.modal.close();                      
                            });
                        });
                    }
                });// END MODAL         
                
                // Запрос вариантов ответа и правильных ключей!!!!!
                load_quetions(id,"1");
                
                //сохранение данных
                $(".edit_test-save").mouseup(function(){
                    //добавляю ид вопроса
                    $("input[name=quetion_id]").val(id);
                    //нумерую ключи
                    $('input:checkbox').each(function(i,k){
                        //если новое поле - пронумеровать, иначе оставить
                        if($(this).val() == "") $(this).val(i);              
                    });
                    
                    var data = $(":input").serializeArray();
                    
                    do_ajax("/ajax_send/save_edit_quetion/", data, "");
                    $.modal.close();
                });// end save;
                
                //удаление поля
                delete_field();
                
                //добавить поле
                $('.add-field').mouseup(function(){
                    add_field('ed');
                })               
           
            });// end get
    });



    //Панелька, которая вылазит при нажатии в редакторе тестов на названии теста
    $(".tests-list-item").click(
        function(){
            $(".test-navigation").hide(200);
            var id = $(this).attr('rel');
            $(".test-navigation[rel="+id+"]").show(100);
        }
        );
    
    //свойство теста
    $(".test-navigation a.test-attr").click(function(){
        id = $(this).attr('rel');
        $.ajax({
            url:"/ajax_getmodal/edit_test_info/",
            type	: 'POST',
            async	: true,
            cache:  false,
            success: function(result){
                $("#ajax").html(result).css({
                    'margin':'12px 0 0'
                });              
                
                //загружаю информацию в форму
                $.ajax({
                    url   : "/ajax_send/edit_get_test_info/",
                    type  : "POST",
                    async : "true",
                    cache : false,
                    data  : "id="+id,
                    dataType: 'json',
                    success: function(data){
                        $("#test-info input[name=name]").val(data.name);
                    }
                });
                //инициализация модалки
                $("#ajax").modal({
                    closeClass: 'simplemodal-overlay',
                    minHeight: 70,
                    minWidth: 780,
                    onClose: function (dialog) {
                        dialog.data.fadeOut(300, function () {
                            dialog.container.hide('slow', function () {                        
                                $.modal.close();                      
                            });
                        });
                    }
                });// END MODAL         
                
                //сохранение информации
                $("#test-info .test-cr").click(function(){
                    var name = $("#test-info input[name=name]").val();
                    if(name==""){
                        alert('заполните все поля');
                        return false;
                    }
                    
                    $.ajax({
                        url : "/ajax_send/edit_save_test_info/",
                        type  : "POST",
                        async : "true",
                        cache : false,
                        data  : "id="+id+"&name="+name,
                        success: function(data){
                            $.modal.close();
                        }
                    });
                });
            }
        });// end ajax
    });//свойства теста
    
    //удаление теста
    $(".test-delete").click(function(){
        id = $(this).attr('rel');

        if(confirm("Удалить тест??")){
            $.ajax({
                url : "/ajax_send/test_delete/",
                type  : "POST",
                async : "true",
                cache : false,
                data  : "id="+id,
                success: function(){
                    $("div.tests-list-item[rel="+id+"]").html('удалено').hide(600);
                }
            });
            
        }
    });

});//End document ready

