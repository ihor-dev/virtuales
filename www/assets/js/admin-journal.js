var uids = "";
var tids = "";
function get_checkbox_values() {
    uids = "";
    tids = "";
    $(":input[name=id]").each(function(i, k) {
        var checked = $(k).attr("checked");
        if (checked == "checked") {
            uids = uids + " " + $(k).val();
        }
    });
    $(":input[name=task]").each(function(i, k) {
        var checked = $(k).attr("checked");
        if (checked == "checked") {
            tids = tids + " " + $(k).val();
        }
    });
}



//Инициализация кнопки "все"
function button_init() {
//выделить все элементы
    $("span#checkall").click(function() {
        if ($("span#checkall").attr('rel') != "1") {
            $(":input[type=checkbox]").each(function(i, k) {
                $(k).attr("checked", true);
            })
            $("span#checkall").attr('rel', 1);
        } else {
            $(":input[type=checkbox]").each(function(i, k) {
                $(k).attr("checked", false);
            })
            $("span#checkall").attr('rel', 0);
        }

    });
}

//пересчет систем оценивания
function change_system(new_type) {
    $.each($(".c-mark"), function(i, k) {
        var mark = $(k).html();//оценка
        var cur_type = $(k).attr('mtype');//текущая система оценивания каждой оценки
        var result = 0;
        //из 12 в 5
        if (new_type == 5 && cur_type !== "5") {
            if (mark < 4) {
                result = 2;
            }
            else if (mark > 3 && mark < 7) {
                result = 3;
            }
            else if (mark > 6 && mark < 10) {
                result = 4;
            }
            else if (mark > 9 && mark < 13) {
                result = 5;
            }
            $(k).html(result).attr('mtype', 5).attr('old', mark);
        }//из 5 в 12
        else if (new_type == 12 && cur_type !== 12) {
            result = $(this).attr('old');
            $(k).html(result).attr('mtype', 12).attr('old', result);
        }
    });
}
$(document).ready(function() {
    $("select[name=group]").val("");
    $("select[name=mark-system]").val(12);

    $("select[name=mark-system]").change(function() {
        change_system($(this).val());
    });

    //Выбрана группа
    $("select[name=group]").change(function() {
        var grid = $(this).val();

        $("#journal-task").html("");

        if (grid !== "group") {
            do_ajax_json("/ajax_send/task-get-users/", "id=" + grid, function(data) {
                $("#tasks-users").html("<div class='g-4 table_header'>Список пользователей:</div>");
                $("#tasks-users").append("<div><span id='checkall' class='f-bu' title='Выделить всех'>Все</span></div>");
                button_init();
                $.each(data.data, function(i, k) {
                    $("#tasks-users").append("<div><input type='checkbox' name='id' value='" + k.uid + "'/>" + k.surname + " " + k.name + " </div>")
                });
                $("input[name=save]").attr('disabled', false);
            }); //-------users

            //tasks
            $("#journal-task").append('<div class="g-4 table_header">Список заданий:</div>');
            do_ajax_json("/ajax_send/get-gr_tasks/", "grid=" + grid, function(data) {
                $.each(data.ts, function(i, k) {
                    if (k.type == "lesson") {
                        img = '<img width="22px" src="/assets/images/icon/lessons.jpg">';
                    } else if (k.type == "test") {
                        img = '<img width="22px" src="/assets/images/icon/adm_test.png">';
                    } else {
                        img = '<img height="22px" src="/assets/images/icon/practik.jpg">';
                    }

                    $("#journal-task").append("<div><input type='checkbox' name='task' value='" + k.tid + "'/>" + img + " " + k.task_name + " </div>");
                });
            }); //---tasks       
        }
    }); //select

    //ЖУРНАЛ
    $("#get_journal").click(function() {
        get_checkbox_values();
        if (uids == "" || tids == "") {
            alert("Выбрать группу, пользователей и задания!");
            return false;
        }

        $("#journal").html("Загрузка...");

        var data = "uids=" + uids + "&tids=" + tids;
        do_ajax_json("/ajax_send/get_journal", data, function(res) {
            $("#journal").html("");
//----------------------отрисовка журнала-----------------------------
            var cols = 0;
            var rows = 0;
            var ts = new Array();
            //заглавие таблицы. темы
            $("#journal").append("<table id=jour-table><thead><tr><td>Фамилия И.О.</td>");
            $.each(res.jr.tasks, function(i, k) {
                $("tr").append("<td>" + k.name + "</td>");
                ts[i] = k.id;
                cols = i + 1;
            });
            $("#journal").append("</tr></thead><tbody>");


            //Столбик фамилий            
            $.each(res.jr.users, function(i, k) {
                $("table").append("<tr id='r" + k.id + "'><td>" + k.name + "</td>");
                rows = i + 1;
                //клеточки для оценок
                for (b = 0; b < cols; b = b + 1) {
                    $("tr#r" + k.id).append("<td class='c-mark' ind='" + ts[b] + "_" + k.id + "'>0</td>");
                }
                $("table").append("</tr>");
            });



            $("#journal").append("</tbody></table>");

            //расстановка оценок
            $.each(res.jr.marks, function(i, k) {
                $.each(k.marks, function(m, n) {
                    $("td[ind=" + n.task_id + "_" + n.user_id + "]").html(n.result);
                });
            });
            th_init();
            count_averange();
            //---------------------------------------------------
        }); //end do_ajax


    });

    //выделение цветом строки
    $("#journal table tr").live('click', function() {
        $(this).toggleClass('selecter-tr');
    });


    slider("param-click", "params-in", "rel");
});


//инициализация таблицы
function th_init() {
    $('#jour-table').fixedHeaderTable({footer: false, cloneHeadToFoot: false, fixedColumn: false, themeClass: 'fancyDarkTable', height: '600', altClass: 'odd'});
}

//подсчет среднего бала.
function count_averange() {
    $("#journal").prepend("<table id='srb'><tr><td>Ф.И.О.</td><td>Средний бал</td></tr></table>");
    $.each($("tbody tr"), function(m, o) {
        var el = $(this).attr('id');
        var name = "";
        var count = 0;
        var max = 0;
        $.each($("#" + el + " td"), function(i, k) {
            if (i == 0) {
                name = $(k).html();
            } else if ($(k).html() != "-") {
                count = count + parseInt($(k).html());
                max = max + 1;
            }

        });

        if (count != 0) {
            $("table#srb tbody").append("<tr><td>" + name + "</td><td class='c-mark'>" + Math.round(count / max) + "</td></tr>");
        }

    })

}

