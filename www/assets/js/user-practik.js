var need_upl = null;
// ------ ВЫПОЛНЕНИЕ ПРАКТИЧЕСКОГО ЗАДАНИЯ -----//
function jquploader_init() {
    var prid = $("input[name=prID]").val();
    var uID = $("input[name=uID]").val();    
    var tID = $('input[name=tID]').val();
    var url = '/ajax_send/upload_user_files/'+prid+"_"+uID+"_"+tID;


    var manualuploader = $('#uploadify_pr').fineUploader({
        
        button: $("#addfile"),
        multiple: false,
        request: {
            endpoint: url
        },
        text: {
            uploadButton: 'Обзор'
        },
        autoUpload: true
    }).on('complete', function(event, id, name, response) {

        update_file_list(); 
    });
}
//стандартная загрузка одного файла
function jform_upl(){
    var prid = $("input[name=prID]").val();
    var uID = $("input[name=uID]").val(); 
    var tID = $('input[name=tID]').val();   
    var url = '/ajax_send/upload_user_files/'+prid+"_"+uID+"_"+tID;    
    
    //если мобильная версия - не инициализировать
    if($("input[name=is_mobile]").val()== 'true') return false;
    // готовим объект
    var options = {
        target: "#divToUpdate",
        url: url,
        clearForm: true,
        beforeSubmit: function(){
            $("#uploadify-send").fadeOut(300);
            $("#uplimg").show();
        },
        complete: function(){
            $("#uploadify-send").show(300);
            $("#uplimg").hide();
            message("файл загружен!", "green");
            update_file_list(); 
            $("#divToUpdate").resetForm();
        }
    };
    //отобразить поле файла
    $("#uploadify").show();

    //показать кнопку загрузки изображения
    $('#uploadify-send').css({"visibility":"visible"});    
    
    //загрузка файла
    $('#uploadify-send').click(function(){
       
       //АВТОРИЗОВАН??
        is_auth();
        
        var val = $("#uploadify").val();
        if(val == ""){
            message('Выберите файл!',"red");
            return;
        } 
        // передаем опции в  ajaxSubmit
        $("#ufile").ajaxSubmit(options);
    })
    
    
}

//обновление списка прикрепленных юзером файлов
function update_file_list(){
    var prid = $("input[name=prID]").val();
    var uID = $("input[name=uID]").val();
    $.ajax({
        url: '/ajax_send/update_user_file_list/',
        async: false,
        type: 'POST',
        data: 'uid='+uID+"&prid="+prid,
        dataType: "json",
        cache: false,
        success: function(data){
            $("#ufiles").html('<div id="files-title">Прикрепленные файлы</div>').hide();
            $.each(data.files, function(i,k){
                var name = k.name=="" ? k.url : k.name;
                var str = "<div class='file' fileID='"+k.id+"'>";
                str += "<a class='g-3' href='/index/dn/user/"+k.url+"_'>"+name+"</a>";
                str += "<a href='#1' class='f-bu f-bu-warning delufile' rel='"+k.id+"'>x</a>";
                str += "</div>";
                $("#ufiles").append(str);
                $("#ufiles").slideDown(300);
            });
        }  
    });
}

//получить комментарий к практ.заданию
function get_comment(){
    var prid = $("input[name=prID]").val();
    var uID = $("input[name=uID]").val();
    var data = "prID="+prid+"&uID="+uID;
    do_ajax("/ajax_send/get_pr_comment/", data, function(res){
        $("textarea[name=text]").val(res).htmlarea('updateHtmlArea');
              
    });
}

//если аплоадиф включен - инициализировать
function check_uploadify(data){
    need_upl = data;    
    if(need_upl=="1"){
        jquploader_init();
    }else{
        jform_upl();
    }
}

//инищиализация редактора
function html_pad_init(){
    //если мобильная версия - не инициализировать
    if($("input[name=is_mobile]").val()=='true') return false;
    
    
    $("textarea[name=text]").htmlarea({
        // Override/Specify the Toolbar buttons to show
        toolbar: [
        ["bold", "italic", "underline"],
        ["subscript", "superscript","|", "justifyleft","justifycenter","justifyright"],
        ["orderedlist","unorderedlist"],
        ["p", "h1", "h2", "h3", "h4", "h5", "h6"],                  
        ],
        toolbarText: $.extend({}, jHtmlArea.defaultOptions.toolbarText, {
            "bold": "жирный",
            "italic": "курсив",
            "underline": "подчеркнутый"
        })
    });
}
$(document).ready(function(){
    //проверяю, включен ли в настройках алоадифити
    get_config("user_pr_upload",check_uploadify);      
   
    update_file_list();
    get_comment();
    
    //init html_pad
    html_pad_init();
    
    //загрузка выбранных файлов
    $('#uploadify-send').click(function(){
        if(need_upl=="1"){
             manualuploader.uploadStoredFiles();
        }
        
    });
    
    //удалить файл
    $(".delufile").live('click',function(){
        if(window.confirm("Удалить прикрепленный файл?")== false) return false;
        var id = $(this).attr('rel');
        var data = "id="+id;
        function res(){
            $('div[fileID='+id+']').hide(500);
            message("Файл удален!", "green");
        }
        do_ajax("/ajax_send/unlink_u_pr_file/", data, res);
    });
    
    //сохранить коммент
    $("button[name=save]").click(function(){        
        //АВТОРИЗОВАН???
        is_auth();
        
        var prid = $("input[name=prID]").val();     
		        
        if($("input[name=is_mobile]").val()=='true'){
            var text = $("textarea[name=text]").val();
        }else{
            var text = $("textarea[name=text]").htmlarea('toHtmlString');
        }      
        
        text = special_chars(text);
        
        var tid  = $("input[name=tID]").val().replace("_get","") ;
        var data = "prid="+prid+"&text="+text+"&tid="+tid;
        
        do_ajax("/ajax_send/link_comment/", data, function(){
            if($("input[name=is_mobile]").val()=='true'){
                alert('сохранено');
            }else{
                message("Данные успешно сохранены!","green");
            }
            
        });
        
    });
});

