//удаление группы
function delete_group(id) {

    $.ajax({
        url: "/ajax_send/group_del",
        type: 'POST',
        async: true,
        data: "id=" + id,
        success: function(data) {
            if (data == 1) {
                $("tr[rel=" + id + "]").fadeOut(500);
            }
        }
    })

}
$(document).ready(function() {
    //Обновление данных группы(редакт)
    $("input[name=group_name]").change(function() {        
        var grid = $(this).attr('grid');
        var grname= $(this).val();
        var data = "grID="+grid+"&name="+grname;
        if(confirm('Обновить данные?')== false) return false;
        
        do_ajax("/ajax_send/group_upd",data,function(){
            alert('Обновлено');
        },1);
    });

    //Новая группа
    $("input[name=add]").click(function() {

        var name = $("input[name=NewName]").val();

        if (name == "") {
            $(".f-input-help").html("Поле должно быть заполнено!").css({
                "color": "red"
            });
            $("input[name=NewName]").focus();
            return false;
        }

        $.ajax({
            url: "/ajax_send/group_add",
            type: 'POST',
            async: true,
            data: "name=" + name,
            success: function(data) {
                if (data == 1) {
                    $(".f-input-help").html('Новая группа добавлена! Можно добавить еще одну').css({
                        "color": "green"
                    });
                    $("input[name=NewName]").val("").focus();
                }
            }
        })
    });


//-----------ДРАГ-дроп добавление юзеров в группу
    $(".user-drag").draggable({
        connectToSortable: "#group",
        helper: "clone",
        revert: true,
    });
    
    $("#group").sortable({
           items:'li'
       });
       
    $("select[name=group]").change(function(){
        var id = $(this).val();
        var h = "";
        $("#userlist li").show(400);
        do_ajax_json('/ajax_send/group_users/',"grid="+id,function(res){
            $("#group").html();            
            $.each(res.d, function(i,k){
                h+="<li uid='"+i+"' class='user-drag'><span uid='"+i+"' class='del-usr'>[x]</span>"+k+"</li>";
                $("#userlist").find("li[uid="+i+"]").hide(300);
            });
            $("#group").html(h);
            
        });
    });
    
    //обновляю список пользователей группы
    $("button").click(function(){
        var data = ids = "";
        var grID = $('select[name=group]').val();
        $("#group li").each(function(i,k){
            ids += $(k).attr('uid')+"_";            
        });
        if(ids=="") return false;
        data = "grID="+grID+"&ids="+ids;
        do_ajax('/ajax_send/group_addusers/',data,function(res){
            
        },1);
    });
    
    $("#group .del-usr").live('click',function(){
        var uid = $(this).attr('uid');
        var grid = $("select[name=group]").val();
        var data = "uid="+uid+"&group_id="+grid;
        if(confirm("Удалить пользователя "+ $("li:has(span[uid="+uid+"])").text() +"?")==false) return false;
        do_ajax('/ajax_send/group_deluser', data, function(){
            $("li:has(span[uid="+uid+"])").hide(300);
        }, 1);
        
    });

});

