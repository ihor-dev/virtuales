
/**
 * функция заменяет основные спецсимволы и вырезает остальные
 */
function special_chars(str) {
    if (str == null)
        return false;
    str = str.replace(/&nbsp;/g, ' ');
    str = str.replace(/&raquo;/g, '"');
    str = str.replace(/&laquo;/g, '"');
    str = str.replace(/(&\w+;)/g, '');
    return str;
}

$(document).ready(function() {

    slider("add-topic-cli", "add-topic", "rel");
    $("input[name=name]").val("");
    $("textarea").val("");


    //новый форум
    $('#add-forum').click(function() {
        var name = $("input[name=fname]").val();
        var data = 'fname=' + name;
        do_ajax("/ajax_send/add-forum", data, function(res) {
            $(".add-topic").attr('slide', 1).hide();
            $("textarea").val("");
        });
    });

    //новая тема
    $('#add').click(function() {
        var id = $("input[name=fid]").val();
        var name = $("input[name=name]").val();
        var text = special_chars($("textarea").val());
        var data = 'id=' + id + '&name=' + name + "&text=" + text;
        do_ajax("/ajax_send/add-topic", data, function(res) {
            $(".add-topic").attr('slide', 1).hide();
            $("textarea").val("");
        });
    });

    //новое сообщение
    $('#add-message').click(function() {
        var id = $("input[name=tid]").val();
        var text = special_chars($("textarea").val());
        var data = 'tid=' + id + "&text=" + text;
        do_ajax("/ajax_send/add-message", data, function(res) {
            $(".add-topic").attr('slide', 1).hide();
            $("textarea").val("");

            var message = '<div class = "g-row topic-item" pid = "' + res + '">';
            message += '<div class = "fmessage-header">' + get_cur_time() + ' | <b> ' + $('div#u-title').html() + ' </b>';
            message += '</div><div>' + text + '</div></div>';
            $("#forum-page .content").append(message);
        });
    });

    //удаляем сообщение
    $('.del-fmessage').live('click', function() {
        if (window.confirm("Удалить сообщение?") == false)
            return false;
        var id = $(this).attr('pid');
        do_ajax("/ajax_send/del-fmessage", "pid=" + id, function() {
            $("div[pid=" + id + "]").hide(300);
        });
    });

    //удаляем topic
    $('.del-topic').live('click', function() {
        if (window.confirm("Удалить сообщение?") == false)
            return false;
        var id = $(this).attr('pid');
        do_ajax("/ajax_send/del-topic", "tid=" + id, function() {
            $("div[tid=" + id + "]").hide(300);
        });
    });

    //удаляем форум
    $('.del-forum').live('click', function() {
        if (window.confirm("Удалить сообщение?") == false)
            return false;
        var id = $(this).attr('fid');
        do_ajax("/ajax_send/del-forum", "fid=" + id, function() {
            $("div[tid=" + id + "]").hide(300);
        });
    });
    //подгружаю сообщения
    $('#get-messages').live('click', function() {
        var offs = $(this).attr('offset');
        var tid = $("input[name=tid]").val();
        data = "offset=" + offs + "&tid=" + tid;
        do_ajax_json("/ajax_send/get_fmessages", data, function(res) {
            var is_admin = $("input[name=myid]").val() == "1" ? "1" : "0";
            $.each(res.m, function(i, k) {
                var message = '<div class = "g-row topic-item" pid = "' + k.id + '">';
                message += '<div class = "fmessage-header">' + k.date + ' | <b> ' + k.username + ' </b>';
                if (is_admin == "1")
                    message += '<span class="del-fmessage f-bu" pid="' + k.id + '">x</span>';
                message += '</div><div>' + k.text + '</div></div>';
                $("#forum-page .content").append(message);
            });
            
            //изменяю кнопку
            $("#get-messages").attr('offset',(parseInt(offs)+5));
        }, "1");
    });

    //редактируем тему форума
    $('#edit-forum-text').click(function() {
        var name = $(".f-name").html();
        var text = $(".f-text").html();
        var type = $(this).attr('type');
        var id = $("input[name=tid]").val();
        if (type != 'save') {//редактирую
            $(".f-name").html("<input name='fname' type='text' value='" + name + "'/>");
            $(".f-text").html("<textarea name='fname'>" + text + "</textarea>");
            $(this).attr('src', '/assets/images/icon/save.png').attr('type', 'save');
        } else {//сохраняю
            name = $("input[name=fname]").val();
            text = $('textarea').val();
            var data = "name=" + name + "&text=" + text + "&id=" + id;
            do_ajax("/ajax_send/update-topic", data, function() {
                $(".f-name").html(name);
                $(".f-text").html(text);
                $('#edit-forum-text').attr('src', '/assets/images/icon/pen.png').attr('type', 'edit');
            }, "1");
        }

    });
    
    //разместить топик в новостях
    $(".report-news").live('click',function(){
        var th = $(this);
        var id = $(this).attr('pid');
        var type = $(this).html();
        var status = type == "+" ? 'news' : "null";
        var data = "id="+id+"&type="+status;
        do_ajax('/ajax_send/set-news',data,function(){
            th.removeClass("bgorange bggreen");
            if(type=="+"){
                th.html("-").addClass("bgorange");
            }else{
                th.html("+").addClass("bggreen");
            }
        })
    });

});