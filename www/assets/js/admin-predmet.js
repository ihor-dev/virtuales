
var ids = ''; 
var predmet = '';//id предмета

//---------------PREDMET-USERS---------------------------------
//проверяю, выбрана ли группа и загружаю список пользователей
function check_group(){
    var group = $("select[name=group]").val();   
    if(group != "group"){
        get_radio();
        var prID = predmet;
        var data = "grID="+group+"&prID="+prID;
        
        //запрос списка пользователей
        do_ajax_json("/ajax_send/predmet-get-users/", data, function(data){
            $("#predmet-users").hide();
            $("#predmet-users").html("<div class='g-4 table_header'>Список пользователей:</div>");
            $("#predmet-users").append("<div><span id='checkall' class='f-bu' title='Выделить всех'>Все</span></div>");
            
            //user's list
            $.each(data.users.data,function(i,k){
                $("#predmet-users").append("<div><label class='file'><input type='checkbox' class='w20 user-id' uid='"+k.uid+"' name='id' value='"+k.uid+"'/>"+k.name+" "+k.surname+"</label> </div>")
            });
            
            //mark linked users
            $.each(data.uids,function(i,k){
                $("label:has(input[uid="+k.user_id+"])").css('color','green').append("  <span id='"+k.id+"' class='w20 g-025 f-bu f-bu-warning  del-user'>x</span>");
            });
                       
            $("#predmet-users").append("<span id='add-user' class='f-bu f-bu-action g-6'>Добавить</span>");
            $("#predmet-users").fadeIn(300);
           
            //кнопка "выбрать всех"
            button_init();          
                       
        });
    }else{
        //delete user`s list
        $("predmet-users").fadeOut(300);
    }
}


//Инициализация кнопки "все"
function button_init(){
    //выделить все элементы
    $("span#checkall").click(function(){
        if($("span#checkall").attr('rel')!="1"){
            $(":input[type=checkbox]").each(function(i,k){
                $(k).attr("checked",true);
            })  
            $("span#checkall").attr('rel',1);
        }else{
            $(":input[type=checkbox]").each(function(i,k){
                $(k).attr("checked",false);
            })
            $("span#checkall").attr('rel',0);
        }
    });
}

//сбор чекбокс-значений
function get_checkbox_values(){
    ids = "";
    $("input.user-id").each(function(i,k){
        var checked = $(k).attr("checked");
        if(checked=="checked"){
            ids = ids + " " + $(k).val();
        }
    });
}

//---------------DOCUMENT READY----------------------------------
$(document).ready(function(){
    $("select[name=group]").val("");

    // NEW PREDMET
    $("span.predmet-cr").click(function(){
        var status = $(this).attr('status');
        
        if(status == 'show'){
            //показать форму
            $("input[name=name]").val("");
            $('.new-predmet').removeClass('hide');
            $(this).attr('status','save');
        }else if(status == 'save'){
            //сохранить предмет
            var name = $("input[name=name]").val();
            do_ajax('/ajax_send/add_predmet',"name="+name,function(id){
                var html = '<div class="tests-list-item g-row fz-14" rel="'+id+'"><div class="g-7 g-ie">'+name+'</div></div>';
                $("#predmet-items").append(html);
                $("span.predmet-cr").attr('status','show');
                $('.new-predmet').addClass('hide');
            });
        }
    });//end new predmet--------
    
    //DEL PREDMET
    $("span.del").live('click',function(){
        if(window.confirm("Удалить предмет??")==false) return false;
        var id = $(this).attr('pid');
        do_ajax('/ajax_send/del-predmet','id='+id,function(){
            $(".tests-list-item[rel="+id+"]").remove();
        });
    });

    
    

    
    //Изменяем группу
    $("select[name=group] , input[type=radio]").change(function(){
        check_group();
    });
    
    
    //Подписываем юзеров на предмет
    $("#add-user").live('click',function(){
        get_checkbox_values();
        get_radio();
        var data = "pr="+predmet+"&ids="+ids;

        do_ajax('/ajax_send/pr-add-users',data,function(res){
            alert("Complete");
        });
        
    });
    
    //delete user
    $("span.del-user").live('click',function(){
        if(window.confirm("Снять участие в предмете?") == false) return false;
        var id = $(this).attr('id');
        var th = $(this);
        do_ajax("/ajax_send/predmet-del-user","id="+id,function(){
            th.remove();
        });
        return false;
    });
    
    
  

});//End document ready

//ид предмета
function get_radio(){
    $("input[name=predmet]").each(function(i,k){
        if($(k).attr('checked')=="checked"){
          predmet = $(k).val();
        }
            
    });
}

