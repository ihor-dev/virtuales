//после запроса конфига, отметить чекбокс нужно ли мыло
function get_settings_needemail(data){
    if(data=="1"){
        $("input[name=need_email]").attr("checked",true);
    }else{
        $("input[name=need_email]").attr("checked",false);
    }
}

function set_settings_needmail(data){
    get_config("need_email",get_settings_needemail);
}
//-----------------------------------------------------


//после запроса конфига, отметить чекбокс нужно ли uploadify
function get_settings_upl(data){
    
    if(data=="1"){
        $("input[name=user_pr_upload]").attr("checked",true);
    }else{
        $("input[name=user_pr_upload]").attr("checked",false);
    }
}

function set_settings_upl(data){
    get_config("user_pr_upload",get_settings_upl);     
}
//------------------------------------------------------


$(document).ready(function(){
    //обновление кнопки мыло при реге
    get_config("need_email",get_settings_needemail);
    
    //обновление кнопки uploadify
    get_config("user_pr_upload",get_settings_upl);
    
   
    //email
    $("input[name=need_email]").click(function(){
        var data = $(this).attr("checked");
        if(data == "checked"){
            set_config("need_email","1",set_settings_needmail);
        }else{
            set_config("need_email","0",set_settings_needmail);
        }
    });

    //uploadify
    $("input[name=user_pr_upload]").click(function(){
        var data = $(this).attr("checked");        
        if(data == "checked"){
            set_config("user_pr_upload","1",set_settings_upl);
        }else{
            set_config("user_pr_upload","0",set_settings_upl);
        }
    });
    
             
                
});

