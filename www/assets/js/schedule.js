

$(document).ready(function(){ 
    
    //СОхранение записи
    $("#upd-sbmt-edit-form").click(function(){
        
        var id= $("input[name=id]").val();
        var content  = tinyMCE.get('text_editor').getContent();        
        content = special_chars(content);                
        
        var data = "id="+id+"&name=Расписание пар"+"&content="+content+"&type=schedule&category=0";
       
        var url = "/ajax_send/update_post";
        
        //validation
        if(content == ''){
            message("А расписания-то нету?!! ...<br/> Заполните все поля!", "#EA1515");
        }        
        $.ajax({
            url	    : url,
            type    : 'POST',
            async   : true,
            data    : data,
            beforeSend: function(){
                $(".write-schedule").fadeOut(300);
                $(".message").fadeIn(600);
            },
            success: function(result){
                if(result != "1"){
                    alert(result);
                }
                $(".message").html("Благодарим за ожидание, данные успешно сохранены на сервере!<br/> Сейчас Вы будете перенаправлены к списку Ваших шпаргалок.");
                location.replace("/schedule");              
            }
        });//---end ajax
    });
    
    $("#clear-text").click(function(){
          tinyMCE.activeEditor.setContent("");
    });
    
});
