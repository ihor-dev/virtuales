/* ------------ Инициализация Tiny MCE ----------*/

tinyMCE_GZ.init({
    plugins : "pagebreak,style,layer,table,save,advhr,advlink,iespell,inlinepopups,insertdatetime,searchreplace,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,autosave",
    themes : 'advanced',
    languages : 'ru',
    disk_cache : true,
    debug : false
});

tinyMCE.init({
    // General options
    mode : "exact",
    elements: "text_editor",
    theme : "advanced",
    language : "ru",
    disk_cache : true,
    relative_urls: false,//Нужно для того, чтобы не ставил относительные пути в картинки
    remove_script_host: false,// --||--
    plugins : "pagebreak,style,layer,table,save,advhr,advlink,iespell,inlinepopups,insertdatetime,searchreplace,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,autosave",

    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,formatselect,fontselect,fontsizeselect,fullscreen",
    theme_advanced_buttons2 : "link,unlink,anchor,|,forecolor,backcolor,|,sub,sup,|,blockquote,cite,|,pastetext,pasteword,|,search,replace,|,undo,redo,|,image,cleanup,removeformat,hr,code",
    theme_advanced_buttons3 : "attribs,|,restoredraft,|,tablecontrols,|,charmap,iespell,visualaid",

    theme_advanced_resizing : true,

    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom"
// Drop lists for link/image/media/template dialogs


});
