//Скрипт модификация title-подсказок
this.screenshotPreview = function(){
        /* CONFIG */
                
                xOffset = 20;
                yOffset = 120;

                // these 2 variable determine popup's distance from the cursor
                // you might want to adjust to get the right result

        /* END CONFIG */
        $(".screenshot").hover(function(e){
                 
                this.t = this.title;
                this.title = "";
                var c = (this.t != "") ? " " + this.t : "";
                
                $("body").append("<p id='screenshot'><img src='"+ $(this).attr('rel') +"' alt='url preview' />"+ c +"</p>");
                $("#screenshot")
                        //.css("top",(e.pageY - yOffset) + "px")
                        //.css("left",(e.pageX + xOffset) + "px")
                        .css("top","20px")
                        .css("left","20px")
                        .fadeIn("fast");
    },
        function(){
                this.title = this.t;
                $("#screenshot").remove();
    });
        $(".screenshot").mousemove(function(e){                        
              setTimeout(function(){
                  $("#screenshot")             
                        //.css("top",(e.pageY  - hei ) + "px")                        
                        //.css("left",(e.pageX + xOffset) + "px");
                        .css("top", "20px")                        
                        .css("left","20px");
              }, 2000);  
        });
};
this.imagePreview = function(){
        /* CONFIG */

                xOffset = 10;
                yOffset = 30;

                // these 2 variable determine popup's distance from the cursor
                // you might want to adjust to get the right result

        /* END CONFIG */
        $("a.preview").hover(function(e){
                this.t = this.title;
                this.title = "";
                var c = (this.t != "") ? " " + this.t : "";
                $("body").append("<p id='preview'><img src='"+ this.href +"' alt='Image preview' />"+ c +"</p>");
                $("#preview")
                        .css("top",(e.pageY - xOffset) + "px")
                        .css("left",(e.pageX + yOffset) + "px")
                        .fadeIn("fast");
    },
        function(){
                this.title = this.t;
                $("#preview").remove();
    });
        $("a.preview").mousemove(function(e){
                $("#preview")
                        .css("top",(e.pageY - xOffset) + "px")
                        .css("left",(e.pageX + yOffset) + "px");
        });
};
this.tooltip = function(){
        /* CONFIG */
                xOffset = 30;
                yOffset = 20;
                // these 2 variable determine popup's distance from the cursor
                // you might want to adjust to get the right result
        /* END CONFIG */
        $(".tooltip").hover(function(e){
                this.t = this.title;
                this.title = "";
                $("body").append("<p id='tooltip'>"+ this.t +"</p>");
                $("#tooltip")
                        .css("top",(e.pageY - xOffset) + "px")
                        .css("left",(e.pageX + yOffset) + "px")
                        .fadeIn("fast");
    },
        function(){
                this.title = this.t;
                $("#tooltip").remove();//Удаление подсказки. можна поставить на клик кнопки
    });
        $(".tooltip").mousemove(function(e){
                $("#tooltip")
                        .css("top",(e.pageY - xOffset) + "px")
                        .css("left",(e.pageX + yOffset) + "px");
        });
};