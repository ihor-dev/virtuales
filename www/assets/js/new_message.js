var statuswindow = "";
//добавить сообщение в мессадже-бокс
function add_messagetolist(from, date, text){
    var str = "<div class='message-item mitem'>";
    str += "<span class='from login'>"+from+"</span>";
    str += "<span class='from'> ["+date+"]</span>";
    str += "<div class='text'>"+text+"</div></div>";
    $(".message-list").prepend(str);
    
}

//возвращает текущее дату время
function get_cur_time(){
    var d = new Date();
    var curr = d.getDate()+"."+(d.getMonth()+1)+"."+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
    return curr;  
}

//загрузка всех сообщений
function get_messages(){
    
    var from = $("select[name=user]").val();
    var data = "from="+from;
    do_ajax_json("/ajax_send/get_messages/", data, function(res){
        $(".message-list").html("");
        $.each(res.messages,function(i,k){
            add_messagetolist(k.from,k.data,k.text);            
        })
                //меняю авку собеседника
        $("#speaker-img").attr('src',res.img);
    });
}

//установить текущего пользователя в списке
function set_curuser(){
    var id = $("input[name=curID]").val();         
    $("select[name=user]").val(id);
    id = $("select[name=user]").val();    
    
    var name = $("select[name=user] option[value="+id+"]").html();      
    $("input[name=uname]").val(name);    
}

//догрузить новые сообщения
function get_new_messages(){
    var id = $("select[name=user]").val();
    var name =  $("input[name=uname]").val();
    do_ajax_json("/ajax_send/get_new_messages/", "id="+id, function(res){
        if(res != "0"){
            if(statuswindow == 0) document.title="Новое сообщение!";
            $.each(res.messages,function(i,k){
                add_messagetolist(name,k.data,k.text);            
            });    
        }
        
    })
}
$(document).ready(function(){
    $(window).blur(function(){
        statuswindow = 0;
    });
    
    $(window).focus(function(){
        statuswindow = 1;
        document.title="Система Web-образования";
    });
    
    
    
    //НИЖЕ ВСЕ - для окна сообщений!!!
    if($("input[name=uname]").val() == undefined) return false;
    setInterval(get_new_messages, 10000);
    
    //устанавливаю текущего юзера
    set_curuser();
    
    get_messages();
    
    //Шлем сообщение
    $("button[name=send-message]").click(function(){
        var text = $("textarea").val();
        text = text.replace(/&[0-9a-z]+;/gi,'');
        var to = $("select[name=user]").val();
        if(text == "") return false;
        
        var data="text="+text+"&to="+to;
        do_ajax("/ajax_send/send_message/",data,function(res){
            if(res == "1"){
                $("textarea").val("");
                add_messagetolist("Я", get_cur_time(), text);
                get_new_messages();
            }
        
        },1);
    });
    
    //send by ENTER
    $("textarea[name=message]").keypress(function(e){   
        
        if(e.keyCode==13){
            $("button[name=send-message]").trigger('click');            
        }
    });
    
    //изменение пользователя
    $("select[name=user]").change(function(){
        
        get_messages();
        id = $("select[name=user]").val();
        var name = $("select[name=user] option[value="+id+"]").html();
        
        $("input[name=uname]").val(name);
        
    });
    
    //удаление переписки    
    $("#ms-delete").click(function(){
        if(confirm("Удалить все прочитанные сообщения??")==false)return false;
        var from = $("select[name=user]").val();
        
        do_ajax("/ajax_send/delete_messages/","from="+from,function(){                    
            $(".message-list").html("");
        });
    });
    
    //проверить статус онлайн        
    $("#ms-online").click(function(){        
        var from = $("select[name=user]").val();        
        do_ajax("/ajax_send/is_online/","from="+from,function(res){                    
            if(res == "1"){
                alert("Пользователь онлайн!");
            }else{
                var name = $.trim($("select[name=user] option[value="+from+"]").html());
                alert(name+" не в сети с "+res);
            }
        });
    });
});

