var ids= "";
function count_checked(){
    count_checkbox = 0;
    $(":input[type=checkbox]").each(function(i,k){
        var check = $(k).attr('checked');
        if(check == "checked"){
            count_checkbox = count_checkbox + 1;
        }
    })
    $("span#ids").html(count_checkbox);
    return true;
}

function get_checkbox_values(){
    ids = "";
    $("input[type=checkbox]").each(function(i,k){
        var checked = $(k).attr("checked");
        if(checked=="checked"){
            ids = ids + " " + $(k).val();
        }
    });

}
$(document).ready(function(){
    count_checked();
    
    //выделить все элементы
    $("span#checkall").click(function(){
        if($("span#checkall").attr('rel')!="1"){
            $(":input[type=checkbox]").each(function(i,k){
                $(k).attr("checked",true);
            })  
            $("span#checkall").attr('rel',1);
        }else{
            $(":input[type=checkbox]").each(function(i,k){
                $(k).attr("checked",false);
            })
            $("span#checkall").attr('rel',0);
        }
        count_checked();
    });
    
    //при изменении чекбокса
    $("input[type=checkbox]").change(function(){
        count_checked();
    });
    
    //действие с выбранными
    $("select[name=action]").change(function(){
        var action = $(this).val();

        get_checkbox_values();
        if(action=="delete"){
            $.ajax({
                url     : "/ajax_send/new-user-delete/",
                type	: 'POST',
                async	: true,
                data	: "data="+ids,
                success: function(data){
                    alert("удалено");
                }
            })
        }else if(action=="active"){
            $.ajax({
                url     : "/ajax_send/new-user-active/",
                type	: 'POST',
                async	: true,
                data	: "data="+ids,
                success: function(data){
                    alert("выполнено");
                }
            })
        }
        
    })
    
    //-----------------------------СПИСОК ПОЛЬЗОВАТЕЛЕЙ ---------------------------
    
    $(".group_title").click(function(){
        var id = $(this).attr('rel');
        var slide = $(this).attr('slide');
        if(slide=="0"){
            $("div.group_users[rel="+id+"]").slideDown(300);
            $(this).attr('slide',1);
        }else{
            $("div.group_users[rel="+id+"]").slideUp(300);
            $(this).attr('slide',0);
        }
    });
     
    //удаляю юзера
    $(".u-delete").click(function(){
        var uid = $(this).attr('uid');
        if(prompt("Для подтверждения удаления юзера, введите пароль!","")!="q") return false;
        do_ajax('/ajax_send/delete_user/',"uid="+uid,function(){
            $(".group_users_user:has(span[uid="+uid+"])").fadeOut(300);
        });
    });
     
    //изменить логин/парль
    $(".u-config").click(function(){        
        if(prompt("Для продолжения, введите пароль!","")!="q") return false;                 
        var userID = $(this).attr("uid");
                        
        do_ajax("/ajax_getmodal/adm-user-info/",null, function(res){            
            $("#ajax").html(res);
            modal_init(400);
            do_ajax('/ajax_send/admin-get-user-login/',"uID="+userID,function(res){
                $("input[name=login]").val(res);
            })      
            $("span.send").click(function(){
                var password = $("input[name=password]").val();
                if(password == ""){
                    $("input[name=password]").focus();
                    alert('Введите пароль!');
                    return false;
                }
                var data = "uID="+userID+"&password="+password;                                                           
                do_ajax('/ajax_send/admin-set-user-pw/',data,function(){
                    alert("Новый пароль: "+password);
                });
            });
        });
        
    });
    
    
    //---------------------Статистика---------------
    $("#statistik-send").val(0);
    $("#statistik-send").change(function(){
        if($(this).val() == 0) return false;
        var data = "time="+$(this).val();
        $("#statistik").html("Загрузка...");
        do_ajax_json("/ajax_send/admin-visitors/",data,function(res){
           html = "<div class='g-row table_header'><div class=g-3 cntr>Ф.И.О</div><div class='g-1'>Группа</div><div class='g-2'>Дата</div><div class='g-2'>Прошло время</div></div>"; 
           $("#statistik").html(html);
           $.each(res.u, function(i,k){
               var html = "<div class='g-row mitem'><div class='g-3'>"+k.name+"</div>";
                html +="<div class='g-1'>"+k.group+"</div>";
                html +="<div class='g-2'>"+k.time+"</div>";
                html +="<div class='g-2'>"+k.diff+" назад</div>";
                html +="</div>";
               $("#statistik").append(html);
           });
           
         });

    });

 
                
});

