var ids = ""; // user ids
/**
 * функция заменяет основные спецсимволы и вырезает остальные
 */
function special_chars(str) {
    if (str == null)
        return false;
    str = str.replace(/&nbsp;/g, ' ');
    str = str.replace(/&raquo;/g, '"');
    str = str.replace(/&laquo;/g, '"');
	str = str.replace(/&ndash;/g, '-');
	str = str.replace(/&mdash;/g, '-');
    str = str.replace(/(&\w+;)/g, '');

    return str;
}

$(document).ready(function() {

    //СОхранение записи
    $("#upd-sbmt-edit-form, #save-sbmt-edit-form").click(function() {
        var type = $(this).attr("id");
        var content = tinyMCE.get('text_editor').getContent();
        content = special_chars(content);
        var name = $("input[name=name]").val();
        var category = $("select[name=category]").val();

        var data = "name=" + name + "&content=" + content + "&category=" + category + "&type=lesson";

        if (type == "upd-sbmt-edit-form") {
            var url = "/ajax_send/update_post";
            data += "&id=" + $("input[name=id]").val();
        } else {
            var url = "/ajax_send/create_post";
        }
        //validation
        if (content == '') {
            message("Контент пуст! <br/> Заполните все поля!", "#EA1515");
        } else if (name == '') {
            message("Поле 'ИМЯ' пусто! <br/> Заполните все поля!", "#EA1515");
            $("input[name=name]").focus();
        }

        do_ajax(url, data, function() {
            message('Сохранено!', "green");
            setTimeout(function() {
                if (confirm("Закончить редактироване?") == true)
                    location.replace("/admin_lessons");
            }, 1000);

        }, "1");//ajax end
    });



    //Панелька, которая вылазит при нажатии в редакторе лекций на названии
    $(".tests-list-item").click(
            function() {
                $(".test-navigation").hide(200);
                var id = $(this).attr('rel');
                $(".test-navigation[rel=" + id + "]").show(100);
            }
    );


    //удаление лекции
    $(".test-delete").click(function() {
        id = $(this).attr('rel');

        if (confirm("Удалить лекцию??")) {
            $.ajax({
                url: "/ajax_send/delete_post/",
                type: "POST",
                async: "true",
                cache: false,
                data: "id=" + id,
                success: function() {
                    $("div.tests-list-item[rel=" + id + "]").html('удалено').hide(600);
                }
            });

        }
    });

    //кнопка создать лекцию
    $(".less-cr").click(function() {
        location.replace('/admin_postwrite');
    });

    //---------------СОРТИРОВКА-------------------------------    
    if ($(".write-shpora").html() == undefined) {
        //Инициализация сортировщика
        $(function() {
            $("#sortable").sortable({
                delay: 1000
            });
        });
        //сохранение результатов
        $("#sortable-save").click(function() {
            if (window.confirm("Подтвердите запрос на сортировку?") == true) {
                get_less_ids();
            }

        });
    }



    //--------------------------------------------------------    

    //share posts
    $("#posts .post_share").click(function() {
        var id = $(this).attr("rel");
        //грузим модалку
        do_ajax("/ajax_getmodal/share_post/", null, function(html) {
            $("#ajax").html(html);
            //инициализирую модалку
            modal_init(400);
            //ид поста в скрытое поле
            $("input[name=postID]").val(id);

            //запрашиваю список групп
            do_ajax_json('/ajax_send/get_post_shared_groups/', 'post_id=' + id, function(res) {
                $.each(res.g, function(i, k) {
                    $('.grlist').append("<div>" + parseInt(i + 1) + ") " + k.name + "<span class='delsharegroup' id='" + k.id + "'> [del]</span></div>");
                });

                //delete shared group
                $('.delsharegroup').click( function() {
                    var grid = $(this).attr('id');
                    if (confirm("Удалить???") == false)
                        return false;
                    var data = 'pid=' + id + '&grid=' + grid;
                    do_ajax('/ajax_send/delete_share_post', data, function() {
                    }, 1);
                });// end delete shared

            }, 1);

            //сохранение доступа            
            $("button[name=share_btn]").click(function() {
                var group = $("select[name=group]").val();
                //если выбраны пользователи                
                if (group == "") {
                    if (window.confirm("Выберите группу!") == false)
                        return false;
                }
                var post_id = $("input[name=postID]").val();
                var data = "group_id=" + group + "&post_id=" + post_id;
                do_ajax("/ajax_send/share_post/", data, function(res) {
                    alert("cохранено");
                });

            });
        });
    });




});

//сбор ид лекций для сортировки
function get_less_ids() {
    ids = '';
    $.each($("#sortable .tests-list-item"), function(i, k) {
        ids = ids + " " + $(k).attr('rel');

    });
    var data = 'id=' + $.trim(ids);
    do_ajax('/ajax_send/sort_posts/', data, function() {
    }, "1");
}

