$(document).ready(function() {
    //прячу все
    hide_inputs();

    //инициализация загрузчика
    jquploader_init();


    //slider password
    slider("passwordbutn", "passwordblock", "rel", "1");

    //клик редактировать
    $("span.e").click(function() {
        var elem = $(this).attr('rel');
        enable_field(elem);
    });

    //update info
    $("button[name=upd]").click(function() {
        var id = $('input[name=uid]').val();
        var name = $("input[name=name]").val();
        var sur = $("input[name=surname]").val();
        var patr = $("input[name=patronymic]").val();
        var email = $("input[name=email]").val();
        var data = "id=" + id + "&name=" + name + "&surname=" + sur + "&patronymic=" + patr + "&email=" + email;

        do_ajax("/ajax_send/profile_upd", data, function() {
            message('Данные успешно обновлены!', "green");
            hide_inputs();
        });
    });//-end update


    //change password
    $('button[name=upd_pass]').click(function() {
        var old = $('input[name=oldpass]').val();
        var newp1 = $('input[name=newpass1]').val();
        var newp2 = $('input[name=newpass2]').val();
        var id = $('input[name=uid]').val();

        if ((old == "") || (newp1 == "") || (newp2 == "")) {
            message("Заполните все три поля!", "red");
            return false;
        }
        else if (newp1 !== newp2) {
            message("Поля 'Новый пароль' и 'Еще раз' не совпадают!", "red");
            return false
        }

        var data = "id=" + id + "&password=" + newp1 + "&old_password=" + old;
        do_ajax('/ajax_send/change_password/', data, function(res) {
            if (res == 'error') {
                message('Старый пароль введен неверно!', 'red');
            } else {
                message('Пароль изменен', "green");

            }

        });
    });

    //инициализация crop
    crop_init();

    //обрезаю авку
    $("#save-prev").click(function() {
        var data = "x="+cropX+"&y="+cropY+"&w="+cropW+"&h="+cropH;
        do_ajax('/ajax_send/resize-avatar',data, function(){
            alert('Ok, твое фото загружено!')
        })
    })
});//end document ready



//инициализация crop
function crop_init(){
        $('#ava-img').Jcrop({
        onChange: showPreview,
        onSelect: showPreview,
        aspectRatio: 1,
        setSelect:   [ 0, 0, 100, 100 ],
        minSize: [90, 90]
    });
}

var cropX, cropY, cropW, CropH = null;
//priview of image
function showPreview(coords)
{
    var rx = 100 / coords.w;
    var ry = 100 / coords.h;
    var imw = $("#ava-img").width();
    var imh = $("#ava-img").height();

    $('#prev').css({
        width: Math.round(rx * imw) + 'px', //150
        height: Math.round(ry * imh) + 'px', //268
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
    });
    cropX = coords.x;
    cropY = coords.y;
    cropW = coords.w;
    cropH = coords.h;
}

//отключаю режим редактирования
function hide_inputs() {
    $(".info").css("border", "none").attr('disabled', true);
    $("button[name=upd]").hide();
}

//включаю поле для редактирования
function enable_field(item) {
    $("input[name=" + item + "]").css("border", "solid 1px #ccc").attr('disabled', false);
    $("button[name=upd]").fadeIn(300).attr('disabled', false);
}

//инициализация загрузчика фоток
function jquploader_init() {
    var manualuploader = $('#uploadify_pr').fineUploader({
        
        button: $("#addfile"),
        multiple: false,
        request: {
            endpoint: '/ajax_send/avatar_upl/1'
        },
        text: {
            uploadButton: 'Обзор'
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'png', 'gif']
        },
        autoUpload: true
    }).on('complete', function(event, id, name, response) {
        var uid = $("input[name=uid]").val();
        var img = '/assets/files/avatar/'+uid+'.jpg'        
        $("img#prev").attr('src',"").attr('src',img);
        $(".avatar-frame").html("").html("<img src='"+img+"' id='ava-img' />");        
        crop_init();
        
    });



}
