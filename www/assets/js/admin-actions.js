//       JS admin actions
//------------------------------------//

function get_content(){
    do_ajax_json('/ajax_send/get_actions/','d',function(res){
        $.each(res.data, function(i,k){
            var h = "<div class='g-row small_rows'>";
            h+="<div class='g-1 g-ie'>"+k[4]+":</div>";
            h+="<div class='g-3 g-ie'>"+k[1] +"</div>";
            h+="<div class='g-3 g-ie ' title='"+k[2]+"'>"+k[0] +"</div>";
            h+="<div class='g-2 g-ie'>"+k[3] +"</div>";
            h+="</div>";
            $("#actions").append(h);
        });
    });
}


function filter(find){
    $('.small_rows').hide();
    
    $('.small_rows:contains("'+find+'")').fadeIn(400);
}
/* ----------- ==============  ---------*/
$(document).ready(function(){
    get_content();
    
    //filter
    $('input[name=group]').change(function(){
        filter($(this).val());
    });
    
    //clear
    $('#clear').click(function(){        
        if(confirm('Очистить историю?')==false) return false;
        do_ajax('/ajax_send/clear_actions/','null',function(){
            
            $('.small_rows').hide();
        });
    });

});
