$(document).ready(function(){
   $("select[name=mark-system]").val(12);
    $.each($('.dinamic'), function(i,k){
        sr_bal($(k).attr('rel'), $(k).html());        

    });
   
   $("span.click").click(function(){
        if(window.confirm("Вы уверены, что хотите отправить запрос на повторное выполнение теста??")==false) return false;
        
        var id = $(this).attr('rel');
        do_ajax("/ajax_send/try_retask/","task_id="+id,function(){
            message("Ваш запрос добавлен, и будет рассмотрен преподавателем.","green");
        })        
    });
    
    $("select[name=mark-system]").change(function(){
        var new_type = $(this).val();        
        $.each($(".c-mark"),function(i,k){
            var mark = $(k).html();//оценка
            var cur_type = $(k).attr('mtype');//текущая система оценивания каждой оценки
            var result = 0;
            //из 12 в 5
            if(new_type==5 && cur_type!=="5"){
                if(mark<4){result = 2;}
                else if(mark>3 && mark <7){result = 3;}
                else if(mark>6 && mark <10){result = 4;}
                else if(mark >9 && mark <13){result = 5;}                    
                $(k).html(result).attr('mtype',5).attr('old',mark);
            }//из 5 в 12
            else if(new_type==12 && cur_type !==12){
                result = $(this).attr('old');
                $(k).html(result).attr('mtype',12).attr('old',result);
            }
        });
    });
    
    //переключение - фильтр по группам
    $(".group").click(function(){
        var id = $(this).attr('rel');
        $('button.group').removeClass('bggreen');
        $(this).addClass('bggreen');
        
        if(id == 'all'){
            $("div.point").show(300); 
            return;
        }
        $("div.point").hide();
        $("div.point[rel="+id+"]").show(300);
    });
    
});//end docready

//подсчет среднего бала
function sr_bal(rel, gr){
    var mark = 0;
    var count = 0;
    $.each($(".c-mark[rel="+rel+"]"),function(i,k){
        count = count+1;
        if($(k).html()!==""){
            mark = mark + parseInt($(k).html());
        }
        
    });

    $("#sr-bal").append('<div rel="'+rel+'">'+gr+": <span class='c-mark'>"+Math.round(mark/count)+"</span> б.</div>");
}