var ids = ""; var predmet = '';
function count_checked() {
    count_checkbox = 0;
    $(":input[type=checkbox]").each(function(i, k) {
        var check = $(k).attr('checked');
        if (check == "checked") {
            count_checkbox = count_checkbox + 1;
        }
    })
    $("span#ids").html(count_checkbox);
    return true;
}

function get_checkbox_values() {
    ids = "";
    $("input[type=checkbox]").each(function(i, k) {
        var checked = $(k).attr("checked");
        if (checked == "checked") {
            ids = ids + " " + $(k).val();
        }
    });

}
//Инициализация кнопки "все"
function button_init() {
    //выделить все элементы
    $("span#checkall").click(function() {
        if ($("span#checkall").attr('rel') != "1") {
            $(":input[type=checkbox]").each(function(i, k) {
                $(k).attr("checked", true);
            })
            $("span#checkall").attr('rel', 1);
        } else {
            $(":input[type=checkbox]").each(function(i, k) {
                $(k).attr("checked", false);
            })
            $("span#checkall").attr('rel', 0);
        }
        count_checked();
    });

    //при изменении чекбокса
    $("input[type=checkbox]").change(function() {
        count_checked();
    });

}

//ид предмета
function get_radio(){
    $("input[name=predmet]").each(function(i,k){
        if($(k).attr('checked')=="checked"){
          predmet = $(k).val();
        }
            
    });
}

//загрузка заданий
function load_tasks(grID){
    var data = "grID="+grID;
        do_ajax_json("/ajax_send/get-all-tasks/",data,function(res){
            $("div.tasks-group_tasks[group_id="+grID+"]").html("");
            //распаковка заданий
            $.each(res.data,function(i,k){
                var h = '<div class="g-row tasks-group_tasks-item no_top_margin" task_id="'+k.task_id+'">';
                h+=' <div class="g-row item no_top_margin">';
                h+=' <div class="g-6 g-ie" title="'+k.task_date+'">';
                h+=' <button class="g-05 f-bu f-bu-default w20 task_slide" task_id="'+k.task_id+'">+</button>';
                h+=k.name+'</div>';
                
                h+='<div class="g-1 g-ie">';
                if(k.type=='lesson'){h+='Л';}
                else if(k.type=='test'){h+='T';}
                else if(k.type=='practik'){h+='П';}
                h+='</div>';
                
                h+='<div class="g-025 w20 g-ie" title="Загрузить информацию задания"><button class="f-bu f-bu get-task" task_id="'+k.task_id+'">G</button></div>';
                h+='<div class="g-025 w20 g-ie" title="Редактировать список участников задания"><button class="f-bu f-bu edit-task" task_id="'+k.task_id+'">E</button></div>';
                h+='<div class="g-025 w20 g-ie" title="Удалить задание"><button class="f-bu f-bu-warning del-task" task_id="'+k.task_id+'">Х</button></div>    </div>';

                h+='<div class="g-row tasks-group_tasks-item-user hide" task_id="'+k.task_id+'" type="'+k.type+'"></div>';
                                
                $("div.tasks-group_tasks[group_id="+grID+"]").append(h);                
            });


        },1);

}

//##################################################################
$(document).ready(function() {
    //группы    
    slider("group_slide", "tasks-group_tasks", "group_id");
    
    //открывать список заданий группы
    slider("task_slide", "tasks-group_tasks-item-user", "task_id", "0");
    
    //очищаю выбор
    $("select[name=group], select[name=task_type]").val("");
    $("input[type=radio]").each(function(i, k) {
        $(k).attr("checked", false);
    });

    var taskType = null;

    //когда выбран тип задания
    $("select[name=task_type]").change(function() {
        $("#tasks-lesson,#tasks-test,#tasks-practik").hide();
        taskType = $(this).val();
        if (taskType != "not") {
            $("#tasks-" + taskType).fadeIn(500);
        }
    });

    //когда выбрано задание
    $("input[type=radio]").change(function() {
        $("#tasks-group").fadeIn(500);
    });


    //Выбрана группа
    $("select[name=group]").change(function() {
        var id = $(this).val();
                
        if (id !== "group")  
            $("input[name=save]").attr('disabled', false);
            
    });//select

    // Save task
    $("input[name=save]").click(function() {
        var task = null;

        //задание
        $("input[type=radio]").each(function(i, k) {
            if ($(k).attr("checked") == "checked") {
                task = $(k).val();
            }
        });
                
        var group = $("select[name=group]").val();        

        if (group == "group") {
            alert("Не все поля заполнены!");
            return false;
        }
        var postData = "task=" + task + "&type=" + taskType + "&group=" + group;

        $.ajax({
            url: "/ajax_send/tasks-set/",
            async: false,
            cache: false,
            type: "POST",
            data: postData,
            success: function(result) {
                if (result == 1) {
                    location.replace("/admin_tasks/");
                }
            }
        });//end ajax
    });//end save




    //------------------------ Готовые задания-------------------

    
    //удалить задание            
    $(".del-task").live('click', function() {
        
        if (!confirm("Удалить задание?"))
            return false;
        var id = $(this).attr('task_id');
        $.ajax({
            url: "/ajax_send/task_delete/",
            type: "POST",
            async: false,
            data: "id=" + id,
            success: function(result) {
                $(".tasks-group_tasks-item[task_id=" + id + "]").hide(900);
            }
        });
    });

    //в модальное окно проверка практического заданияы
    $(".g-row .user-practik .viewbtn").live('click', function() {
        var taskID = $(this).attr("taskid");
        var userID = $(this).attr("uid");


        do_ajax("/ajax_getmodal/check_practik/", null, function(res) {
            $("#ajax").html(res);
            modal_init(400);
            var uname = $(".g-row .user-practik[uid=" + userID + "] > div:first").html();
            var data = "taskID=" + taskID + "&userID=" + userID;
            $("#pr_ckeck #username").html(uname);

            //загрузка ответов пользователя
            do_ajax_json("/ajax_send/get_user_practik_to_admin/", data, function(response) {
                $.each(response.files.files, function(i, k) {
                    var owner = "";
                    if(k.owner !== undefined){
                        owner = k.owner;                        
                    }
                    
                    $("#files").append("<div class='file'><a title='"+k.id+"' href='/index/dn/" + k.path + "/" + k.url + "/'>" + k.url.substr(4,2)+"."+ k.url.substr(2,2)+"."+  k.url.substr(0,2) + ", "+ k.url.substr(6,2)+":"+ k.url.substr(8,2)+":"+ k.url.substr(10,2)+"</a>"+ "<span class='bgred'> "+owner+"</span></div>");
                });
                $("#ajax #comment").html(response.comment);
                $("#ajax input[name=userID]").val(response.userID);
                $("#ajax input[name=taskID]").val(response.taskID);

                //ставим оценку
                $("button[name=send_mark]").click(function() {
                    var mark = $("select[name=mark]").val();
                    var userID = $("#ajax input[name=userID]").val();
                    var taskID = $("#ajax input[name=taskID]").val();
                    var data = "mark=" + mark + "&userID=" + userID + "&taskID=" + taskID;
                    do_ajax("/ajax_send/set_practik_mark", data, function(result) {
                        message("Оценка сохранена!", "green");
                    });
                })
            })
        });

        return false;//чтобы не сворачивалось окно
    });//--проверка задания

    //#-----ЗАГРУЗКА ИНФОРМАЦИИ О ЗАДАНИИ (ПОЛЬЗОВАТЕЛИ)--------
    $(".get-task").live('click', function() {
        var tid = $(this).attr('task_id');
        var type = $('.tasks-group_tasks-item-user[task_id=' + tid + ']').attr('type');
        var th = $(this);
        //запрос данных задания
        do_ajax_json('/ajax_send/get_task_info/', 'tid=' + tid, function(result) {
            //расфасовка..
            th.addClass("f-bu-success");
            $(".tasks-group_tasks-item-user[task_id=" + tid + "]").html("");

            $.each(result.data, function(i, k) {
                var div = '<div class="g-row user';
                if (type == 'practik') {
                    div += '-practik';
                }
                div += ' no_left_margin" uid="' + k.uid + '" taskid="' + tid + '"';
                if (type == 'practik') {
                    div += 'noslide="1"';
                }
                div += '>';
                div += '<div class="g-2 g-ie mitem">' + k.user + '</div>';//user name
                if (k.file == '1') {
                    div += '<div class="g-05 g-ie bgreen" ind="' + tid + '_' + k.uid + '">'
                } else {
                    div += '<div class="g-05 g-ie" ind="' + tid + '_' + k.uid + '">'
                }

                if (k.status == 'not_executed') {
                    div += '<img src="/assets/images/icon/cancel.png" height="19"/>';
                } else if (k.status == 'try_retask') {
                    div += '<img src="/assets/images/icon/qw_mark.png" height="19"/>';
                } else if (k.status == 'retask') {
                    div += '<img src="/assets/images/icon/!.png" height="19"/>';
                } else if (k.status == 'wait') {
                    div += '<img src="/assets/images/icon/pencil.png" height="19"/>';
                } else {
                    div += '<img src="/assets/images/icon/ok.png" height="19"/>';
                }
                div += '</div><div class="g-2 g-ie">';//дата
                if (k.date !== null) {
                    div += k.date;
                } else {
                    div += '---';
                }
                div += '</div><div class="g-1 g-ie cntr">';//оценка                
                if (type == 'lesson' || 1==1) {
                    mark = k.mark == null ? "" : k.mark;
                    div += '<input class="g-05" type="text" name="mark" ind="' + tid + '_' + k.uid + '" value="' + mark + '"/>';
                    if (k.temp_mark !== null) {
                       div += '<span title="пересдача">(' + k.temp_mark + ')</span>';
                    }
                } else {
                    if (k.mark !== null) {
                        div += '<span title="текущая оценка">' + k.mark + '</span>';
                        if (k.temp_mark !== null) {
                            div += '<span title="пересдача">(' + k.temp_mark + ')</span>';
                        }
                    } else {
                        div += '<span>-</span>';
                    }
                }

                div += '</div>';
                div += '<button class="g-025 f-bu viewbtn" taskid="'+tid+'" uid="'+k.uid+'">v</button>';
                if (k.status == 'try_retask') {//кнопка подтверждения пересдачи
                    div += '<div class="g-025 g-ie cntr">';
                    div += '<img class="f-bu ok_retask" ind="' + tid + '_' + k.uid + '" tid="' + tid + '" uid="' + k.uid + '" src="/assets/images/icon/!.png" height="19" title="'+k.count_retask+'"/></div>';
                }
                if (k.status == 'retasked' || k.temp_mark !== null) {//подтвердить или отменить результат пересдачи теста
                    div += '<div class="g-2 g-ie cntr">';
                    div += '<img class="f-bu g-025 ch_mark" act="1" tid="' + tid + '" uid="' + k.uid + '" src="/assets/images/icon/ok.png" height="19"/>';
                    div += '<img class="f-bu g-025 ch_mark" act="0" tid="' + tid + '" uid="' + k.uid + '" src="/assets/images/icon/cancel.png" height="19"/>';
                }
                div += '<img src="/assets/images/icon/del-u.png" class="g-025 del-user" title="Удалить пользователя из задания" tid="' + tid + '" uid="' + k.uid + '"/>';
                div += '</div>';


                $(".tasks-group_tasks-item-user[task_id=" + tid + "]").append(div);
            });
        });
        return false;
    });


    //удаляем пользователя из задания
    $(".del-user").live('click', function() {
        if (!confirm("Удалить пользователя из задания?"))
            return false;
        var uid = $(this).attr('uid');
        var tid = $(this).attr('tid');
        var data = "uid=" + uid + "&tid=" + tid;

        do_ajax("/ajax_send/del-pr_user/", data, function() {
            $("div.user-practik:has(div[ind=" + tid + "_" + uid + "])").hide(300);
        })
        return false;
    });
    
    //#------------------ЗАГРУЗКА В ГРУППЫ ЗАДАНИЯ--------------------------------
    
    $(".group_slide").click(function(){
        
        //ставим минус на развернутый список
        if($(this).html()=="+"){$(this).html('-');}else{$(this).html('+');}
                             
        var grID = $(this).attr("group_id");
        
        //чтобы при развороте списка не грузилось каждый раз
        if($(this).attr('loaded')!=='1'){
            $(this).attr('loaded',1);//статус, что уже загрузили данные
            load_tasks(grID);
        }

    });
    
    //#--при смене предмета - ставим возможность загрузить задания--
    $("input[name=predmet]").change(function(){
        $(".group_slide").removeAttr('loaded');
    });
    

    //# --------ПЕРЕСДАЧА ТЕСТОВ-------
    //Разрешить пользователю пересдачу
    $('.ok_retask').live('click', function() {
        var uid = $(this).attr('uid');
        var tid = $(this).attr('tid');
        var data = "uid=" + uid + "&tid=" + tid;
        do_ajax("/ajax_send/confirm_teacher_retask/", data, function() {
            $("div[ind=" + tid + "_" + uid + "] img").attr("src", "/assets/images/icon/!.png");
            $("img[ind=" + tid + "_" + uid + "]").hide(400);

        });
        return false;
    });

    //подтвердить/отменить оценку
    $('.ch_mark').live('click', function() {
        var action = $(this).attr('act');
        var uid = $(this).attr('uid');
        var tid = $(this).attr('tid');
        var data = "uid=" + uid + "&tid=" + tid + "&action=" + action;
        do_ajax("/ajax_send/check_mark/", data, function() {
            $("div[ind=" + tid + "_" + uid + "] img").attr("src", "/assets/images/icon/ok.png");
            $(".ch_mark").hide(400);
        });
        return false;
    })


    //редактирование задания(добавить юзеров)
    $(".edit-task").live('click', function() {
        var task_id = $(this).attr('task_id');
        do_ajax("/ajax_getmodal/share_post/", null, function(res) {
            $("#ajax").html(res);
            $("button[name=share_btn]").html("Добавить");
            modal_init(400);

            //выбор группы
            $("select[name=group]").change(function() {
                var id = $(this).val();

                do_ajax_json("/ajax_send/task-get-users/", "id=" + id, function(data) {

                    $("#share_lessons #tasks-users").html("<div class='g-4 table_header'>Список пользователей:</div>");
                    
                    $("#share_lessons #tasks-users").append("<div><span id='checkall' class='f-bu' title='Выделить всех'>Все</span></div>");
                    button_init();
                    
                    $.each(data.data, function(i, k) {
                        $("#share_lessons #tasks-users").append("<div><input type='checkbox' name='id' value='" + k.uid + "'/>" + k.surname + " " + k.name + " </div>")
                    });
                    
                });//end ajax
            });//end select

            //добавление юзеров в задание
            $("button[name=share_btn]").live('click', function() {
                get_checkbox_values();//ids = ид пользователей
                var postData = "ids=" + ids + "&task_id=" + task_id;
                do_ajax("/ajax_send/add-users-task/", postData, function(res) {
                    message("Добавлено!", "green");
                });
            });

        });
    });//end


    //изменить статус практического задания: открыто/закрыто
    $("select[name=status]").live('change', function() {
        var status = $(this).val();
        var userID = $("#ajax input[name=userID]").val();
        var taskID = $("#ajax input[name=taskID]").val();
        var data = 'status=' + status + "&tid=" + taskID + "&uid=" + userID;
        if (status == "")
            return false;
        do_ajax('/ajax_send/practik_status/', data, function() {
            message("Сохранено", "green");
        });
    });
    
    //Ставим оценку
    $("input[name=mark]").live('change',function(){
        if(window.confirm("Сохранить оценку?")==false) return false;
        var data =  $(this).attr('ind') + "_" + $(this).val();
        do_ajax('/ajax_send/set_lesson_mark/',"data="+data, function(){
            message("Сохранено", "green");
        });
        
    });
    

});

