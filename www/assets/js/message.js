function get_messages(){
    $("#ms-body #text").html("");
    var from = $("#messager select").val()==undefined ? 1 : $("#messager select").val();
    var data = "from="+from;
    do_ajax_json("/ajax_send/get_messages/", data, function(res){
        $.each(res.messages,function(i,k){
            var message = "<p>"+k.from+"("+k.data+"): "+k.text+"</p>";
            $("#ms-body #text").prepend(message);
        })
    });
}

//проверка количества новых сообщений
function count_messages(){
    //если админ - проверяю сообщение и размечаю выпадающий список пользователей
    if($("#messager select").val() != undefined){
        do_ajax_json("/ajax_send/cout_messages/","type=admin",function(res){
            //убираю звездочки перед обновлением данных
            $.each($(".ms-red"),function(i,k){
                $(k).remove();                
            })
            //ставлю звездочки на юзеров, от которых есть сообщения
            if(res != null){
                $.each(res.users, function(i,k){
                    $("#messager select optgroup option[value="+k+"]").append("<span class='ms-red'>*</span>");
                });
            }
            
        });
        do_ajax("/ajax_send/cout_messages/"," ",function(res){
            $("#ms-count").html("["+res+"]");        
        })
    }else{
        //иначе проверяю только количество новых сообщений
        do_ajax("/ajax_send/cout_messages/"," ",function(res){
            $("#ms-count").html("["+res+"]");        
        })
    }
    
    //Если окошко открыто - обновляю сразу и сообщения
    if($("#messager").attr('status')==1){
        
        get_messages();
    }
    
}
$(document).ready(function(){
    //если нет месседжера - закругляемся.
    if($("#messager").html()==undefined)return false;
    //проверка количества сообщений
    count_messages();
    
    //Сообщения
    $("#messager").click(    
        function(){
        
            var status = $(this).attr('status');
            if(status==0){
                get_messages();          
                //раскрываю
                $(this).animate({
                    bottom:0, 
                    opacity:1
                }, 400);
                $(this).attr('status',1);
            }else{                
                //сворачиваю
                $(this).animate({
                    bottom:-287, 
                    opacity:0.5
                }, 400);
                $(this).attr('status',0);
            }
        
        
        });    
    $("#ms-body textarea,#ms-body #send").click(function(){
        return false; 
    });
    //отправляю сообщение
    $("#send").click(function(){        
        var text = $("#write textarea[name=message]").val();
        text = text.replace(/&[0-9a-z]+;/gi,'');
        var to = $("#messager select").val()==undefined ? 1 : $("#messager select").val();
        var data="text="+text+"&to="+to;
        $("#write textarea[name=message]").val("");        
                
        do_ajax("/ajax_send/send_message/",data,function(){
            get_messages();
        });
    })

    //кнопка проверить почту
    $("#ms-reload").click(function(){        
        count_messages();
        return false;
    });
    //чтобы не сворачивалось на селекте!
    $("#messager select").click(function(){
        return false;
    })
    //выбор пользователя в списке
    $("#messager select").change(function(){
        get_messages();    
        return false;
    });
    
    //удаление истории переписки
    $("#ms-delete").click(function(){
        if(confirm("Удалить все сообщения??")==false)return false;
        var from = $("#messager select").val()==undefined ? 1 : $("#messager select").val();
        
        do_ajax("/ajax_send/delete_messages/","from="+from,function(){                    
        });
    });
    
    
});
