/* РЕгистрация */

function validation(){
    //удаляю все пометки
    del_classes();
        
    //проверяю пустые поля
    check_empty_fields();
    

    for_password();
    //проверяю совпадение паролей
    check_password_fields();
    
    
 

}

//валидация эмейла
function email_valid(email){
    var expr = /^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$/;
    var result = expr.test(email);
    if(!result){
        $('div.f-input:has(input[name=email]) span').removeClass("ok-field").addClass("error-field").html("Введите полностью адрес с '@'");
    }
    return result;
}

function login_valid(login){    
    var expr =/^[a-zA-Zа-яА-ЯёЁ\і\І0-9]+$/;
//    var expr = /.\s./;
    var result = expr.test(login); 
    if(!result){
        $('div.f-input:has(input[name=login]) span').removeClass("ok-field").addClass("error-field").html("В логине не должно быть пробелов и других символов типа (+_!’№;%:?*!)");
        return false;
    }
    return result;
}

function register_needemail(data){
    if(data =="0"){
        $("div.f-row:has(input[name=email])").remove();
    }
}

//спрятать все знаки формы
function del_classes(){
    $("span.status").each(function(i,k){
        $(k).removeClass("error-field ok-field").html("");
    })
}

//проверить все поля, чтобы были не пустые
function check_empty_fields(){
    //проверяю, не пустые ли поля
    $(":input").each(function(i,k){
        if($(k).val()==""){            
            var name = $(k).attr('name');
            $("div.f-input:has(input[name="+name+"]) span").removeClass("ok-field").addClass("error-field");
        }else{
            //очищаю пробелы
            $(k).val($.trim($(k).val()));
            
            var name = $(k).attr('name');
            $("div.f-input:has(input[name="+name+"]) span").removeClass("error-field").addClass("ok-field");
        }
    });
        
    var sel = $("select").val();
    if(sel=="no"){
        $("div.f-input:has(select[name=group]) span").removeClass("ok-field").addClass("error-field");
    }else{
        $("div.f-input:has(select[name=group]) span").removeClass("error-field").addClass("ok-field");
    }
    
    //email
    email_valid($("input[name=email]").val());
    //login
    login_valid($("input[name=login]").val());
}
//пароли совпадают?
function check_password_fields(){
    var p1 = $("input[name=password]").val();
    var p2 = $('input[name=password1]').val();

    if(p1!="" && p2!="" && p2==p1){
        $("div.f-input:has(input[name=password]) span").removeClass("error-field").addClass("ok-field");
        $('div.f-input:has(input[name=password1]) span').removeClass("error-field").addClass("ok-field");
    }else{
        $("div.f-input:has(input[name=password]) span").removeClass("ok-field").addClass("error-field");
        $('div.f-input:has(input[name=password1]) span').removeClass("ok-field").addClass("error-field").html("Несовпадение паролей");
    }
}

//правила--------------
function for_login(){
    var login = $("input[name=login]").val();
    if(login.length<4){
        $('div.f-input:has(input[name=login]) span').removeClass("ok-field").addClass("error-field").html("Длина не меньше 4х символов");
    }else if(!login_valid(login)){        
        return false;
    }
    else{
        $.ajax({
            url: '/ajax_send/register_checkvalidation/',
            cache: false,
            async: false,
            data: "name="+login,
            type: "POST",
            success: function(data){
                if(data=="1"){
                    $('div.f-input:has(input[name=login]) span').removeClass("ok-field").addClass("error-field").html("Логин занят! Выберите другой");
                }else{
                    $('div.f-input:has(input[name=login]) span').removeClass("error-field").addClass("ok-field");
                }
            }
        });
        
    }
}
function for_email(){
    var email = $("input[name=email]").val();
    
    if(email==""){
        $('div.f-input:has(input[name=email]) span').removeClass("ok-field").addClass("error-field").html("");
        return false;
    }else if(email_valid(email)==false){       
        return false;
    }
    $.ajax({
        url: '/ajax_send/register_checkvalidation/',
        cache: false,
        async: false,
        data: "email="+email,
        type: "POST",
        success: function(data){
            if(data=="1"){
                $('div.f-input:has(input[name=email]) span').removeClass("ok-field").addClass("error-field").html("Пользователь с таким email существует! Попробуйте восстановить пароль, если зарегистрированны!");
            }else{
                $('div.f-input:has(input[name=email]) span').removeClass("error-field").addClass("ok-field");
            }
        }
    });
        
    
}
function for_password(){
    var login = $("input[name=password]").val();
    if(login.length<6){
        $('div.f-input:has(input[name=password]) span, div.f-input:has(input[name=password1]) span').removeClass("ok-field").addClass("error-field").html("Длина не меньше 6-ти символов");
    }else{
        $('div.f-input:has(input[name=password]) span').removeClass("error-field").addClass("ok-field");
    }
}
$(document).ready(function(){
    get_config("need_email",register_needemail);
    
    // перед отправкой
    $("#regbtn").click(function(){
        
        validation();
        var err = $("span.error-field").html();
        
        //удаление пробелов трим                
        if(err==null){            
            var data = $(":input").serialize();

            $.ajax({
                url: "/ajax_send/register/",
                cache: false,
                type: "POST",
                data: data,
                dataType:"jsone",
                complete: function(text,k){

                    $(".f-horizontal").fadeOut(400);
                    $(".f-message").addClass('f-message-success').html("Регистрация завершена, но ее должен подтвердить преподаватель!");
                    setTimeout(function(){
                        //location.replace('/');
                    },4000);
                   
                }
            });
        }
            
        
    });
    
    //при изменении поля
    $("input, select").change(function(){
        validation();
    });
    
    //ajax-valid username, email
    $("input[name=email], input[name=login]").change(function(){
        for_login();
        for_email();

    });
    
});
