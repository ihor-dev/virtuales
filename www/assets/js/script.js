//AJAX-ЗАПРОС
function do_ajax(url, data, callback, visual) {
    if (url == "" || data == "")
        return false;
    if (visual == "")
        visual = "0";
    $.ajax({
        url: url,
        type: 'POST',
        async: true,
        data: data,
        cache: false,
        success: function(res) {
            loading(0);
            callback(res);
        },
        beforeSend: function() {
            if (visual == "1")
                loading(1);
        },
        error: function() {
            loading(0);
            is_auth();
            //message("Ошибка передачи данных!","red");
        }
    });//ajax//
}
/**
 *ajax с json-ответом
 */
function do_ajax_json(url, data, callback, visual) {
    if (url == "" || data == "")
        return false;
    $.ajax({
        url: url,
        type: 'POST',
        async: true,
        data: data,
        dataType: "json",
        cache: false,
        success: function(res) {
            loading(0);
            callback(res);
        },
        beforeSend: function() {
            if (visual == "1")
                loading(1);
        },
        error: function() {
            loading(0);
            is_auth();
            //message("Ошибка передачи данных!","red");
        }
    });//ajax//
}
//записать настройки
function get_config(name, callback) {
    if (name == "")
        return false;

    $.ajax({
        url: "/ajax_config/get_config",
        type: 'POST',
        async: true,
        data: "name=" + name,
        success: callback
    });//ajax//
}
//is_auth?? modal_auth
function is_auth() {
    var uid = $("input[name=myid]").val();
    var btn = $("#auth-submit-btn").html();
    if (btn != null)
        return false;
    do_ajax('/ajax_send/islogged', "i=" + uid, function(res) {
        if (res == "0") {
            alert("Внимание! Время авторизации вышло! Для продолжения авторизуйтесь и повторите действие!");
            do_ajax('/ajax_getmodal/modal-auth', null, function(res) {
                $("#ajax").html(res);
                modal_init(400);
                return false;
            });
        }
    });
}

//Извлечь настройки
function set_config(name, value, callback) {
    if (name == "" || value == "")
        return false;
    $.ajax({
        url: "/ajax_config/set_config",
        type: 'POST',
        async: true,
        data: "name=" + name + "&value=" + value,
        success: callback
    });//ajax//
}

/**
 * функция заменяет основные спецсимволы и вырезает остальные
 */
function special_chars(str) {
    if (str == null)
        return false;
    str = str.replace(/&nbsp;/g, ' ');
    str = str.replace(/&raquo;/g, '"');
    str = str.replace(/&laquo;/g, '"');
    str = str.replace(/(&\w+;)/g, '');

    return str;
}
/*
 * Подгрузка контента с помощью ajax-запроса
 */
function load_content(id, href) {
    var exclusive = $.ajax({
        url: href,
        global: false,
        type: 'GET',
        async: false,
        dataType: 'json'
    }).responseText;
    exclusive = $.parseJSON(exclusive);

    for (var key_i in exclusive) {
        if (typeof(exclusive[key_i]) != 'object') {
            set_content($('#' + id + ' [content="' + key_i + '"]'), exclusive[key_i]);
        } else {
            for (var key_j in exclusive[key_i]) {
                set_content($('#' + id + ' [content="' + key_i + '_' + key_j + '"]'), exclusive[key_i][key_j]);
            }
        }
    }
}
/**
 *Вывод сообщения справа вверху. Подключить blockUI.js!!!
 *text - сообщение
 *color - фон
 *position - true/false
 */
function message(text, color, position) {
    if (text == "")
        var text = "Сообщение не указано!";
    if (color == "")
        var color = "#000";

    $.blockUI({
        message: text,
        fadeIn: 300,
        fadeOut: 500,
        timeout: 2500,
        showOverlay: false,
        centerX: false,
        centerY: false,
        css: {
            width: '450px',
            top: '10px',
            left: '',
            right: '10px',
            border: 'solid 2px #ccc',
            padding: '12px',
            font: '20px Arial',
            backgroundColor: color,
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 10,
            color: '#fff'
        }
    });
}

/**
 *Слайдер. Передается параметры:
 * parentclass - клик-класс
 * childclass  - слайд-класс
 * initstatus - нужно ли сворачивать при старте
 * atr - атрибут с уникальным ид для доступа.
 * Для клик-класса и слайд-класса нужно задать одинаковые уник-иды 
 * + для слайд-класса параметр: slide="0"
 */
function slider(parentclass, childclass, atr) {

    $("." + parentclass).live('click', function() {
        var id = $(this).attr(atr);
        var slide = $("." + childclass + "[" + atr + "=" + id + "]").attr('slide');

        if (slide == "1") {
            $("." + childclass + "[" + atr + "=" + id + "]").slideDown(300).attr('slide', 0);
        } else {
            $("." + childclass + "[" + atr + "=" + id + "]").slideUp(300).attr('slide', 1);
        }

    });
    $("." + parentclass).trigger("click");
}

/**
 *подтверждение переадресации
 *1й: адрес
 *2й: сообщение
 */
function f_conf(url, msg) {
    if (!url)
        var url = '';
    if (!msg)
        var msg = 'Вы уверены?';

    if (confirm(msg)) {
        location.replace(url);
    }
    return;
}
//возвращает текущее дату время
function get_cur_time() {
    var d = new Date();
    var curr = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
    return curr;
}
/**
 * modal init
 */
function modal_init(minHeight) {
    $("#ajax").modal({
        closeClass: 'simplemodal-overlay',
        minHeight: minHeight,
        onClose: function(dialog) {
            $.modal.close();
            $("#ajax").html("");
        }
    });// END MODAL 
}

//вывод модалки или спрятать
function loading(i) {
    if (i == "1") {
        $("#loading-modal").fadeIn(300);
    } else {
        $("#loading-modal").fadeOut(300);
    }
}
//отправляет запрос, что пользователь оналйн
function i_online() {
    var id = $("input[name=myid]").val();
    if(id != undefined)
    	do_ajax('/ajax_send/ionline/', "i=" + id, function() {
    });
}

function show_menu() {
    var status = $("#show-menu").attr('rel');

    if (status == "1") {
        $("#block-menu").fadeIn(300);
        $("#show-menu").attr('rel', 0).html("Свернуть меню");
        $(".view-shpora").css("width", "");
    } else {
        $(".view-shpora").css("width", "960");
        $("#block-menu").fadeOut(300);
        $("#show-menu").attr('rel', 1).html("Показать меню");
    }
}


$(document).ready(function() {
    i_online();
    //Если юзер авторизован - посылаем раз в 3 минуты обновление время пребывания
    if ($("input[name=myid]").val() != undefined) {
        setInterval(i_online, 1000 * 60 * 3);        
    }

    //отправка сообщения при нажатии Enter
    $("#auth-table input").live('keypress', function(e) {
        if (e.keyCode == 13) {
            $("#auth-submit-btn").trigger('click');
        }
    });


    tooltip();//active les tooltip simple

    //инициализация авторизации для мобильных телефонов
    if ($("input[name=is_mobile]").val() == 'true') {
        $("span[rel=auth]").click(function() {
            $("#auth-submit-btn").trigger('click');

        });
    } else {

    }

    //auth
    $("#auth-submit-btn").live('click', function() {
        var name = $("input[name=login]").val();
        var pass = $("input[name=password]").val();

        $(".sysinfo").fadeIn();
        $.ajax({
            url: "/ajax_send/auth",
            type: 'POST',
            async: true,
            data: "login=" + name + "&password=" + pass,
            success: function(data) {
                $("div.ajax-message").addClass("f-message");
                var is_modal = $("input[name=page-status]").val() == 'auth-page' ? 0 : 1;
                if (data == '') {
                    $("div.ajax-message").addClass("f-message-error");

                    msg = "Ошибка авторизации! Повторите ввод, а если Вы еще не зарегистрированны - зарегистрируйтесь!";
                    $("div.ajax-message").html(msg);
                    $("input[name=password]").val("");
                } else {
                    $("#auth-table").slideUp();
                    $("div.ajax-message").addClass("f-message-success");

                    msg = "Авторизация прошла успешно!";

                    setTimeout(function() {
                        //если не модалка, переадресация, иначе закрыть окно
                        if (is_modal == 0) {
                            location.replace(data);
                        } else {
                            $.modal.close();
                        }


                    }, 1500);
                    $("div.ajax-message").html(msg);
                }
                $(".sysinfo").fadeOut();
            }
        })
    });//end auth

    //показать/спрятать инфа
    $("#showinf").click(function() {
        var inf = '<embed height="370" width="180" bgcolor="#eeeeee" name="vishnu" src="http://iii.ru/static/Vishnu3.swf" wmode="window" flashvars="uuid=edc1b8fc-e028-47cc-8f7a-d22e320ce63a&disableRuOverride=1&home=48e9f8d827971a9d9dea988f6177757f&skin_color=0xFFFFFF&vertical_layout=1" type="application/x-shockwave-flash" quality="low" style=""></embed>';
        var status = $(this).attr("show");
        if (status == "1") {
            $("#inf").html("").fadeOut();
            $(this).attr("show", 0).html("Показать инфа");
        } else {
            $("#inf").html(inf).slideDown();
            $(this).attr("show", 1).html("Убрать инфа");
        }
    });//end show inf




    //управление меню
    $("#show-menu").click(function() {
        show_menu();
    });

});

