<!--Левый блок меню администратора -->
<div class="sidebar">
    <h2>Admin:</h2>
    <ul>
        <a href="/admin_lessons/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/lessons.jpg" width="22px"/>
                Лекции
            </li>
        </a>
        
        <a href="/admin_practiks/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/practik.jpg" height="22px"/>
                Практические
            </li>
        </a>
        
        <a href="/admin/test/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/adm_test.png" width="22px"/>
                Тесты
            </li>
        </a>
                        
        <a href="/admin_tasks/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/new_tasks.png" width="22px"/>
                Задания
            </li>
        </a>
        
        <a href="/admin_journal/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/journal.png" width="22px"/>
                Журнал
            </li>         
        </a>
        
        <a href="/admin_users/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/user.png" width="22px"/>
                Пользователи
            </li>
        </a>
        
        <a href="/admin_groups/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/user_group.png" width="19px"/>
                Группы
            </li>
        </a>
        
        <a href="/admin_predmet/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/predmets.png" height="22px"/>
                Предметы
            </li>
        </a>
        
        <a href="/admin_settings/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/settings.png" width="19px"/>
                Настройки
            </li>             
        </a>
        
        <a href="/admin_actions/" class="g-2">
            <li>
                <img class="navi_img" src="/assets/images/icon/statistik.png" height="22px"/>
                Статистика
            </li>
        </a>
        
    </ul>

</div>

