<!--Левый блок меню авторизованного пользователя -->
<div class="sidebar">
    <?
    //только для юзеров, не для админа!
    if (!Engine_User_API::is_admin() and !Engine_User_API::is_lab()):
        ?>
        <h2>Меню:</h2>
        <ul>
            
            <a href="/user/" >
                <li>               
                    <img class="navi_img" src="/assets/images/icon/home.png" width="22px"/>
                    Главная

                </li>
            </a>
             
            <a href="/user_task" title="ВЫБЕРИТЕ СПРАВА ПРЕДМЕТ &raquo;">
            <li >
                <img class="navi_img" src="/assets/images/icon/new_tasks.png" width="22px"/>
                Задания                 
            </li>
        </a>
           
            <a href="/user_points/">
                <li>
                    <img class="navi_img" src="/assets/images/icon/points.png" width="22px"/>
                   Табель 
                </li>
            </a>
            
            <a href="/index/schedule/">
                <li>
                    <img class="navi_img" width="22px" src="/assets/images/icon/adm_test.png">
                    Расписание
                </li>
            </a>

        </ul>
<? endif; ?>
    <h2>Meta:</h2>
    <ul>
        <!--Спецом для модера -->
<? if (Engine_User_API::is_lab()): ?>
            <a href="/schedule/">
                <li>
                    <img class="navi_img" width="22px" src="/assets/images/icon/adm_test.png">
                    Расписание
                </li>
            </a>
<? endif; ?>
        <a href="/forum/" >
            <li>
                <img class="navi_img" width="22px" src="/assets/images/icon/forum.png">
                Форум
            </li>
        </a>
        <!-- <sup class="ms-red"><i>New</i></sup> -->
      
        <a href="/user_messagelist/" >
            <li >
                <img class="navi_img" src="/assets/images/icon/messages.jpg" width="19px"/>
                Сообщения(<? Engine_User_U::msg_counter(); ?>) 
            </li>
        </a>
        
        <a href="/user_profile/">
            <li>
                <img class="navi_img" src="/assets/images/icon/user_info.png" width="19px"/>
                Личные данные 
            </li>
        </a>
        
        <a href="/auth/logout/">
            <li>            
                <img class="navi_img" src="/assets/images/icon/logout.png" width="19px"/>
                Выход
            </li>
        </a>
    </ul>

</div>

