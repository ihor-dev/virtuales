<!--Левый блок меню администратора -->
<div class="sidebar">
    <h2>Admin:</h2>
    <div>
    <a href="/admin/test/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/adm_test.png" width="22px"/>
        Тесты
    </a>
    </div>

    <div>
    <a href="/admin_lessons/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/lessons.jpg" width="22px"/>
        Лекции
    </a>
    </div>
    
    <div>
    <a href="/admin_practiks/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/practik.jpg" height="22px"/>
        Практические
    </a>
    </div>
        
    <div>
    <a href="/admin_tasks/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/new_tasks.png" width="22px"/>
        Задания
    </a>
    </div>
        
    <div>
    <a href="/admin_users/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/user.png" width="22px"/>
        Пользователи
    </a>
    </div>
        
    <div>
    <a href="/admin_groups/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/user_group.png" width="19px"/>
        Группы
    </a>
    </div>
    
    <a href="/admin_actions/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/statistik.png" height="22px"/>
        Статистика
    </a>
        
    <div>
    <a href="/admin_settings/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/settings.png" width="19px"/>
        Настройки
    </a>
    </div>
        
    <div>  
    <a href="/user_profile/" class="g-2">
        <img class="navi_img" src="/assets/images/icon/settings.png" width="19px"/>
        Личные данные
    </a>
    </div>
</div>

