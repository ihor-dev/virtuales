<!--Левый блок меню авторизованного пользователя -->
<div class="sidebar">
    <? //только для юзеров, не для админа!
    if (!Engine_User_API::is_admin()): ?>

    <div>
        <a href="/user/">
            <img class="navi_img" src="/assets/images/icon/home.png" width="22px"/>
            Главная
        </a>
    </div>
    
    <div>
        <a href="/index/schedule/">
            <img class="navi_img" width="22px" src="/assets/images/icon/adm_test.png">
            Расписание
        </a>
    </div>
    
    <div>
        <a href="/user_task/">
            <img class="navi_img" src="/assets/images/icon/new_tasks.png" width="22px"/>            
            Задания <? Engine_User_U::task_counter(); ?> 
        </a>
    </div>
    
    <div>
        <a href="/user_points/">
            <img class="navi_img" src="/assets/images/icon/points.png" width="22px"/>
            Табель 
        </a>
    </div>

    <div>
        <a href="/user_profile/">
            <img class="navi_img" src="/assets/images/icon/user_info.png" width="19px"/>
            Личные данные
        </a>
    </div>

    <? endif; ?>
    <div>
        <a href="/user_messagelist/" class="g-2">
            <img class="navi_img" src="/assets/images/icon/messages.jpg" width="19px"/>
            Сообщения (<? Engine_User_U::msg_counter(); ?>) 
        </a>
    </div>
    
    <div>    
        <a href="/forum/" >            
            <img class="navi_img" width="22px" src="/assets/images/icon/forum.png">
            Форум            
        </a>
    </div>
    
    <div>
        <a href="/auth/logout/" class="g-2">
            <img class="navi_img" src="/assets/images/icon/logout.png" width="19px"/>
            Выход
        </a>
    </div>
</div>

