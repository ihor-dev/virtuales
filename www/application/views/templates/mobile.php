<!DOCTYPE html>
<html>
	<head>            
	<meta name="description" content="<?php if(isset($meta_description)) echo $meta_description;?> Сервис онлайн шпаргалок. Вы можете легко создать шпаргалку и воспользоваться ею с мобильного телефона бесплатно">
                <meta name="keywords" content="Онлайн шпаргалки, создать онлайн шпаргалку бесплатно, шпаргалки бесплатно, шпаргалки для мобильного, шпаргалка">
	<meta name="viewport" content="width=240" />
                <title>
                <?php if(isset($title) AND $title !="") echo $title." >";?>
                    Система Web-образования
                </title>
		<?php // if(isset($styles)) foreach($styles  as $file) { echo "\t", HTML::style($file,  NULL, FALSE), "\n"; } ?>
                <?php if(isset($scripts)) foreach($scripts as $file) { echo "\t", HTML::script($file, NULL, FALSE), "\n"; } ?>
                <link rel="stylesheet" href="/assets/css/mobile.css"/>
                <script src="/assets/js/mobile.js" type="text/javascript"></script>
                
	</head>
	<body>
            <input type="hidden" name="is_mobile" value="true"/>
            <div id="wrapper">
                    <div id="header">
                        <h1>Virtual-es.ru</h1>
                        <? if(Engine_User_API::islogged()):?>
                                    <div id="u-title">                                     
                                            <? $u = Auth::instance()->get_user();
                                            echo $u->name . " ".$u->surname; ?>                                        
                                    </div>
                        <?endif;?>
                           <div id="block-menu">
                                    <?php if( Engine_User_Api::is_admin() ):?>
                                            <?php echo View::factory('blocks/mobile/admin-menu');?>
                                    <?php endif;?>
                                    <?php if( ! Engine_User_Api::islogged() ):?>
                                            <?php echo View::factory('blocks/mobile/left-menu-unlogin');?>
                                    <?php else:?>
                                            <?php echo View::factory('blocks/mobile/left-menu');?>
                                    <?php endif;?>
                                <!-- INF block -->

                            </div>
                    </div>
                    <div id="content" >
                        <?php if(isset($content)) echo $content;?>
                    </div>
                    <div id="footer" class="g-row">
                        <div class="content">
                                <?php echo date("Y");?>&copy;<i>"Система онлайн-образования"</i>
                        </div>
                        <div class="f-row cntr">
                                <i>Developing version</i>
                        </div>
                    </div>
            </div>
            <!--Before ajax -->
            <div id="ajax"></div>
	</body>
</html>