<?header('Expires: Sat, 03 Aug 2013 00:00:00 GMT');
header('Last-Modified: ' . gmdate( 'D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<!--doctype html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="ru"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="ru"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="ru"><![endif]-->
<!--[if gt IE 8]><!--><html lang="ru"><!--<![endif]-->
	<head>            
                    <LINK REL="SHORTCUT ICON" HREF="favicon.ico" TYPE="image/x-icon">
                    <link rel="icon" type="image/x-icon" href="favicon.ico">
                    <meta name="description" content="<?php if(isset($meta_description)) echo $meta_description;?> Виртуальная система обучения. Удаленный доступ к теоретическим, тестовым и практическим учебным материалам студентам Липковатовского аграрного колледжа">
                    <meta name="keywords" content="Виртуалис, Виртуалес, virtuales.ru, virtual-es.ru, Виртуальная система обучения">
                    <meta http-equiv="Cache-Control" content="no-cache">
                    <title>
                        <?php if(isset($title) AND $title !="") echo $title." -";?>Система Web-обучения
                    </title>
                    <?php if(isset($styles)) foreach($styles  as $file) { echo "\t", HTML::style($file,  NULL, FALSE), "\n"; } ?>
                    <?php if(isset($scripts)) foreach($scripts as $file) { echo "\t", HTML::script($file, NULL, FALSE), "\n"; } ?>
                    <!--[if lt IE 9]>
                        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
                    <![endif]-->
	</head>
    <body>
    <!--[if IE]>
    <div class="f-message f-message-error cntr"><img src="/assets/images/icon/exclamation-octagon-frame.png" width="20"/>  Вы используете программу для скачивания браузеров (Internet Explorer). Пожалуйста, установите один из следующих браузеров:
    <a href="http://www.opera.com/download/get.pl?id=35479&thanks=true&sub=true" target="_blank">Opera</a>,
    <a href="http://www.mozilla.org/ru/firefox/new/" target="_blank">FireFox</a>,
    <a href="https://www.google.com/intl/ru/chrome/browser/" target="_blank">Google Chrome</a> или
    <a href="http://biblprog.org.ua/ru/safari/" target="_blank">Safari</a> для корректной работы сайта!</div>    
    <![endif]-->     
    <? if(Engine_User_API::islogged()):?><input name="myid" type="hidden" value="<?= Engine_User_U::uid();?>"/><?endif;?>
    <div id="page">
    
            <div id="sup-wrapper"></div>
            <div id="wrapper" class="g">
                    <div id="header" class="g-row">
                            <div class="g-12 g-ie header " >			
                                <img src="/assets/images/logo.jpg" class="pngie" />
                                <? if(Engine_User_API::islogged()):?>
                                    <div id="u-title">                                     
                                            <? $u = Auth::instance()->get_user();
                                            echo $u->name . " ".$u->surname; ?>                                        
                                    </div>
                                <?endif;?>
                            </div>
                        <span id="show-menu">Свернуть меню</span>
                    </div>
                    <div id="content" class="g-row">
                            <div id="block-menu" class="g-3 g-ie bg">
                                    <?php if( Engine_User_Api::is_admin() ):?>
                                            <?php echo View::factory('blocks/admin-menu');?>
                                    <?php endif;?>
                                    <?php if( ! Engine_User_Api::islogged() ):?>
                                            <?php echo View::factory('blocks/left-menu-unlogin');?>
                                    <?php else:?>
                                            <?php echo View::factory('blocks/left-menu');?>
                                    <?php endif;?>
                                <!-- INF block -->
                                
                                <? if(!Request::user_agent('mobile')):?>
                                <div class="inf_wrapper">
                                    <div id="inf" class="hide">
                                        
                                    </div>
                                    <button id="showinf" class="f-bu f-bu-default cntr">Показать инфа</button>
                                </div>
                                <? endif;?>
                                

                            </div>
                            <div id="block-content" class="g-9 g-ie bg">
                                    <?php if(isset($content)) echo $content;?>
                            </div>

                    </div>                              
                    <div id="footer" class="g-row">
                            <div class="g-12 bg">
                                    <div class="content f-row cntr">
                                            <?php echo "2012-". date("Y");?>&copy;<i>"Система онлайн-обучения"</i>
                                    </div>
                                    <div class="f-row cntr">
                                            <i>VirtualES v1.0</i>
                                    </div>

                            </div>
                    </div>
            </div>
            <!--Before ajax -->
            <div id="ajax"></div>
            <!--loading -->
            <div id="loading-modal">
                <div id="transparent-div"></div>
                <img src="/assets/images/load.gif"/>
            </div> 
            <!-- Panel-->
            <?php echo View::factory('blocks/panel');?>
           
            
    </div><!-- //page-->
    
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter23250784 = new Ya.Metrika({id:23250784}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/23250784" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->        
               
    <script type="text/javascript" src="/assets/js/panel.js"></script>	
    </body>
</html>
