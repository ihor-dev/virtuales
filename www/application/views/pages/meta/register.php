<div id="register">
    <div class="title">Регистрация</div>
    <p class="f-message">Заполните, пожалуйста, все поля!</p>

    <div class="f-message f-message-error message">

    </div>

    <div class="f-horizontal">
        <div class="f-row">
            <label>Email:</label>
            <div class="f-input"><input class="g-5" type="text" name="email"><span class="status"></span></div>
        </div>
        <div class="f-row">
            <label>Логин:</label>
            <div class="f-input">
                <input class="g-5" type="text" name="login"><span class="status"></span>
                <p class="f-input-help">Это имя будет использоваться для авторизации</p>
            </div>
        </div>
        <div class="f-row">
            <label>Пароль:</label>
            <div class="f-input"><input class="g-5" type="password" name="password"><span class="status"></span></div>
        </div>
        <div class="f-row">
            <label>Подтвердить пароль:</label>
            <div class="f-input"><input class="g-5" type="password" name="password1"><span class="status"></span></div>
        </div>
        <br/>
        <div class="f-row">
            <label>Фамилия:</label>
            <div class="f-input"><input class="g-5" type="text" name="famil"><span class="status"></span></div>
        </div>

        <div class="f-row">
            <label>Имя:</label>
            <div class="f-input"><input class="g-5" type="text" name="name"><span class="status"></span></div>
        </div>

        <div class="f-row">
            <label>Отчество:</label>
            <div class="f-input"><input class="g-5" type="text" name="otch"><span class="status"></span></div>
        </div>

        <div class="f-row">
            <label>Группа:</label>
            <div class="f-input">
                <select name="group" class="g-5">
                    <option value="no">выберите группу</option>
                    <? foreach ($groups as $gr): ?>
                        <option value="<?= $gr['id']; ?>"><?= $gr['name']; ?></option>
                    <? endforeach; ?>
                </select>
                <span class="status"></span>
            </div>
        </div>

        <div class="f-row">

            <div class="f-input"><input id="regbtn" class="g-5 f-bu f-bu-default" type="submit"  value="Регистрация"></div>
        </div>

        

    </div>

</div>