<!-- Восстановление пароля -->
<div id="password-recovery">
	<div class="title">
		Восстановление утеряного пароля:
	</div>
	<?php if(isset($data['error'])):  
		switch($data['error']){
			case "1":
				$data['part'] = 'form';
				$msg = "Пользователь с указанным email не существует! <br/>Повторите ввод.";
			break;
			case "passwords":
				$msg = "Пароли не совпадают";
			break;
		}?>
	<div class="f-message f-message-error">
		<?=$msg?>
	</div>
	<?php endif;?>
	
	<?php if( isset($data['part']) AND $data['part'] == 'form'):?>
	<!-- форма восстановления -->
	<div class="recovery-form">
		
		<form action="/auth/passrecovery" method="POST">
			<div class="f-row">
				
				<label>Введите Ваш email:</label>
				<div class="f-input">
					<input type=text name=password>
				</div>
			</div>
			
			<div class="f-actions">
				<input type="submit" class="reg-subm f-bu g-3" name="send" value="Отправить">
			</div>
		
		</form>
	</div>
	
	<?php elseif( isset($data['part']) AND $data['part'] == 'result'): ?>	
	<!-- ======== результат ========= -->
	
	<div id="recovery-result">
		<div class="f-message">
			На Ваш адрес элктронной почты отправлено письмо. Перейдите по ссылке.
		</div>
	</div>
	
	<?php elseif( isset($data['part']) AND $data['part'] == 'newpassword'): ?>
	<!-- ===== форма ввода нового пароля ========= -->
	
	<div class="recovery-form">
		
		<form action="/auth/recovery" method="POST">
			<div class="f-row">
				
				<label>Введите новый пароль:</label>
				<div class="f-input">
					<input type=password name=password>
				</div>
			</div>
			<div class="f-row">
				
				<label>пароль еще раз:</label>
				<div class="f-input">
					<input type=password name=password1>
				</div>
			</div>
			
			<div class="f-actions">
				<input type="submit" class="reg-subm f-bu" name="pwrdadd" value="Отправить">
			</div>
		
		</form>
	</div>
	
	<?php elseif( isset($data['part']) AND $data['part'] == 'complete'): ?>
	<!-- успешно восстановлен -->
		<div class="f-message f-message-success">
			Ваш новый пароль сохранен!
		</div>
	<?php endif;?>
</div>