<div class="title">Табель оценок </div>
Cистема оценивания: 
<select name="mark-system" class="g-2">
    <option value="12">12 бальная</option>
    <option value="5">5 бальная</option>    
</select>
<!--Список групп, в которых состоит юзер -->
    <div id="groups">
         <button class="group" rel="all">Все</button>
    <? foreach($groups as $gr):?>
        <button class="group dinamic" rel="<?=$gr['id'];?>"><?=$gr['name'];?></button>
    <? endforeach;?>
    </div>
<div class="user_points">
    <div class="g-row table_header padding-10">
        <div class="g-4 g-ie green">Название</div>
        <div class="g-1 g-ie ">Тип</div>
        <div class="g-05 g-ie red">Бал</div>
        <div class="g-2 g-ie blue">Дата</div>
        <div class="g-05 g-ie">Действия</div>
    </div>
    <? foreach ($data as $point):  ?>
    <div rel="<?=$point['grid'];?>" class="g-row padding-10 mitem practik-items point <? if($point['result']>9){echo "bggreen";}elseif(($point['result']>6) AND ($point['result']<10)){echo "bgyellow";}elseif(($point['result']>3) AND ($point['result']<7)){echo "bgorange";}else{echo "bgred";}?>">       
            <div class="g-4 g-ie green"><?= $point['name']; ?></div>
            <div class="g-1 g-ie">
                <? switch($point['type']){
                    case('test'): echo "Тест";break;
                    case('practik'): echo "Практич."; break;                    
                    case('lesson'): echo "Конспект"; break;
                } ?>
            </div>
            <div class="g-05 g-ie red c-mark" rel="<?=$point['grid'];?>"><?= $point['result']; ?></div>
            <div class="g-2 g-ie blue"><?= $point['date']; ?></div>
            <div class="g-05 g-ie">
                <?if($point['type']=='test'){?><span class="f-bu f-bu-default click" rel="<?=$point['task_id'];?>" title="Запрос на пересдачу теста">П</span><?}?>
            </div>
        </div>
    <? endforeach; ?>
</div>
<br/>
<div id="sr-bal">Прогноз оценки за семестр: </div>