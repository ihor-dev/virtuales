<!--Создание и редактирование расписания -->
<div class="write-schedule">
    <?php if (isset($data[0])): extract($data[0]);endif; ?>        
    <input type='hidden' name='id' value="<?= $id; ?>">    
    <textarea name="text" class="g-9" id="text_editor">
        <? if (isset($text))echo $text; ?>
    </textarea>
    <span class="f-bu f-bu-success g-4" id="upd-sbmt-edit-form">Обновить</span>
    <span class="f-bu f-bu-warning g-4" id="clear-text">Очистить</span> 
    <br/><br/>
    <div class='f-message f-message-success'>
    <b>Инструкция:</b><br/>
    <ul>
    <li>Нажать кнопку "Очистить"</li>
    <li>скопировать таблицу в Word-документе</li>
    <li>поместить курсор в поле ввода</li>
    <li>нажать комбинацию клавишь: Ctrl + V</li>
    <img src='/assets/images/ctlV.jpg'/>
    <li>нажать кнопку "Обновить"</li>
    <li>Рсписание сохранено. Закрыть браузер.</li>
    </ul>
    </div>


</div>
<div class="message f-message f-message-success">
    Ожидайте, сохранение записи может занять некоторое время...<br/>
    Пожалуйста, не перезагружайте страницу до полного сохранения!!!
    <img class="loading" src="/assets/images/icon/loading.gif"/>
</div>