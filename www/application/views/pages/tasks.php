<h1 class="title cntr"></h1>
<div id="toolbar" class="g-row">
    <div class="g-1 g-ie bar-button bar-active cntr" rel="0">Все</div>
    <div class="g-2 g-ie bar-button  cntr" rel="lesson">Лекции</div>
    <div class="g-2 g-ie bar-button cntr" rel="test">Тесты</div>
    <div class="g-3 g-ie bar-button cntr" rel="practik">Лабораторные работы</div>
</div>

<div class="bar-pages">
    
    <div class="">
        <?foreach($data as $el): //['type'], ['group']'?>
                <div class="g-row practik-items mitem" type='<?=$el['type'];?>'>
                    <div class="g-4 g-ie">
                        <?if( $el['type'] == 'lesson'):?><img src="/assets/images/icon/lessons.jpg"/> <a href="/user_read/<?= $el['lesson_id'];  ?>_<?= $el['id']; if(is_null($el['task_complete'])) echo "_get"; ?>"><?= $el['task_name'];  ?></a><?endif;?>
                        <?if( $el['type'] == 'test'):?><img src="/assets/images/icon/adm_test.png"/> <a href="<?if($el['task_complete'] =='complete'||$el['task_complete'] =='wait' || $el['task_complete'] == 'try_retask'):?>#<?endif;?>/test/index/<?= $el['test_id']; ?>_<?= $el['id']; if(is_null($el['task_complete'])) echo "_get"; ?>"><?= $el['task_name']; ?></a><?endif;?>
                        <?if( $el['type'] == 'practik'):?><img src="/assets/images/icon/practik.jpg"/> <a href="/user_practik/<?= $el['practik_id']; ?>_<?= $el['id']; if(is_null($el['task_complete'])) echo "_get"; ?>"><?= $el['task_name']; ?></a><?endif;?>
                    </div>
                    <div class='g-1 g-ie'><?= $el['group']; ?></div>
                    <div class='g-4 g-ie'>                    
                    <? if($el['task_complete'] =='not_executed'):?>
                    	<img src='/assets/images/icon/cancel.png'/>
                    	<small>не выполнено..</small>
                    <? elseif($el['task_complete'] =='complete'):?>
                    	<img src='/assets/images/icon/ok.png'/>
                    	<small>выполнено, проверено!</small>
                    <? elseif($el['task_complete'] == 'wait'):?>
                    	<img src='/assets/images/icon/!.png'/>
                    	<small>ждем проверку..</small>
                    <? elseif($el['task_complete'] == ''):?>
                    	<img src='/assets/images/icon/notread.png'/>
                    	<small>новое задание!</small>
		   			<? elseif($el['task_complete'] == 'try_retask'):?>
                    	<img src='/assets/images/icon/!.png'/>
                    	<small>ожидаем подтверждения преподавателем</small>
                   	<? elseif($el['task_complete'] == 'retask'):?>
                    	<img src='/assets/images/icon/retask.png'/>
                    	<small>можно пересдать!</small>
                    <? endif; ?>
                    </div>
                </div>
        <?endforeach;?>
    </div>
   
</div>
