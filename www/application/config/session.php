<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'cookie' => array(
		'encrypted' => FALSE,
	),
         'native' => array(
            'name' => 'session_name',
            'lifetime' => Date::HOUR*4,
        ),
);
