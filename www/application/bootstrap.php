<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/kohana/core' . EXT;

if (is_file(APPPATH . 'classes/kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Europe/Kiev');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'ru_RU.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}
/**
 * Устанавливаю место хранения сессии в своей папке. Папка хранится в корне.
 */
session_save_path($_SERVER['DOCUMENT_ROOT']."/sessions_directory");

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
    'base_url' => '/',
    'index_file' => FALSE,
    'errors' => true,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * cookie для "запомнить"
 */
Cookie::$salt = 'fdsh-tretgd-re-gfds-gd-erg-fdg-';

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
    'auth' => MODPATH . 'auth', // Basic authentication
    // 'cache'      => MODPATH.'cache',      // Caching with multiple backends
    // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'orm' => MODPATH . 'orm', // Object Relationship Mapping
    'email' => MODPATH . 'email', // Email
        // 'unittest'   => MODPATH.'unittest',   // Unit testing
        // 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
Route::set('ajax', 'ajax_<action>/<page>(/<id>)', array('page' => '.*?', 'id' => '.*?'))
        ->defaults(array(
            'controller' => 'ajax',
            'action' => 'action',
        ));

//расписание
Route::set('schedule', 'schedule(/<action>(/<id>))', array('action' => '.*?', 'id'=> '.*?'))
        ->defaults(array(
            'controller' => 'moderator',
            'action' => 'index',
        ));

//force login
Route::set('forcelogin', 'a=<id>-<code>', array('code' => '.*?', 'id'=> '.*?'))
        ->defaults(array(
            'controller' => 'auth',
            'action' => 'forcelogin',
        ));

//Авторизация
Route::set('auth', 'auth/(<action>(/<id>(_<uid>)))', array('id' => '.*?', 'uid' => '.*?'))
        ->defaults(array(
            'controller' => 'auth',
            'action' => 'index',
        ));
//регистрация
Route::set('register', 'register(_<id>)', array('id' => '1|2|3|4'))
        ->defaults(array(
            'controller' => 'auth',
            'action' => 'register',
        ));

//тестирование
Route::set('test', 'test/(<action>(/<id>(_<task>)))', array('id' => '.*?', 'task' => '.*?'))
        ->defaults(array(
            'controller' => 'test',
            'action' => 'index',
        ));

//Кабинет пользователя
Route::set('user', 'user(_<action>(/<id>(_<task_id>)))', array('id' => '.*?', 'task_id' => '.*?'))
        ->defaults(array(
            'controller' => 'user',
            'action' => 'index',
        ));

//категории
Route::set('category', 'category/(<action>(/<page>))', array('page' => '.*?'))
        ->defaults(array(
            'controller' => 'category',
            'action' => 'page',
        ));


//Adminka-main
Route::set('adminka', 'admin(_<action>(/<id>))', array('action' => '.*?','id'=>'.*?'))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'admin',
            'action' => 'index',
        ));
//Adminka-all
Route::set('admin', 'admin/<controller>(/<action>(/<id>))', array('id' => '.*?'))
        ->defaults(array(
            'directory' => 'admin',
            'controller' => 'controller',
            'action' => 'index',
        ));


Route::set('default', '(<controller>(/<action>(/<id>)))', array('id' => '.*?'))
        ->defaults(array(
            'controller' => 'index',
            'action' => 'index',
        ));
