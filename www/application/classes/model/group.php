<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Group extends ORM {

    protected $_table_name = 'groups';

//
//    protected $_has_many = array(
//        'user' => array(
//            'model' => 'user',
//            'foreign_key' => 'group_id',
//        )
//    );
#======================================================================
    /**
     *  Возвращает данные по всем группам
     * Если передать параметр, то выдаст массив типа $array['id'] = value;
     * TODO::запрос групп, которые разрешил админ для реги
     * @return array все группы
     */
    public function get_all($param = null) {
        $db = ORM::factory('group')->where('id', '>', 0)->order_by('oid')->find_all();
        $data = $temp = array();

        if ($param == null) {
            foreach ($db as $item) {
                $temp['id'] = $item->id;
                $temp['name'] = $item->name;
                array_push($data, $temp);
            }
        } else {
            foreach ($db as $item) {
                $data[$item->id] = $item->name;
            }
        }

        return $data;
    }

    //название группы по ид
    public function get_name($id = null) {
        if ($id == null)
            return false;
        $db = ORM::factory('group', $id);
        return $db->name;
    }

    /**
     *  Обновить группы
     * @param type $data
     * @return type месседж
     */
    public function upd($data = null) {
        if ($data == null OR count($data) < 1)
            return 'no data';

        DB::update('groups')->value('name', $data['name'])->where('id', '=', $data['grID'])->execute();
        return 'обновлено';
    }

    /**
     *  Добавить новую группу
     * @param type $data имя
     * @return type статус
     */
    public function _add($data = null) {
        if ($data == null)
            return 'no data';

        $db = ORM::factory('group')
                ->set('name', $data['name'])
                ->save();
        return $db->_saved;
    }

    //Удалить группу
    //@TODO: УДАЛЯТЬ ИЗ ЮЗЕР-ГРУПС ЗАПИСИ!!!!!!!
    public function _delete($id = null) {
        if ($id == null)
            return 'no data';

        echo DB::delete('groups')->where('id', '=', $id)->execute();
    }

    //id всех групп
    public function get_all_ids() {
        $data = array();
        $sql = "SELECT id FROM `groups` WHERE 1";
        $result = DB::query(Database::SELECT, $sql)->execute();
        foreach ($result as $id) {
            $data[] = $id['id'];
        }
        return $data;
    }

    /**
     * Добавляем юзеров  в группу
     * @param type $array gr_id, users_id:string
     * @return type error
     */
    public function add_users($array = null) {
        if ($array == null)
            return (array('error' => 'no data'));
        //$array['grID'];

        $ids = explode("_", rtrim($array['ids'], "_"));

        foreach ($ids as $id) {
            $this->add_usertogroup($array['grID'], $id);
        }
    }

    //добавляю в группу юзера
    public function add_usertogroup($grid = null, $uid = null) {
        if ($grid == null OR $uid == null)
            return false;
        $sql = Engine_User_API::insert_unique('user_groups', array('group_id' => $grid, 'user_id' => $uid, 'default' => '1'));
        DB::query(Database::INSERT, $sql)->execute();
    }

    /**
     * Пользователи группы
     * @param type $grID ид группы
     * @return array массив данніх юзеров
     */
    public function get_groupusers($grID = null) {
        if ($grID == null)
            return array('error' => 'no group id');
        $sql = "Select name, surname, users.id as id from users, user_groups WHERE user_groups.group_id = $grID AND users.id = user_groups.user_id";
        $res = DB::query(Database::SELECT, $sql)->execute();
        $data = array();
        foreach ($res as $el) {
            $data[$el['id']] = $el['surname'] . " " . $el['name'];
        }
        return $data;
    }

    /**
     * Возвращает список ид-групп, в которых состоит юзер
     * @param type $uid user id
     * @return array массив ид-групп юзера
     */
    public function get_usergroups($uid = null, $param = null) {
        if ($uid == null)
            return array('error' => 'no user id');
       
        //добавляем имена групп
        if ($param == 'name') {
            $sql = "Select group_id, groups.name as name FROM user_groups, groups WHERE group_id = groups.id AND user_id = $uid";
            $res = DB::query(Database::SELECT, $sql)->execute();
            $data = array();

            foreach ($res as $el) {
                $temp = array();
                $temp['id'] = $el['group_id'];
                $temp['name'] = $el['name'];
                array_push($data, $temp);
            }
        } else {
            //Просто массив ids
            $sql = "Select group_id FROM user_groups WHERE user_id = $uid";
            $res = DB::query(Database::SELECT, $sql)->execute();
            $data = array();

            foreach ($res as $el) {
                $data[] = $el['group_id'];
            }
        }
        return $data;
    }
    
    /**
     * удалить пользователя с группы
     * @param type $uid - user id
     */
    public function del_userfromgroup($uid = null, $group_id = null){
        if($uid == null OR $group_id == null) return 'no user or group id';
        $sql = "Delete from user_groups WHERE user_id =$uid AND group_id = $group_id";
        DB::query(Database::DELETE, $sql)->execute();
    }

}

