<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Forum extends ORM {

    protected $_table_name = 'forums';

    #======================================================================

    /**
     * Новый форум
     * @param 
     */
    public function _new($data = null) {
        if ($data == null)
            return 'no data';

        $db = ORM::factory('forum')
                ->set('name', $data)
                ->save();
    }

    //все форумы
    public function get_all() {
        $db = ORM::factory('forum')->find_all();
        $data = array();
        foreach ($db as $el) {
            $temp = array();
            $temp['id'] = $el->id;
            $temp['name'] = $el->name;
            array_push($data, $temp);
        }
        return $data;
    }

    /**
     * Название форума по ид
     */
    public function get_name($fid = null) {
        if ($fid == null)
            return false;
        $db = ORM::factory('forum', $fid);
        return $db->name;
    }

    /**
     * Удаление форума
     */
    public function _del($fID = null) {
        if (!Engine_User_API::is_admin())
            return false;
        $ids = Model::factory('topic')->get_allids($fID);
        foreach ($ids as $id) {
            Model::factory('topic')->_del($id);
        }
       
        $sql = "Delete FROM forums WHERE id =$fID";
        return $db = DB::query(Database::DELETE, $sql)->execute();
    }

}

//end

