<? # MODEL CATEGORY # ?>
<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Category extends ORM {

    protected $_table_name = 'categories';
    protected $_has_many = array(
        'posts' => array(
            'model' => 'post',
            'foreign_key' => 'category_id',
        ),
    );

#------------------------------------------------------------------------------------------------

    /**
     * Статический метод вывода категорий 
     */
    public static function get_cat_names() {
        $db = ORM::factory('category')->where('id', '>', '0')->order_by('name')->find_all();
        $data = $arr = array();

        foreach ($db as $el) {
            $arr = array('id' => $el->id, 'name' => $el->name);
            array_push($data, $arr);
        }
        return $data;
    }

    /**
     * Имя категории по ид 
     */
    public static function get_cat_name($id = null) {
        if ($id == null)
            return false;

        $db = ORM::factory('category', $id);
        return $db->name;
    }

    /**
     * id категории по имени
     */
    public static function get_cat_id($name = null) {
        if ($name == null)
            return false;
        $db = ORM::factory('category', $name);
        return $db->id;
    }

    /**
     * добавление новой категории
     */
    public function add_category($name = null) {
        if ($name == null)
            return false;

        $db = ORM::factory('category');
        $db->name = trim($name);
        $db->save();
        return true;
    }

    public function update_category($data = null) {
        if ($data == null)
            return false;

        for ($i = 0; $i < count($data['id']); $i++) {

            $db = ORM::factory('category', $data['id'][$i]);
            $db->name = $data['name'][$i];
            $db->update();
        }
    }

    /**
     * Удаление категории
     * TODO: при удалении, все записи категории - в стандартную категорию!
     */
    public function del_category($id=null) {
        if ($id == null)
            return false;

        $db = ORM::factory('category', $id);
        if ($db->loaded())
            $db->delete();
        return true;
    }

}
