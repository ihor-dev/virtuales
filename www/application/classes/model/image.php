<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Image extends ORM {

    protected $_table_name = 'images';

#======================================================================

    /**
     * Загрузка изображений в галерею
     * @TODO sdf
     */
    public function upload_file($gal_id = null) {        
        $fileElementName = 'qqfile';

        //имя нового изображения
        $rand_name = date('YmdHis') . rand(1, 99) . ".jpg";
        $new_image = $_SERVER['DOCUMENT_ROOT'] . "/assets/upload/" . $rand_name;
        $thumbnail = $_SERVER['DOCUMENT_ROOT'] . "/assets/upload/mini/" . $rand_name;

        move_uploaded_file($_FILES[$fileElementName]['tmp_name'], $new_image);
        //Ресайз имаги до 800х600
        Image::factory($new_image)->resize(800, 600, Image::AUTO)->save($new_image, 80);
        //Миниатюра
        Image::factory($new_image)->resize(150, 150, Image::AUTO)->save($thumbnail, 80);

        //регистрирую имагу в БД
        $this->add_image($gal_id, $rand_name);
        //return $rand_name;
        return json_encode(array('success'=>true));
    }

    public function add_image($gal_id = null, $img_name = null) {
        if ($gal_id == null OR $img_name == null)
            return false;

        $db = ORM::factory('image')->set('file', $img_name)->save();
        $sql = "Insert INTO galleries_images (`id`, `gallery_id`, `image_id`) VALUES(NULL, '" . $gal_id . "', '" . $db->id . "')";
        DB::query(Database::INSERT, $sql)->execute();
    }

    public function del_image($id = null) {
        if ($id == null)
            return false;

        $db = ORM::factory('image', $id);
        $name = $db->file;
        if ($db->loaded()) {
            $db->delete();
            $filename = $_SERVER['DOCUMENT_ROOT'] . "/assets/upload/" . $name;
            $filemini = $_SERVER['DOCUMENT_ROOT'] . "/assets/upload/mini/" . $name;
            @unlink($filename);
            @unlink($filemini);
        }
    }

}

