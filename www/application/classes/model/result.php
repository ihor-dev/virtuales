<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Result extends ORM {

    protected $_table_name = 'results';
    protected $_belongs_to = array(
        'quetion' => array(
            'model' => 'quetion',
            'foreign_key' => 'quetion_id',
        )
    );

#======================================================================
    /**
     * Подсчет результата теста, запись в бд
     */

    public function result() {
        //id теста
        $task_id = Session::instance()->get('task_id');

        //User id
        $uid = Auth::instance()->get_user()->id;
        //Набранные баллы
        $point = $this->get_point();
        //ключи вопросов
        $qw_ids_str = Model::factory('test')->get_qw_ids();

        $qw_ids = explode(" ", $qw_ids_str);
        //макс бал
        $max_point = $this->count_max_bal($qw_ids);
        //Оценка
        $result = round(($point / $max_point) * 12);

        $data = array(
            'task_id' => $task_id,
            'result' => $result,
            'user_id' => $uid
        );

        //запись оценки
        $need_report = $this->add_result($data);

        //удаление задание, отметка о том, что выполнено задание
        //если это первая сдача, а не пересдача
        if($need_report == 'yes'){
            Model::factory('taskuser')->report_task($task_id);
        }
        return $result;
    }

    /**
     *  Сохраняет в БД оценку
     * @param array $data массив данных
     * @return type false
     */
    public function add_result($data = null) {
        if ($data == null)
            return false;

        extract($data);

        //статус задания
        $status = $this->get_task_type($task_id, $user_id);

        //в случае пересдачи теста..
        if ($status == "retask") {
            //оценку во временное поле
            $db = ORM::factory('result')->where('task_id', "=", $task_id)->and_where('user_id', '=', $user_id)->find();
            $db->temp_result = str_replace(".", "", $result);
            $db->save();
            
            //статус и количество перездач
            $db1 = ORM::factory('taskuser')->where('task_id', "=", $task_id)->and_where('user_id', '=', $user_id)->find();
            $db1->count_retask +=1;
            $db1->status = "retasked";
            $db1->save();
            
            return 'no';
            
        } else {

            $db = ORM::factory('result')->where('task_id', "=", $task_id)->and_where('user_id', '=', $user_id)->count_all();
            if ($db == "0") {
                //save mark
                ORM::factory('result')
                        ->set('task_id', $task_id)
                        ->set('result', $result)
                        ->set('user_id', $user_id)
                        ->save();
            } else {
                //update mark
                $db = ORM::factory('result')->where('task_id', "=", $task_id)->and_where('user_id', '=', $user_id)->find();
                $db->result = $result;
                $db->save();
            }
        }
        return 'yes';
    }

    //Status задания
    public function get_task_type($tid = null, $uid = null) {
        $db = ORM::factory('taskuser')->where('task_id', '=', $tid)->and_where('user_id', '=', $uid)->find();
        return $db->status;
    }

    /**
     *  Возвращает маскимальное количество баллов пройденных тестов
     * @param type $ids - список вопросов
     * @return int макс бал
     */
    public function count_max_bal($ids = null) {
        if ($ids == null OR !is_array($ids))
            return false;
        $max_point = null;

        foreach ($ids as $id) {
            $max_point += ORM::factory('quetion', $id)->bal;
        }
        return $max_point;
    }

    /**
     * возвращает количество набранных баллов
     */
    public function get_point() {
        return Session::instance()->get('point');
    }

    /**
     * табель пользователя
     */
    public function get_results($uid = null) {
        if ($uid == null)
            return false;

        $sql = "SELECT result, results.id as result_id, results.task_id as task_id, task_users.date as date,
                CASE type 
                WHEN 'test' THEN (SELECT name from tests WHERE tests.id =  tasks.test_id)
                WHEN 'practik' THEN (SELECT name from practiks WHERE practiks.id =  tasks.practik_id)
                WHEN 'lesson' THEN (SELECT name from posts WHERE posts.id =  tasks.lesson_id)
                END as name, type, groups.id as grid, groups.name as grname
                FROM `tasks`, `results`, `task_users`, `groups` WHERE 
                tasks.id = results.task_id AND
                tasks.id = task_users.task_id AND
                task_users.user_id = results.user_id AND
                groups.id = tasks.group_id AND
                results.user_id = $uid
                Order by results.id DESC";

        $result = DB::query(Database::SELECT, $sql)->execute();
        $data = array();
        foreach ($result as $res) {
            array_push($data, $res);
        }
        return $data;
    }
    
    
    //перенос оценки при ретаске
    public function retask_result($data = null){
        extract($data);
        
        if($action == "1"){
            //если надо сохранить новую оценку
            $db = ORM::factory('result')->where('user_id','=',$uid)->and_where("task_id", "=", $tid)->find();
            $db->result = str_replace(".", "", $db->temp_result);
            $db->temp_result = null;
            $db->save();
        }else{
            //оставляю старую оценку. удаляю новую
            $db = ORM::factory('result')->where('user_id','=',$uid)->and_where("task_id", "=", $tid)->find();
            $db->temp_result = null;
            $db->save();
        }
        //сбиваю статус
        $db1 = ORM::factory('taskuser')->where('user_id','=',$uid)->and_where("task_id", "=", $tid)->find();
        $db1->status = "complete";
        $db1->save();
        
    }
    
    
    //удаление результатов юзера
    public function delete_results($uid = null){
        if($uid == null) return false;
        $sql = "DELETE FROM results WHERE user_id = $uid";
        
        return DB::query(Database::DELETE, $sql)->execute();
    }

}

