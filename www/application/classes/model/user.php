<?php

defined('SYSPATH') or die('No direct script access.');

class Model_User extends Model_Auth_User {

    /**
     * Я онлайн
     */
    public function ionline($uid = null) {
        if ($uid == null)
            return false;
        $time = time();
        $sql = "Update users Set `last_login` = $time Where `id` = $uid";
        DB::query(Database::UPDATE, $sql)->execute();
    }

    /**
     * Юзер онлайн? юзер считается онлайн, если время последней активности не более 3х минут
     */
    public function is_online($uid=null) {
        if ($uid == null)
            return false;

        $last_time = ORM::factory('user')->where('id', '=', $uid)->find()->last_login;
        if (((time() - $last_time) / 60) < 3) {
            $res = "1";
        } else {
            $res = date("d.m в H:i:s", $last_time);
        }
        return $res;
    }

    /**
     * Регистрация пользователя
     */
    public function register() {
        $data = Engine_User_Api::clear($_POST);
        extract($data);
        $trans_login = Engine_User_API::translit($login);
        $email = isset($email) ? $email : $trans_login . "@domen.com";

        $user = ORM::factory('user');
        $user->username = $login;
        $user->email = $email;
        $user->password = $password;
        $user->surname = $famil;
        $user->name = $name;
        $user->patronymic = $otch;
        $user->date = date("Y-m-d H:i:s");        
        $user->save();

        //add group
        Model::factory('group')->add_usertogroup($group, $user->id);

        return "1";
    }

    /**
     * отправка почты
     * $to -кому
     * $from - от кого
     * $message - сообщение
     */
    public function send_email($data = null) {
        if (!$data OR count($data) < 1)
            return 'false';
        extract($data);

        return Email::send($to, $from, $subject, $message, $html);
    }

    /**
     * Проверка мыла на существование и отправка кода восстановления
     * $to - мыло пользователя, забывшего пароль
     */
    public function rise_password($to = null) {
        If (!$to OR $to == null)
            return false;
        $db = ORM::factory('user')->where('email', '=', $to)->find();
        if (!$db->loaded())
        //user not founded
            return "01";

        $url = "http://{$_SERVER['SERVER_NAME']}/auth/recovery/" . Engine_User_Api::pwhashe($db->username) . "_" . $db->id . "/";
        $link = '<a href="' . $url . '">' . $url . '</a>';
        $data = array(
            'to' => $to,
            'subject' => 'Восстановление пароля',
            'from' => 'admin@remaind.com',
            'message' => "<b>Для восстановления пароля перейдите по ссылке: $link</b>",
            'html' => true
        );

        $this->send_email($data);
    }

    /**
     * 
     */
    public function chack_code($uid = null, $code = null) {
        if ($code == null OR $uid == null)
            return $data['error'] = 'no data';

        $id = Engine_User_Api::clear($uid, 'single');
        $db = ORM::factory('user', $id)->username;

        return Engine_User_Api::pwhashe($db) == $code ? $data['result'] = 1 : $data['result'] = 0;
    }

    /**
     *
     * @param type $uid
     * @param type $old_password
     * @param type $password
     * @return type 
     */
    public function change_password($data = null) {
        extract($data);
        if ($id == null OR $password == null OR $old_password == null)
            return false;

        $hashe = Auth::instance()->hash_password($old_password);
        $curr_pasword = Auth::instance()->get_user()->password;

        if ($curr_pasword == $hashe) {
            $this->set_password($password, $id);
        } else {
            return 'error';
        }
        return true;
    }

    //запись пароля в бд
    public function set_password($password = null, $id = null) {
        if ($password == null or $id == null)
            return false;
        $db = ORM::factory('user', $id);
        $db->password = $password;
        $db->save();
    }

    /* ------------------- Cabinet ------------------------- */


    /**
     * Валидация при регистрации мыла и юзернейма
     */
    public function register_valid($data = null) {
        if ($data == null)
            return false;
        extract($data);

        //Email valid
        if (isset($email)) {
            $res = DB::query(Database::SELECT, "SELECT `id` FROM `users` WHERE `email` = '" . $email . "'")->execute()->count();
            return $res;
        }
        //Login valid
        if (isset($name)) {
            $res = DB::query(Database::SELECT, "SELECT `id` FROM `users` WHERE `username` = '" . $name . "'")->execute()->count();
            return $res;
        }
    }
    
    /**
     * добавляем аватарку
     */
    public function ad_avatar($data = null){
        $uid = Engine_User_U::uid();
        if($data == null OR !$uid) return false;        
                
         $fileElementName = 'qqfile';

        //имя нового изображения
        $name =  $uid.".jpg";
        $new_image = $_SERVER['DOCUMENT_ROOT'] . "/assets/files/avatar/" . $name;

        move_uploaded_file($_FILES[$fileElementName]['tmp_name'], $new_image);
        //Ресайз имаги до 800х600
        $img = Image::factory($new_image);
        if($img->width >400 OR $img->height >400){
            $img->resize(400, 400, Image::AUTO)->save($new_image, 90);
        }
       
        return json_encode(array('success'=>true));
    }
    
    /**
     * Ресайз аватарки
     */
    public function resize_avatar($data = null){
        if($data == null) return false;
        
        $uid = Engine_User_U::uid();
        $name =  $uid."-min.jpg";        
        $thumbnail = $_SERVER['DOCUMENT_ROOT'] . "/assets/files/avatar/" . $name;
        $image = $_SERVER['DOCUMENT_ROOT'] . "/assets/files/avatar/".$uid.".jpg";
        extract($data);
        //Миниатюра
        Image::factory($image)->crop($w, $h ,$x, $y)->resize(100, 100, Image::AUTO)->save($thumbnail, 100);
    }

    /* ------------------- ADMIN ------------------------- */

    /**
     * Список заявок на регистрацию
     */
    public function get_new_users() {
        
        //$sql = "SELECT * FROM `users` WHERE `id` NOT IN (SELECT `id` FROM `users` JOIN `roles_users` ON (`user_id` = `id`) AND ( `role_id` = '1'))";        
        $sql = "SELECT users.id as uid, surname, users.name as uname, patronymic, date, groups.name as grname FROM `users` 
        JOIN user_groups ON  (users.id = user_id) JOIN groups ON (group_id = groups.id)
        WHERE users.`id` NOT IN 
        (SELECT `id` FROM `users` JOIN `roles_users` 
        ON (`user_id` = users.`id`) 
        AND ( `role_id` = '1'))";

        $res = DB::query(Database::SELECT, $sql)->execute();
        $data = $temp = array();
        foreach ($res as $result) {            
            $temp['id'] = $result['uid'];
            $temp['surname'] = $result['surname'];
            $temp['name'] = $result['uname'];
            $temp['patronymic'] = $result['patronymic'];
            $temp['date'] = $result['date'];
            $temp['gr'] = $result['grname'];
            array_push($data, $temp);
        }
        return $data;
    }

    //Удалить заявки на регистрацию
    public function delete_new_users($ids = null) {
        if ($ids == null)
            return false;
        $ids = trim($ids);
        $arr_ids = explode(" ", $ids);

        foreach ($arr_ids as $id) {
            $this->delete_user($id);
        }
    }

    //активировать заявки на регистрацию
    public function active_new_users($ids = null) {
        if ($ids == null)
            return false;
        $ids = trim($ids);
        $arr_ids = explode(" ", $ids);

        foreach ($arr_ids as $id) {

            DB::query(Database::INSERT, "INSERT INTO `roles_users` (`user_id`,`role_id`) VALUES ($id,1)")->execute();
        }
    }

    /**
     * возвращает пользователей c группами
     * возвращает всех, кроме Админа. Админ должен быть с ИД = 1!!!!!!!!!!
     */
    public function get_users() {
        //группы
        $gr_ids = Model::factory('group')->get_all();
        $data = $temp = $gr_temp = $group = array();

        foreach ($gr_ids as $gr) {            
            $sql = "SELECT users.id as id, users.name as name, surname, patronymic, logins, last_login, date FROM `users` 
                        JOIN `roles_users` ON (`roles_users`.`user_id` = `id`) AND ( `role_id` = '1') AND (`roles_users`.`user_id` != '1') 
                        JOIN `user_groups` ON (`user_groups`.user_id = users.id) 
                        JOIN `groups` ON (`group_id`= {$gr['id']}) AND (group_id = groups.id)  
                        ORDER BY `surname`";
            $db = DB::query(Database::SELECT, $sql)->execute();

            $group = array();
            foreach ($db as $el) {
                $tmp['name'] = $el['name'];
                $tmp['surname'] = $el['surname'];
                $tmp['patronymic'] = $el['patronymic'];
                $tmp['logins'] = $el['logins'];
                $tmp['uid'] = $el['id'];
                $tmp['last_login'] = $el['last_login'];
                array_push($group, $tmp);
            }
            $gr_temp['id'] = $gr['id'];
            $gr_temp['name'] = $gr['name'];
            $gr_temp['count'] = count($group);
            $gr_temp['users'] = $group;
            array_push($data, $gr_temp);
        }

        return $data;
    }

    /**
     * Полные данные юзеров по ид группы
     * $id = ид группы
     */
    public function get_group_users($id = null) {
        if ($id == null)
            return false;

                    $sql = "SELECT users.id as id, users.name as name, surname, patronymic, logins, last_login, date FROM `users` 
                        JOIN `roles_users` ON (`roles_users`.`user_id` = `id`) AND ( `role_id` = '1') AND (`roles_users`.`user_id` != '1') 
                        JOIN `user_groups` ON (`user_groups`.user_id = users.id) 
                        JOIN `groups` ON (`group_id`= $id) AND (group_id = groups.id)  
                        ORDER BY `surname`";
        $db = DB::query(Database::SELECT, $sql)->execute();

        $group = array();
        foreach ($db as $el) {
            $tmp['name'] = $el['name'];
            $tmp['surname'] = $el['surname'];
            $tmp['patronymic'] = $el['patronymic'];
            $tmp['logins'] = $el['logins'];
            $tmp['uid'] = $el['id'];
            array_push($group, $tmp);
        }
        return array('data' => $group);
    }

    /**
     * удаляет пользователя
     */
    public function delete_user($id = null) {
        if ($id == null)
            return false;

        //1)оценки
        Model::factory('result')->delete_results($id);
        //2) задания юзера
        Model::factory('taskuser')->delete_results($id);
        //3) файлы юзера
        Model::factory('file')->delete_user_files($id);
        //4) комментарии юзера
        Model::factory('comment')->del_comments($id);
        //5) сообщения и топики юзера в форуме
        Model::factory('topic')->on_userdelete($id);
        
        //6) удаляю привязки к группам user_groups
        $sql="Delete FROM user_groups Where user_id = $id";
        DB::query(Database::DELETE, $sql)->execute();
        
        //7) #TODO сообщения юзера 
        
        //пользователя
        return ORM::factory('user', $id)->delete();
    }

    // информация о юзере по ид
    public function get_user($id = null) {
        if ($id == null)
            return false;
        
        $sql = "Select * FROM `users` WHERE users.id = $id ";
        $db = DB::query(Database::SELECT, $sql)->execute();        
        $img = $_SERVER['DOCUMENT_ROOT'] . "/assets/files/avatar/".$id.".jpg";
        
        $u = $db[0];
        $data['id'] = $u['id'];
        $data['name'] = $u['name'];
        $data['email'] = $u['email'];
        $data['surname'] = $u['surname'];
        $data['patronymic'] = $u['patronymic'];
        $data['img'] = file_exists($img) ? $id : 'no';
             
        return $data;
    }

    /**
     * Возвращает логин юзера по заданому ид
     * @param type $id ид пользователя
     * @return type string логин юзера
     */
    public function get_login($id = null) {
        if ($id == null)
            return false;
        $db = Model::factory('user')->where('id', '=', $id)->find();
        return $db->username;
    }

    /**
     * Обновление инфы о юзере
     * @param type $data 
     */
    public function upd_user_info($data=null) {
        if ($data == null)
            return false;
        $db = Model::factory('user')->where('id', '=', $data['id'])->find();
        $db->name = $data['name'];
        $db->surname = $data['surname'];
        $db->patronymic = $data['patronymic'];
        $db->email = $data['email'];
        $db->save();
    }

    //При авторизации переадресация юзера в зависимости от роли
    public function role_redirect() {
        $admin = Engine_User_API::is_admin();
        $lab = Engine_User_API::is_lab();
        if ($admin) {
            return "/admin";
        } elseif ($lab) {
            return "/schedule";
        } else {
            return "/user";
        }
    }

    /**
     * Возвращает массив всех юзеров, где ключ есть ид, значение - фамилия,имя.
     * @return string Массив[id] = name surname
     */
    public function users_array() {
        $db = Model::factory('user')->where('id', '>', 0)->Order_by('surname')->find_all();
        $users = array();
        foreach ($db as $u) {
                $users[$u->id] = $u->surname . " " . $u->name;
        }
        return $users;
    }
    
    public function all_users(){        
        $sql = "Select users.id as id, users.name as name, surname, groups.name as gr 
        FROM `users` 
        JOIN user_groups ON  (users.id = user_id) AND (`user_groups`.`default` = '1')
        JOIN groups ON (group_id = groups.id)";
        $db = DB::query(Database::SELECT, $sql)->execute();
        
        $data = array();
        foreach($db as $el){
            $temp= array();           
            $temp['name'] = $el['name'];
            $temp['surname'] = $el['surname'];
            $temp['group'] = $el['gr'];
            $data[$el['id']] = $temp;
            
        }
        return $data;
    }

    /**
     * кто был сегодня на сайте???
     */
    public function visitors($time=null) {
        if ($time == null)
            return false;
        //сутки
        $sql = "SELECT users.id, surname, users.name, patronymic, last_login as time, groups.name as `group` FROM users, groups, user_groups
            WHERE
            user_groups.group_id = groups.id AND
            users.id = user_groups.user_id AND
            users.id IN (SELECT id
            FROM `users` 
            Where last_login > (UNIX_TIMESTAMP()-$time))
            Group By users.id
            Order By last_login DESC";
        $db = DB::query(Database::SELECT, $sql)->execute();
        $users = $temp = array();
        foreach ($db as $u) {
            $temp['id'] = $u['id'];
            $temp['name'] = $u['surname'] . " " . $u['name'];
            $temp['group'] = $u['group'];
            $temp['time'] = date("d.m.Y H:i:s", $u['time']);
            $date_diff = Engine_User_API::diff(date("d.m.Y  H:i:s", $u['time']), date("d.m.Y H:i:s", time()));
            $temp['diff'] = $date_diff['hours'] . "ч." . $date_diff['minutes'] . "м.";
            array_push($users, $temp);
        }
        return $users;
    }
    
    /**
     * Who online ------------------------------------------------------------------------------------------------
     */
     public function online_users(){
        $sql = "SELECT users.id uid, users.name name, surname, patronymic, groups.name as `group`
                    FROM  `users` 
                    LEFT JOIN user_groups 
                    ON (users.id = user_id ) AND (`user_groups`.`default` = '1')
                    LEFT JOIN groups ON groups.id = group_id
                    WHERE last_login > ( UNIX_TIMESTAMP( ) -180 ) 
                    Order by surname";
       
       if($this->online_timeout()){
            $db = DB::query(Database::SELECT, $sql)->execute();               
        
            $users = $temp = $file_arr= array();
            foreach ($db as $u) {
                $temp['uid'] = $u['uid'];
                $temp['name'] = $u['name'] . " " . $u['surname'];
                $temp['group'] = $u['group'];
                $temp['photo'] = Engine_User_U::avatar($u['uid']);

                //массив для записи в файл
                $file_str = $u['uid']."#".$temp['name']."#".$u['group']."#".$temp['photo'];
                array_push($file_arr, $file_str);
                array_push($users, $temp);
            }

            $this->online_write($file_arr);
       }else{
           $users = $this->online_read();
       }
                      
        return $users;
    }
    
    //запись онлайн посетителей в файл
    private function online_write($data = null){
        if($data == null) return false;
        $path = $_SERVER['DOCUMENT_ROOT'] . "/assets/online.txt";       
        $str = join("\r\n", $data);
        $fp = fopen($path, 'w');
        fputs($fp, $str);
        fclose($fp);            
    }
    
    private function online_read(){
         $path = $_SERVER['DOCUMENT_ROOT'] . "/assets/online.txt";
         
         $fp = file($path);
         $temp = $users = array();
         foreach ($fp as $key => $val) {
             $t = explode("#", $val);
             $temp['uid'] = $t[0];
             $temp['name'] = $t[1];
             $temp['group'] = $t[2];
             $temp['photo'] = $t[3];
             array_push($users, $temp);
             $temp = array();
         }   
         return $users;
    }
    
    /**
     * Если с момента записи в файл прошло больше 60 сек
     * возвращает 1, иначе 0
     * @return type 0 or 1
     */
    private function online_timeout(){
        $time = 60; //60 sec 
        $path = $_SERVER['DOCUMENT_ROOT'] . "/assets/online.txt";
        return (time() - filemtime($path) ) > $time ? 1 : 0; 
    }
    
    

}

//End User
