<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Comment extends ORM {

    protected $_table_name = 'comments';

#======================================================================
    /**
     * Возвращает ответ к практ.заданию
     */

    public function get_comment($practikID=null, $userID=null) {
        if ($practikID == null OR $userID == null)
            return false;

        $sql = "SELECT text FROM `practik_comments`, comments WHERE
            comment_id = comments.id AND
            user_id = " . $userID . " AND
            practik_id = " . $practikID;
        $db = DB::query(Database::SELECT, $sql)->execute();
        foreach ($db as $data) {
            return $data['text'];
        }
    }
    
    /**
    * Все ответы пользователей по id задания (Проверка всех заданий сразу)
    */
    public function get_task_comments($taskID = null){
    	if($taskID == null) return array('error'=>'no task id');
 
        $sql = "SELECT text, user_id FROM `practik_comments`, comments WHERE
            comment_id = comments.id AND
            task_id = " . $taskID;
            
        $db = DB::query(Database::SELECT, $sql)->execute();
        $arr = array();
        $users = Model::factory('user')->all_users();
        
        foreach ($db as $data) {

            $temp['text'] = $data['text'];
            $temp['user'] = $users[$data['user_id']]['surname']." ".$users[$data['user_id']]['name'];
            array_push($arr, $temp);
        }
        return $arr;
            
    }

    /**
     *  Добавление комментария         
     */
    public function _add($text = null, $userID = null, $commentID = null) {
        if ($text == null OR $userID == null)
            return false;

        if ($commentID == null) {
            //СОздаю новый комментарий
            $db = ORM::factory('comment')
                    ->set('user_id', $userID)
                    ->set('text', $text)
                    ->set('date', date('d-m-Y H:i:s'))
                    ->save();
        } else {
            //обновляю существующий
            $db = ORM::factory('comment', $commentID);
            $db->text = $text;
            $db->date = date('d-m-Y H:i:s');
            $db->save();
        }
        return $db->id;
    }

    /**
     * Удалить комментарий
     */
    public function del($commentID=null) {
        if ($commentID == null)
            return false;
        
        $db = ORM::factory('comment',$commentID);
        if($db->loaded())
                $db->delete();
    }
    
    //удалить все комменты юзера
    public function del_comments($uid = null){
        if ($uid == null)
            return false;
        
        $sql = "SELECT id FROM comments WHERE user_id = $uid";
        $db = DB::query(Database::SELECT, $sql)->execute();
        
        foreach($db as $id){
            $ids[] = $id['id'];
        }
        
        if(!isset($ids)) return true;
        $str_ids = "(". join(",", $ids) .")";
        
        $sql = "DELETE FROM practik_comments WHERE comment_id IN $str_ids";
        DB::query(Database::DELETE, $sql)->execute();
        
        $sql = "DELETE FROM comments WHERE user_id = $uid";
        DB::query(Database::DELETE, $sql)->execute();
        
    }

}

