<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Config extends ORM {

    protected $_table_name = 'configs';

#======================================================================
    /**
     *  ДОбавить настройки
     * @param type $name
     * @param type $value
     * @return type 
     */

    public function set_config($name=null, $value=null) {
        if ($name == null OR $value == null)
            return false;
        //конфиг существует?
        $db = ORM::factory('config')->where('name', '=', $name)->find();

        if ($db->loaded()) { #Обновляю
            $db->value = $value;
            $db->save();
        } else {             #Новый конфиг
            ORM::factory('config')->set('name', $name)->set('value', $value)->save();
        }
    }

    /**
     * Получить конфиг
     * @param type $name имя настройки
     * @return type значение конфига
     */
    public function get_config($name=null) {
        if ($name == null)
            return false;

        $db = ORM::factory('config')->where('name', '=', $name)->find();
        if ($db->loaded())
            return $db->value;
    }

    /**
     * Проверить, конфиг занят?
     * @param type $name - имя конфига
     * @return type true/false
     */
    public function is_config($name=null) {
        if ($name == null)
            return false;

        $db = ORM::factory('config')->where('name', '=', $name)->find();
        if ($db->loaded()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *  Удалить конфиг
     * @param type $name имя
     */
    public function del_config($name=null) {
        if ($name == null)
            return false;

        if ($this->is_config($name)) {
            $db = Model::factory('config')->where('name', '=', $name)->find();
            if ($db->loaded())
                $db->delete();
        }
    }

}

