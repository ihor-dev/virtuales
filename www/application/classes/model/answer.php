<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Answer extends ORM {

    protected $_table_name = 'answers';
    protected $_belongs_to = array(
        'quetion' => array(
            'model' => 'quetion',
            'foreign_key' => 'quetion_id',
        )
    );
    protected $_has_many = array(
        'key' => array(
            'model' => 'key',
            'foreign_key' => 'quetion_id',
        ),
    );
#======================================================================
    /**
     * удалить вопрос через ajax
     * @param type $id
     * @return type 
     */
    public function remove_answer($id = null) {
        if ($id == null)
            return false;
        
        $db = ORM::factory('answer', $id);
        
        if ($db->loaded()){
            //удаляю ключи
            $key = ORM::factory('key')->where('answer_id','=',$id)->find();
            if($key->loaded()){
                $key->delete();
            }
            //удаляю вариант ответа
            $db->delete();
        }
          
        return true;
    }

    //добавить вариант ответа
    public function add_answer($data = array()) {
        if (count($data) < 1)
            return false;

        $db = ORM::factory('answer')
                ->set('content', $data['content'])
                ->set('quetion_id', $data['qw_id'])
                ->save();

        if ($data['key'] == "1")
            Model::factory('key')->add_key($data['qw_id'], $db->id);
        return true;
    }

}

