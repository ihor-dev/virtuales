<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Quetion extends ORM {

    protected $_table_name = 'quetions';
    protected $_has_many = array(
        'answer' => array(
            'model' => 'answer',
            'foreign_key' => 'quetion_id',
        ),
        'key' => array(
            'model' => 'key',
            'foreign_key' => 'quetion_id',
        ),
        'result' => array(
            'model' => 'result',
            'foreign_key' => 'quetion_id',
        ),
    );
    protected $_belongs_to = array(
        'test' => array(
            'model' => 'test',
            'foreign_key' => 'test_id',
        ),
    );
#======================================================================
    /**
     *  Возвращает массив данных вопроса по ид
     * @param type $id записи
     * @return type массив данных
     */

    public function get_quetion($id = null, $shuffle = false) {
        if ($id == null)
            return false;
        $answers = $data = array();

        $db = ORM::factory('quetion', $id);
        $quetion = $db->as_array();

        foreach ($db->answer->find_all() as $el) {
            $answers[$el->id] = $el->content;
        }

        //Перемешиваю варианты ответов
        if ($shuffle == 1) {
            $i = 1;
            //Если перемешать без бубна, то индексы пропадут, там будут цифры.
            foreach ($answers as $key => $value) {
                $data[$i]['id'] = $key;
                $data[$i]['key'] = $value;
                $i++;
            }

            shuffle($data);
            
            $answers = array();
            foreach ($data as $value) {
                $answers[$value['id']] = $value['key'];
            }
        }

        $quetion['answers'] = $answers;
        return $quetion;
    }

    /**
     * Возарвщает массив данных по нескольким вопросам
     * @param type $quetions_ids массив ид вопросов
     * @return type array
     */
    public function get_all_quetions($quetions_ids = null) {
        if ($quetions_ids == null)
            return false;
        $data = array();

        foreach ($quetions_ids as $id) {
            $data[] = $this->get_quetion($id);
        }
        return $data;
    }

    /**
     *  Проверка ответа пользователя??
     * @param type $data - POST-данные от пользователя
     * @return type 
     */
    public function check_answer($data = null) {
        if ($data == null)
            return false;
        $true = $false = 0;
        $id = $data['id'];
        $keys = explode(" ", $data['keys']);

        //Сверяю ответы пользователя с ключами.
        foreach ($keys as $kid) {
            $db = ORM::factory('key')->where('quetion_id', '=', $id)->and_where('answer_id', '=', $kid)->find();
            if ($db->loaded()) {
                $true +=1;
            } else {
                $false +=1;
            }
        }

        //общее количество правильных ответов вопроса
        $count = ORM::factory('key')->where('quetion_id', '=', $id)->count_all();

        if ($true == $count AND $false < 1) {
            echo "true";
            $this->set_point($id);
        } else {
            return "false";
        }
    }

    /**
     *  Увеличение количества баллов
     * @param type $num - количество зачисляемых баллов
     * @return type false
     */
    public function set_point($id = null) {
        if ($id == null OR !is_numeric($id))
            return false;
        $bal = ORM::factory('quetion', $id)->bal;

        $ses = Session::instance();

        $point = $ses->get('point', 0);

        $ses->set('point', $point + $bal);
    }

    /**
     *  Запись вопроса в БД + вариантов ответов
     * @param type $data
     * @return type 
     */
    public function add_answer($data = null) {
        if ($data == null)
            return false;
        $answer_id = array();
        $test_id = Session::instance()->get('test_id');

        //Вопрос
        $qw = ORM::factory('quetion')
                ->set('content', $_POST['quetion'])
                ->set('test_id', $test_id)
                ->save();


        //варианты ответов
        for ($i = 0; $i < count($_POST['variant']); $i++) {
            $answ = ORM::factory('answer');
            $answ->content = $_POST['variant'][$i];
            $answ->quetion_id = $qw;
            $answ->save();

            //если задан хоть один правильный вар. ответов
            if (isset($_POST['true'])) {

                if (in_array($i, $_POST['true']))
                    $answer_id[] = $answ;
            }
        }

        //ключи
        foreach ($answer_id as $el) {
            ORM::factory('key')
                    ->set('quetion_id', $qw)
                    ->set('answer_id', $el)
                    ->save();
        }
    }

//end
    //Редактирование вопроса
    public function update_quetion() {
        $data = $_POST;
        $true_ans_ids = array(); #массив ключей новых правильных вариантов ответов
        //Обновляю вопрос
        $qw = ORM::factory('quetion', $data['quetion_id']);
        $qw->content = $data['quetion'];
        $qw->save();

        //Обновляю варианты ответов
        for ($i = 0; $i < count($data['variant']); $i++) {

            $db = ORM::factory('answer', $data['ansid'][$i]);
            $db->content = $data['variant'][$i];
            $db->save();
        }//--end for
        //Обновляю ключи
        if ((isset($data['true'])) AND (count($data['true']) > 0)) {
            #удалить все ключи текущего вопроса
            $keys = ORM::factory('key')->where('quetion_id', '=', $qw->id)->find_all();
            foreach ($keys as $key) {
                $key->delete();
            }
            foreach ($data['true'] as $true) {

                ORM::factory('key')
                        ->set('quetion_id', $qw->id)
                        ->set('answer_id', $true)
                        ->save();
            }//endforach
        }//endif
    }

//---end update

    /**
     *  Все ключи теста
     * @param type $test_id
     * @return type 
     */
    public function get_true_keys($test_id = null) {
        if ($test_id == null)
            return 'error';
        $keys = array();
        $i = 0;
        $qw = ORM::factory('quetion')->where('test_id', '=', $test_id)->find_all();
        foreach ($qw as $el) {
            foreach ($el->key->find_all() as $id) {
                $keys[$i++] = $id->answer_id;
            }
        }
        return array('keys' => $keys);
    }

    /**
     * Удаление вопроса
     * @param type $id вопроса
     * @return type 
     */
    public function delete_quetion($id = null) {
        if ($id == null)
            return false;

        $db = ORM::factory('quetion', $id);

        //delete keys
        foreach ($db->key->find_all() as $keys) {
            $keys->delete();
        }

        //delete answers
        foreach ($db->answer->find_all() as $ans) {
            $ans->delete();
        }

        //delete quetion
        $db->delete();
        return true;
    }

}

//end Quetion