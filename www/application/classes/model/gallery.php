<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Gallery extends ORM {

    protected $_table_name = 'galleries';
#======================================================================

    /**
     *  Добавить новую группу
     * @param type $data имя
     * @return type статус
     */
    public function _add($data = null) {
        if ($data == null)
            return 'no data';

        $db = ORM::factory('gallery')
                ->set('name', $data)
                ->save();
        return $db->_saved;
    }

    public function get_all() {
        $db = ORM::factory('gallery')->where('id', '>', 0)->find_all();
        $data = $temp = array();
        foreach ($db as $item) {
            $temp['id'] = $item->id;
            $temp['name'] = $item->name;
            array_push($data, $temp);
        }
        return array('galleries' => $data);
    }

    public function upd($data = null) {
        if ($data == null)
            return 'no data';
        DB::update('galleries')->value('name', $data['name'])->where('id', '=', $data['id'])->execute();
    }

    public function del($id = null) {
        if ($id == null)
            return false;

        //получить список изображений галереи
        $ids = DB::query(Database::SELECT, "SELECT image_id as id FROM `galleries_images`
               WHERE gallery_id = $id")->execute();

        //удалить список изображений галереи
        foreach($ids as $img_id){

            Model::factory('image')->del_image($img_id['id']);
        }

        //удалить связи
        DB::delete('galleries_images')->where('gallery_id', '=', $id)->execute();
        
        //удалить галерею
        DB::delete('galleries')->where('id', '=', $id)->execute();
    }


    
    public function get_galleries(){
        $data =  $images = $temp_img = array();
        
        $galleries = $this->get_all();
        foreach($galleries['galleries'] as $gal){
            $images = array();
            $sql = "SELECT images.id as id, file, name FROM galleries_images, images
                    WHERE galleries_images.gallery_id = {$gal['id']}
                    AND galleries_images.image_id = images.id";
            $db = DB::query(Database::SELECT,$sql)->execute();
            
            foreach($db as $im){
                $temp_img = array();
                $temp_img['id']   = $im['id'];
                $temp_img['file'] = $im['file'];
                $temp_img['name'] = $im['name'];
                array_push($images, $temp_img);
            }
            $temp['gallery']= $gal;
            $temp['images'] = $images;
            array_push($data,$temp);
        }
        return array('data'=>$data);
    }

}

