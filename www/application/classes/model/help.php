<?php defined('SYSPATH') or die('No direct script access.');

class Model_Help extends ORM
{
	protected $_table_name = 'helps';

	public function add_help($data = null,$upd = 0){
		if( $data == null ) return "error";
                
		extract($data);
                if( $upd == 0){
                    $db = ORM::factory('help');
                }else{
                    $db = ORM::factory('help',$id);
                }

		$db->name    = $name;
		$db->content = $content;
		$db->aliase  = $aliase;
		$db->oid 	 = $oid;
		if( $db->save()){
			return true;
		}else{
			return false;
		}
	}

	/**
         *проверка доступности алиаса
         * @param str $aliase
         * @return str 1 - ошибка, 0 - все норм
         */
	public function check_aliase($aliase = null){
		if( $aliase == null ) return "error";

		$db = ORM::factory('help')->where('aliase','=',$aliase)->find_all()->count();
		return $db > 0 ? 0 : 1;
		//0 - true; 1 - error;

	}

        /**
         *возвращает все записи справки в таблицу
         * @return array все записи справки
         */
        public function get_all_helps(){
            $db = ORM::factory('help')->where('id','!=','')->order_by('oid')->find_all();

            $data = array();

            foreach($db as $el){
                $temp['name'] = $el->name;
                $temp['aliase'] = $el->aliase;
                $temp['content'] = $el->content;
                $temp['oid'] = $el->oid;
                $temp['id'] = $el->id;
                array_push($data, $temp);
            }
            return $data;
        }

        /**
         *изменение порядкового номера
         * @param type $id
         * @param type $oid
         * @return type bool
         */
        public function edit_oid($id = null, $oid = null){
            if( $id==null OR $oid==null ) return 'error';

            $db = ORM::factory('help',$id);
            $db->oid = $oid;
            if( $db->save() )
            {
                return true;
            }else{
                return false;
            }
        }

        public function delete_help($id=null){
            if( $id==null ) return false;

           $db = ORM::factory('help')->where('id','=',$id)->find();

           if($db->loaded()) $db->delete();
        }

        /**
         *Возращает данные записи по ид
         * @param type $id
         * @return array
         */
        public function get_help($id = null, $aliase = null){

            if( $id != null ){
                $db = ORM::factory('help',$id);
            }elseif( $aliase != null ){

                $db = ORM::factory('help')->where('aliase','=',$aliase)->find();
            }

            $data = array(
                'id'=>$db->id,
                'name'=>$db->name,
                'aliase'=>$db->aliase,
                'content'=>$db->content,
                'oid'=>$db->oid
            );
            return $data;
        }
}
