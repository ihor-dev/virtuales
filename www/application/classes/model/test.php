<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Test extends ORM {

    protected $_table_name = 'tests';
    protected $_has_many = array(
        'quetion' => array(
            'model' => 'quetion',
            'foreign_key' => 'test_id',
        ),
    );
#======================================================================
    /**
     *  Создание теста
     * @param type $name - название теста
     * @return bool 1/0 результат 
     */

    public function test_create($name=null) {
        if ($name == null)
            return false;

        $db = ORM::factory('test')->set('name', $name)->set('date', date("Y.m.d H:i:s"))->save();

        $this->register_test_id($db->id);
        return true;
    }

    /**
     * Записывает в сессию ид текущего теста
     * @param type $id - ид теста
     * @return type true/false
     */
    public function register_test_id($id=null) {
        if ($id == null)
            return false;

        Session::instance()->delete('test_id');
        Session::instance()->set('test_id', $id);
    }

    /**
     * количество вопросов в тесте
     * @return type int
     */
    public function count_quetions($test_id = null) {
        if ($test_id == null)
            $test_id = Session::instance()->get('test_id');

        if ($test_id == "")
            return false;

        $db = ORM::factory('test', $test_id);

        if ($db->loaded()) {
            return $db->quetion->count_all();
        } else {
            return false;
        }
    }

    /**
     *  полностью вся информация о тесте
     * @param type $id - теста
     * @return array данные теста
     */
    public function get_test($id=null) {
        if ($id == null)
            return false;

        $quetion_ids = array();

        $db = ORM::factory('test', $id);

        //запрос массива ид-вопросов теста
        $quetion_ids = $this->get_quetion_ids($id);

        //передача массива для запроса данных теста
        $data = Model::factory('quetion')->get_all_quetions($quetion_ids);
        $info['name'] = $db->name;
        $info['date'] = $db->date;
        $info['test_id'] = $id;

        return array('info' => $info, 'data' => $data);
    }

    /**
     *  Возвращает все ид вопросов теста
     TODO: ЗДЕСЬ ПИШЕМ ОГРАНИЧЕНИЕ НА КОЛИЧЕСТВО ВЫВОДИМЫХ ВОПРОСОВ В ТЕСТЕ
     * @param type $test_id
     * @return type 
     */
    public function get_quetion_ids($test_id = null) {
        if ($test_id == null)
            return false;

        $db = ORM::factory('test', $test_id);
        $arr = array();

        foreach ($db->quetion->find_all() as $el) {
            $arr[] = $el->id;
        }
        return $arr;
    }

    /**
     * Вывести список тестов
     */
    public function get_all_tests() {
        $data = $temp = array();
        $db = ORM::factory('test')->find_all()->as_array();
       
        foreach ($db as $el) {
            $count = $this->count_quetions($el->id);
           
            $temp = array(
                'id' => $el->id,
                'name' => $el->name,
                'date' => $el->date,
                'count' => $count
            );
            array_push($data, $temp);
        }
        return $data;
    }
    
    /**
     * массив всех тестов для actions
     * @return array type $arr['id'] = name;
     */
    public function all_tests(){
        $sql = "SELECT id, name FROM tests WHERE id >0";
        $db = DB::query(Database::SELECT, $sql)->execute();
        
        $data = array();
        foreach($db as $el){
            $data[$el['id']] = $el['name'];
        }
        return $data;
    }

    /**
     * Возвращает данные теста
     * @param int $id  test id
     * @return array test info
     */
    public function get_test_info($id=null){
        if($id == null) return false;
        $db = ORM::factory('test',$id);
        return array('name'=>$db->name);
    }
    /**
     *  Обновляет инфу теста
     * @param type $data
     * @return type 
     */
    public function upd_test_info($data = null){
        if($data == null) return false;
        $db = ORM::factory('test',$data['id']);
        $db->name = $data['name'];
        $db->save();
    }
    
    /**
     *  Удалить тест и все вопросы
     * @param type $id теста
     * @return type ошибка
     */
    public function test_delete($id = null){
        if($id == null) return 'error';
        $test_ids = $this->get_quetion_ids($id);
        
        foreach($test_ids as $qid){
            Model::factory('quetion')->delete_quetion($qid);
        }
        ORM::factory('test',$id)->delete();
        
        //удаляю задание с тестами
        $db = ORM::factory('task')->where('test_id','=',$id)->find_all();
        foreach($db as $ids){
            Model::factory('task')->delete_task($ids->id);
        }
    }
    
    # ---------------------------------------------------------------------
    /**
     * Выводит тесты, которые необходимо выполнить юзеру. переделать на задания. 
     * Сейчас будет выводить все тесты
     */

    public function user_tests() {
        return $this->get_all_tests();
    }

    # ---------------------------------------------------------------------
    # Vars: Session['test_id']    - ид теста
    #       Session['playlist']   - список ключей вопросов текущего теста
    #       Session['end_test']   - 1/0 вопросов больше нет. проверка тестов
    #       Session['qw_ids_str'] - список ключей вопросов (статика) для оценивания. по ним считаю макс сумму баллов
    #       Session['point']      - количество набранных балов за тест
    #       Session['task_id']j   - ид задания
    /**
     * Тестирование
     */
    public function test() {
      
        //test id
        $id = Session::instance()->get('test_id');
        $playlist = $this->get_playlist();


        if ( $playlist == "no") {
            //массив ид вопросов теста
            $qw_ids     = $this->get_quetion_ids($id);
            
            //формирую плейлист
            $qw_ids_str = $this->set_playlist($qw_ids);
            
            //записываю номера вопросов для оценивания
            $this->set_qw_ids($qw_ids_str);
        }

        $playlist = $this->get_playlist();
        
        $qw_id    = $this->parse_playlist($playlist);
        
        if($qw_id !== "end"){
            // возвратить данные вопроса
            $data = Model::factory('quetion')->get_quetion($qw_id,1);
            
            $data['end']= 0;
           
            return $data;
        }else{
            //возвратить статус об окончании теста
            return array('end'=>1);
        }
        
    }
    
    /**
     * возвращает плейлист
     */
    public function get_playlist() {
        return Session::instance()->get("playlist", "no");
    }

    /**
     *  Запись в сессию  ид вопросов
     * @param type $arr - qw_ids
     * @return type false
     */
    public function set_playlist($arr = null) {
        if ($arr == null)
            return false;
        if($arr !== "end"){
            $str = null;
            shuffle($arr);

            foreach ($arr as $el) {
                $str .= $el . " ";
            }
            $str = trim($str);
        
            Session::instance()->set('playlist', $str);
            //возвращаю плейлист для того, чтобы записать его в другую сессию
            return $str;
        }else{
            //иначе записываю о том, что вопросы кончились
            Session::instance()->set('playlist', $arr);
        }
    }

    /**
     *  Разбирает строку вопросов и возвращает один ид
     *  Если строка закончилась, возвращает "End!" - конец теста
     * 
     * @param type $str строка ид-вопросов
     * @return type ид вопроса
     */
    public function parse_playlist($str = null) {
        if ($str == null)
            return false;


        $arr = explode(" ", $str);

        $id = array_shift($arr);


        if (count($arr) < 1) {
            
            $this->delete_playlist();
            $this->set_playlist('end');
            return  $id;
        } else {

            $this->set_playlist($arr);
            return $id;
        }
    }

    /**
     *  Удаление сессии
     */
    public function delete_playlist() {
        Session::instance()->delete('playlist');
    }
    
    /**
     * Записываю номера вопросов, чтобы при оценке подсчитать макс. количество баллов.
     * @param type $str - строка номеров вопросов
     * @return type false
     */
    public function set_qw_ids($str = null){
        if($str == null) return false;
        
        Session::instance()->set('qw_ids_str',$str);
    }
    
    /**
     * Регистрация ид-задания
     * @param type $id - id задания
     * @return type false
     */
    public function set_task_id($id = null){
         if($id == null) return false;
         Session::instance()->set('task_id',$id);
    }
    
    /**
     *  Возвращает номера вопросов пройденного теста
     * @return type строка ключей
     */
    public function get_qw_ids(){
        return Session::instance()->get('qw_ids_str','empty');
    }

    /**
     * Удвление всех сессий перед началом нового теста
     */
    public function clear_sessions(){
        Session::instance()->delete('test_id');
        Session::instance()->delete('playlist');
        Session::instance()->delete('point');

    }
}

//--END TEST

