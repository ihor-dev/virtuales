<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Task extends ORM {

    protected $_table_name = 'tasks';
    protected $_belongs_to = array(
        'test' => array(
            'model' => 'test',
            'foreign_key' => 'test_id',
        ),
        'group' => array(
            'model' => 'group',
            'foreign_key' => 'group_id',
        )
    );
    protected $_has_many = array(
        'taskuser' => array(
            'model' => 'taskuser',
            'foreign_key' => 'task_id',
        )
    );

#======================================================================
    //добавление задания

    public function create_task($data = null) {
        if ($data == null)
            return false;

        switch ($data['type']) {
            case('test'):
                $db = ORM::factory('task')
                        ->set('type', 'test')
                        ->set('status', 'active')
                        ->set('test_id', $data['task'])
                        ->set('group_id', $data['group'])
                        ->set('date', date('d.m.Y H:i:s'))                        
                        ->save();

                break;

            case('lesson'):
                $db = ORM::factory('task')
                        ->set('type', 'lesson')
                        ->set('status', 'active')
                        ->set('lesson_id', $data['task'])
                        ->set('group_id', $data['group'])
                        ->set('date', date('d.m.Y H:i:s'))
                        ->save();
 
                break;

            case('practik'):
                $db = ORM::factory('task')
                        ->set('type', 'practik')
                        ->set('status', 'active')
                        ->set('practik_id', $data['task'])
                        ->set('group_id', $data['group'])
                        ->set('date', date('d.m.Y H:i:s'))
                        ->save();
               
                break;
        }
        return true;
    }

   //Все задания в админке. Вывод заданий по предмету на выбраную группу
    public function get_by_group( $grID=null) { 
        
        $sql = "select id, type, status, test_id, lesson_id,date, 
                CASE type
                    WHEN 'test' THEN (Select name from tests Where tests.id = test_id)
                    WHEN 'lesson' THEN (Select name from posts Where posts.id = lesson_id)
                    WHEN 'practik' THEN (Select name from practiks Where practiks.id = practik_id)
                END as name
                FROM tasks
                WHERE status = 'active'
                AND group_id = $grID
                ORDER BY id DESC";

        $res = DB::query(Database::SELECT, $sql)->execute();

        $task = $task_temp = array();
        foreach ($res as $info) {
            //task
            $task_temp['name'] = $info['name'];
            $task_temp['status'] = $info['status'];
            $task_temp['task_id'] = $info['id'];
            $task_temp['type'] = $info['type'];
            $task_temp['task_date'] = $info['date'];
            array_push($task, $task_temp);
        }

        
        return $task;
    }//конец------------------------

    /**
     * ответ на ajax-запрос данных задания.
     * параметры tid - id задания
     */
    public function get_task_info($tid = null) {
        if ($tid == null)
            return false;


//новая версия. С проверкой загружен ли юзером файл
        $sql2 = "SELECT users.id as uid, task_users.date as date, task_users.status as status, task_users.count_retask as count_retask, name, surname, patronymic, result, temp_result, file_id as file
                        FROM `task_users`
                        INNER JOIN users
                        ON task_users.user_id = users.id AND task_users.task_id = $tid
                        
                        LEFT JOIN results ON task_users.user_id = results.user_id 
                        AND task_users.task_id = results.task_id
                        
                        Inner JOIN tasks
                        ON task_users.task_id = tasks.id
                        
                        LEFT JOIN user_files
                        ON task_users.user_id = user_files.user_id
                        AND tasks.practik_id = user_files.practik_id
                        Group By uid
                        Order by surname";

        $res1 = DB::query(Database::SELECT, $sql2)->execute();
        $user = $user_temp = array();
        foreach ($res1 as $u) {
            //user
            $user_temp['uid'] = $u['uid'];
            $user_temp['user'] = $u['surname'] . " " . mb_substr($u['name'], 0, 1) . "." . " " . mb_substr($u['patronymic'], 0, 1) . ".";

            $user_temp['status'] = $u['status'];
            $user_temp['date'] = $u['date'];
            $user_temp['mark'] = $u['result'];
            $user_temp['temp_mark'] = $u['temp_result'];
            $user_temp['file'] = $u['file'] == NULL ? 0 : 1;
	    $user_temp['count_retask'] = $u['count_retask'];
            array_push($user, $user_temp);
        }
        return array('data' => $user);
    }

//----------------------------------------------------------------------------------------------
//ПОдсчет количества заданий юзера
    public function count_user_tasks($uid = null) {
        if ($uid == null)
            return fasle;
        $db = DB::query(Database::SELECT, "Select COUNT(id) as count FROM `task_users` Where `user_id` = $uid AND status IN ('not_executed', 'retask') ")->execute();
        foreach ($db as $el)
            return $el['count'];
    }

    /**
     *  Задания пользователя
     * @param type $uid
     * @return array 
     */
    public function get_tasks($uid = null, $type = null) {
        if ($uid == null OR $type == null)
            return false;
        
        $ugroups = Model::factory('group')->get_usergroups($uid);
        $gr_str = join(",",$ugroups);        

        $sql = "SELECT tasks.id,tasks.type,test_id,lesson_id, practik_id, tasks.date as task_date, task_users.date as date_complete,tasks.status as task_status, task_users.status as task_complete, count_retask, groups.name as `group`, CASE tasks.type
            WHEN 'test' THEN (Select name from tests Where tests.id = test_id)
            WHEN 'lesson' THEN (Select name from posts Where posts.id = lesson_id)
            WHEN 'practik' THEN (Select name from practiks Where practiks.id = practik_id)
            END as task_name 
            FROM `tasks` 
            LEFT JOIN task_users
            ON user_id = $uid
            AND tasks.id = task_id
            INNER JOIN groups
            ON groups.id = group_id
            WHERE
            group_id IN ( $gr_str)
            Group BY tasks.id Order By tasks.id DESC";


        $db = DB::query(Database::SELECT, $sql)->execute();
        $data = array();
        foreach ($db as $el) {
            array_push($data, $el);
        }

        return $data;
    }

    /*
     * Удаление задания
     */

    public function delete_task($id = null) {
        if ($id == null)
            return false;

//оценки
        $sql = "Delete FROM results WHERE task_id = $id";
        DB::query(Database::DELETE, $sql)->execute();

//задания пользователей
        $sql = "Delete FROM task_users WHERE task_id = $id";
        DB::query(Database::DELETE, $sql)->execute();
//комментарии и файлы к практическим
        $db = Model::factory('task')->where('id', '=', $id)->find();
        $prid = $db->practik_id;
        if (isset($prid)) {
            Model::factory('practik')->delete_task_u_files($id);
            Model::factory('practik')->delete_task_comments($id);
        }

//задание
        $sql = "Delete FROM tasks WHERE id = $id";
        DB::query(Database::DELETE, $sql)->execute();

        return true;
    }

    /**
     * Добавление пользователей в задание
     * @param type $data user`s ids array
     * @return boolean
     */
    public function add_users_to_task($data = null) {
        if ($data == null)
            return false;
        $ids = explode(" ", trim($data['ids']));
        $tid = $data['task_id'];
        foreach ($ids as $id) {
            $db = ORM::factory('taskuser')->where('task_id', '=', $tid)->and_where('user_id', '=', $id)->count_all();
            if ($db < 1) {
                $db1 = ORM::factory('taskuser')
                        ->set('task_id', $tid)
                        ->set('user_id', $id)
                        ->save();
            }
        }
    }

    /**
     * Удаление пользователей из задания
     * @param type $data (id_user, task_id)
     */
    public function delete_taskuser($data = null) {
        if ($data == null)
            return false;
        //оценки

        $sql = "Delete FROM `results` WHERE task_id = {$data['tid']} AND user_id = {$data['uid']}";
        DB::query(Database::DELETE, $sql)->execute();

        //задания пользователей
        $sql = "Delete FROM task_users WHERE task_id = " . $data['tid'] . " AND user_id = " . $data['uid'];
        DB::query(Database::DELETE, $sql)->execute();
        //комментарии к практическим
        $db = Model::factory('task')->where('id', '=', $data['tid'])->find();
        $prid = $db->practik_id;
        if (isset($prid)) {
            Model::factory('practik')->delete_u_comment($prid, $data['uid']);
            Model::factory('practik')->delete_u_pr_files($prid, $data['uid']);
        }

        return true;
    }

    /**
     * Получить список заданий группы
     */
    public function get_bygroupid($grid = null) {
        if ($grid == null)
            return "no group id!";

        $sql = "SELECT tasks.id as tid, tasks.type as type, CASE tasks.type
                        WHEN 'test' THEN (Select name from tests Where tests.id = test_id)
                        WHEN 'lesson' THEN (Select name from posts Where posts.id = lesson_id)
                        WHEN 'practik' THEN (Select name from practiks Where practiks.id = practik_id)
                    END as task_name 
                    FROM `tasks`, `posts`, `tests`, `practiks` WHERE
                    group_id = $grid
                    Group BY tasks.id";
        $db = DB::query(Database::SELECT, $sql)->execute();
        $data = array();
        foreach ($db as $el) {
            array_push($data, $el);
        }
        return $data;
    }
    
     
    
    /**
    * Формирую html-документ - все ответы юзеров на задание
    * @TODO Дописать скрипт, который будет удалять старые файлы и повесить на крон!
    */
    public function get_html($taskID = null){
    	if($taskID == null) return array('error'=>'no task id');

    	$path = "tmp/task-".$taskID.".html";
    	$fullpath = DOCROOT ."assets/files/". $path;
    	
    	$data = Model::factory('comment')->get_task_comments($taskID);
    	$html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>';
    	foreach($data as $el){
    	    $html.= "<h3>".$el['user'].":</h3> <p>".$el['text']."</p><hr/>";    	
    	}
    	
    	$html .= '</body></html>';
    	
        $fp = fopen($fullpath, 'a');
        fputs($fp, $html);
        fclose($fp);
    	
    	return $path;    
    }

    /**
     * Журнал
     */
    public function get_journal($data = null) {
        if ($data == null)
            return false;

        $uids = explode(" ", trim($data['uids']));
        $tids = explode(" ", trim($data['tids']));
        $str_tids = implode(",", $tids);
        
        $journal = $user_list = $task_temp =  array();

        $sql1 = "SELECT id, CASE type
                          WHEN 'test' THEN (Select name from tests Where tests.id = test_id)
                          WHEN 'lesson' THEN (Select name from posts Where posts.id = lesson_id)
                          WHEN 'practik' THEN (Select name from practiks Where practiks.id = practik_id)
                        END as name  
                        FROM `tasks` WHERE id in ($str_tids) Order By id";
        $db1 = DB::query(Database::SELECT, $sql1)->execute();
        foreach ($db1 as $el1) {
            array_push($task_temp, $el1);
        }
        $journal['tasks'] = $task_temp;

        $marks = array();
        foreach ($uids as $uid) {
            $temp = array();
            $u = Model::factory('user')->get_user($uid);
            $temp['uinfo']['id'] = $u['id'];
            $temp['uinfo']['name'] = $u['surname'] . " " . mb_substr($u['name'], 0, 1) . "." . mb_substr($u['patronymic'], 0, 1) . ".";
            array_push($user_list, $temp['uinfo']);//формирую списо пользователей
            
            $sql = "SELECT task_id, user_id, result, type, tasks.`date`, 
                        CASE type
                          WHEN 'test' THEN (Select name from tests Where tests.id = test_id)
                          WHEN 'lesson' THEN (Select name from posts Where posts.id = lesson_id)
                          WHEN 'practik' THEN (Select name from practiks Where practiks.id = practik_id)
                        END as task_name
                        FROM `results`, `tasks` WHERE
                        task_id = tasks.id
                        AND user_id = $uid
                        AND `tasks`.`id` in ($str_tids)
                        Order By tasks.id";

            $db = DB::query(Database::SELECT, $sql)->execute();
            $task_temp = array();
            foreach ($db as $el) {
                array_push($task_temp, $el);
            }
            $temp['marks'] = $task_temp;
            array_push($marks, $temp);
        }
        $journal['users'] = $user_list;
        $journal['marks'] = $marks;
        return $journal;
    }

}

