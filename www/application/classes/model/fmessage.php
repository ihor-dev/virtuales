<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Fmessage extends ORM {

    protected $_table_name = 'fmessages';    

    #======================================================================

    /**
     * Новый форум
     * @param 
     */
    public function _new($data = null) {
        if($data == null) return false;
        $db = ORM::factory('fmessage')
                ->set('topic_id',$data['tid'])
                ->set('user_id',  Engine_User_U::uid())
                ->set('text',$data['text'])
                ->set('date',date('d-m-Y H:i:s'))
                ->save();
        return $db->id;
    }

    /**
     * Удаление сообщения
     */
    public function _del($pID = null) {
        $sql = "Delete FROM fmessages WHERE id =$pID";
        return $db = DB::query(Database::DELETE, $sql)->execute();
    }
    
    /**
     * Удаление всех сообщений форума
     */
    public function _del_all($tID = null) {
        $sql = "Delete FROM fmessages WHERE topic_id =$tID";
        return $db = DB::query(Database::DELETE, $sql)->execute();
    }
    
    /**
     * Удаление всех сообщений юзера
     */
    public function del_umessages($uid = null){
        if($uid == null) return false;
        $sql = "Delete FROM fmessages WHERE user_id =$uid";
        return $db = DB::query(Database::DELETE, $sql)->execute();
    }
    
    /**
     * Возврат сообщений и инфы к сообщениям
     * @param type $tid - топик-ид
     * @param type $count - вывести сообщений
     * @param type $offset - пропустить сообщений
     * @return boolean|array ошибка если нет данных, или массив значений
     */
    public function get_messages($tid = null, $count = null, $offset = null){
        if($tid == null or $count == null or $offset == null) return 'no data get_messages';

        $db = ORM::factory('fmessage')->where('topic_id','=',$tid)->limit($count)->offset($offset)->order_by("id")->find_all();
        $data = array();
        foreach($db as $el){            
            $temp = array();
            $temp['id'] = $el->id;
            $temp['tid'] = $el->topic_id;
            $temp['user_id']  = $el->user_id;
            $u = Model::factory('user')->get_user($el->user_id);
            $temp['username'] = $u['name']." ".$u['surname'];            
            $temp['text'] = $el->text;
            $temp['date'] = $el->date;
            array_push($data, $temp);
        }
        
        return $data;
    }

   

}//end

