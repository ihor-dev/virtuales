<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Topic extends ORM {

    protected $_table_name = 'topics';

    #======================================================================

    /**
     * Новая тема форума
     * @param 
     */
    public function _new($data = null) {
        if ($data == null)
            return false;
        $db = ORM::factory('topic')
                ->set('forum_id', $data['id'])
                ->set('name', $data['name'])
                ->set('text', $data['text'])
                ->set('date', date("d.m.Y H:i:s"))
                ->set('status', 1)
                ->set('user_id', Engine_User_U::uid())
                ->save();
    }

    /**
     * изменить тему
     * @param type $name название
     * @param type $text текст
     */
    public function _update($id = null, $name = null, $text = null) {
        if ($id == null || $name == null || $text == null)
            return false;
        $db = ORM::factory('topic', $id);
        $db->name = $name;
        $db->text = $text;
        return $db->save();
    }

    //все форумы
    public function get_all($fid = null, $status = null) {
        if ($fid == null)
            return false;
        if ($status == null) {            
           
            $sql = 'SELECT 
                          t.id AS tid
                        , t.name AS tname
                        , t.text AS ttext
                        , u.id as uid
                        , u.name as uname
                        , u.surname usurname
                        , t.last_message_id AS mid
                        , m.text AS mtext
                        , m.date mdate
                        , t.status as status
                        , m.user_id as muid
                        , um.surname as msurname
                        , um.name as mname
                 FROM (SELECT t.id
                       , t.name
                       , t.text
                       , t.status
                       , t.user_id
                       , (SELECT MAX(m.id) 
                         FROM fmessages m
                         WHERE m.topic_id = t.id ) as last_message_id
                 FROM  topics t  WHERE `forum_id` = '.$fid.') t 
                 LEFT JOIN fmessages m 
                 ON m.topic_id = t.id
                 AND m.id = t.last_message_id
                 LEFT JOIN users u
                 ON t.user_id = u.id
                 LEFT JOIN users um
                 ON m.user_id = um.id
                 Order by t.id DESC';
            
        } else if ($status == "news") {//вывожу новости            
            $sql = "SELECT * FROM topics WHERE  status = 'news' Order by id DESC";
        }
            $db = DB::query(Database::SELECT, $sql)->execute();        
            $data = array();
        
            if($status == null){
                foreach ($db as $el) {
                    $t = array();
                    $t['tid'] = $el['tid'];
                    $t['tuid'] = $el['uid'];
                    $t['tname'] = $el['tname'];
                    $t['ttext'] = $el['ttext'];
                    $t['status'] = $el['status'];  
                    $t['mdate'] = $el['mdate'];       
                    $t['mid'] = $el['mid']; 
                    $t['mtext'] = $el['mtext']; 
                    $t['user'] = $el['uname'] . " " . $el['usurname'];
                    $t['muser'] = $el['mname'] . " " . $el['msurname'];
                    $t['muid'] = $el['muid'];

                    array_push($data, $t);
                }
            }elseif($status == 'news'){//вывожу новости            
                foreach ($db as $el) {
                    $temp = array();
                    $temp['id'] = $el['id'];
                    $temp['name'] = $el['name'];
                    $temp['date'] = $el['date'];
                    $temp['status'] = $el['status'];
                    $u = Model::factory('user')->get_user($el['user_id']);
                    $temp['user'] = $u['name'] . " " . $u['surname'];            
                    $temp['text'] = $el['text'];
                    array_push($data, $temp);
                }
            }
        
        $fname = Model::factory('forum')->get_name($fid);
        return array('topics' => $data, 'fname' => $fname);
    }

    //все сообщения темы
    public function read($id = null, $count = null, $offset = null) {
        if ($id == null)
            return false;

        $result = Model::factory('fmessage')->get_messages($id, $count, $offset);
        return $result;
    }

    //информация о топике
    public function get_info($tid = null) {
        if ($tid == null)
            return false;
        $db = ORM::factory('topic', $tid);
        $data['name'] = $db->name;
        $data['text'] = $db->text;
        $data['user_id'] = $db->user_id;
        $data['forum_id'] = $db->forum_id;
        return $data;
    }

    //Все id топиков форума
    public function get_allids($fid = null) {
        if ($fid == null)
            return false;
        $id = array();
        $db = ORM::factory('topic')->where('forum_id', '=', $fid)->find_all();
        foreach ($db as $el) {
            $id[] = $el->id;
        }
        return $id;
    }
    
    //ids топиков определенного юзера
    //Возвращает массив id постов, которые создал uid
    public function get_byuid($uid = null){
        if($uid == null) die('no user id');
        
        $db = ORM::factory('topic')->where('user_id', '=', $uid)->find_all();
        $id = array();
        foreach ($db as $el) {
            $id[] = $el->id;
        }
        return $id;
    }

    /**
     * Удаление темы
     */
    public function _del($tID = null) {
        if ($tID == null)
            return false;
        //удаляем сообщения
        Model::factory('fmessage')->_del_all($tID);

        $sql = "Delete FROM topics WHERE id =$tID";
        return $db = DB::query(Database::DELETE, $sql)->execute();
    }
    
    /**
     * Удаляем топики  и сообщения юзера (при удалении юзера)    
     */
    public function on_userdelete($uid = null){
        if($uid == null) return false;
        $ids = $this->get_byuid($uid);
        
        //delete user`s topics
        foreach($ids as $id){
            $this->_del($id);
        }
        
        //delete fmessages
        Model::factory('fmessage')->del_umessages($uid);
    }

    //сделать топик новостью
    public function set_news($id = null, $type = null) {
        if ($id == null || $type == null)
            return false;
        $db = ORM::factory('topic', $id);
        $db->status = $type;
        $db->save();
    }

}//end