<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Action extends Model {

    private $path = '';

    private function init() {
        $this->path = DOCROOT . "assets/file.txt";
    }

    /**
     * Запись действия
     * 0) type
     * 1) uID
     * 2)typeID - ид лекции, практ, теста и т.д., в зависимости от типа действия
     * 3)date
     * @param 
     */
    public function _new($data = null) {
        if ($data == null)
            return 'no data';
        $path = DOCROOT . "assets/file.txt";

        $str = join("_", $data) . "\r\n";
        $fp = fopen($path, 'a');
        fputs($fp, $str);
        fclose($fp);
    }

    //все действия
    public function get_all() {
        $path = DOCROOT . "assets/file.txt";
        $fp = file($path);
        
        foreach ($fp as $key => $val) {
            $temp[] = explode("_", $val);
        }        

        $data = array();
        $u  = Model::factory('user')->all_users();
        $pr = Model::factory('practik')->all_practiks();
        $t   = Model::factory('test')->all_tests();
        $p   = Model::factory('post')->all_posts();
        
        if(!isset($temp)) return false;
        
        foreach ($temp as $el) {            
            if(isset($u[$el['1']]['surname'])){
                $temp1[1] = $u[$el['1']]['surname'] . " " . $u[$el['1']]['name']; //NAME                   
                $temp1[3] = $el[3]; //DATE
                $temp1[4] = $u[$el['1']]['group']; //Group
            }

            switch ($el[0]) {
                
                //юзер добавил файл
                case('add-file'):
                    $temp1[2] = $pr[$el[2]]; //OBJECT id              
                    $temp1[0] = "ЛР(файл)"; //TYPE 
                    break;
                
                //Коммент к практической(ответ)
                case('save-pr'):
                    $temp1[2] = $pr[$el[2]]; //OBJECT id              
                    $temp1[0] = "ЛР(ответ)"; //TYPE 
                    break;
                
                case('login'):
                    $temp1[2] = ""; //OBJECT id              
                    $temp1[0] = "зашел"; //TYPE 
                    break;
                
                case('exit'):
                    $temp1[2] = ""; //OBJECT id              
                    $temp1[0] = "вышел"; //TYPE 
                    break;
                
                //смотрим табель оценок
                case('points'):
                    $temp1[2] = ""; //OBJECT id              
                    $temp1[0] = "табель"; //TYPE 
                    break;
                
                //сообщения
                case('msg'):
                    $temp1[2] = ""; //OBJECT id              
                    $temp1[0] = "сообщения"; //TYPE 
                    break;
              
                //форум
                case('forum'):
                    $temp1[2] = ""; //OBJECT id              
                    $temp1[0] = "форум"; //TYPE 
                    break;
                
                //запрос на пересдачу
                case('try-retask'):
                    $temp1[2] = $el[2]; //OBJECT id              
                    $temp1[0] = "пересдача?"; //TYPE 
                    break;
                
                //тест выполнен
                case('test'):
                    $temp1[2] = $t[$el[2]]; //OBJECT id              
                    $temp1[0] = "тест"; //TYPE 
                    break;
                
                //читаем лекцию
                case('lesson'):
                    $temp1[2] = $p[$el[2]]; //OBJECT id              
                    $temp1[0] = "лекция"; //TYPE 
                    break;
                
                default :
                    $temp1[2] = $el[2];
                    $temp1[0] = $el[0]; //TYPE 
                    break;
            }



            array_push($data, $temp1);
        }        
        $temp = $data;
        if (!isset($temp))
            $temp = array();
        return $temp;
    }

    /**
     * Удаление записи по ид
     */
    public function _del($id = null) {
        if ($id == null)
            return false;

        $path = DOCROOT . "assets/file.txt";
        $fp = file($path);

        foreach ($fp as $key => $val) {

            if ($id != $key)
                $temp[] = explode("_", $val);
        }

        $fp = fopen($path, 'w');

        if (isset($temp)) {
            foreach ($temp as $line) {
                $l[] = join('_', $line);
            }
            fputs($fp, join('', $l));
        }

        fclose($fp);
    }

    //Очистить все записи
    public function _clear() {
        $path = DOCROOT . "assets/file.txt";
        $fp = fopen($path, 'w');
        fclose($fp);
    }

}

//end

