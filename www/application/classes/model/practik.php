<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Practik extends ORM {

    protected $_table_name = 'practiks';

    #======================================================================
    //добавить практическое задание

    public function _add($name = null) {
        if ($name == null)
            return false;
        $db = ORM::factory('practik')->set('name', $name)->save();

        return $db->id;
    }

    //обновить практическое задание
    public function _upd() {
        extract($_POST);
        $db = ORM::factory('practik', $id);
        $db->text = $text;
        $db->name = $name;
        $db->save();
    }

    //привязать файл-задание к практическому заданию
    public function link_file($file_id = null, $pr_id = null) {
        if ($file_id == null)
            return false;

        $sql = "Insert into `practik_files` (id, file_id, practik_id) VALUES ( null, " . $file_id . "," . $pr_id . ")";
        $db = DB::query(Database::INSERT, $sql)->execute();
    }

    
    /**
     * Привязать файлы к ответу пользователя
     * 
     * @param type $fileID
     * @param type $prID
     * @param type $uID
     * @param type $ownerID hashe of file (md4)
     * @return boolean
     */
    public function link_user_file($fileID = null, $prID = null, $uID = null, $ownerID = null, $task_id = null) {
        if ($fileID == null OR $prID == null OR $uID == null OR $task_id == null)
            return false;

        if ($ownerID == null) {//Если файл ничей
            $sql = "Insert into `user_files` (id, user_id, file_id, practik_id, task_id) VALUES ( null, " . $uID . "," . $fileID . "," . $prID .",". $task_id .")";
        } else {//Если плагиат - дописываю ид юзера, у кого скопировано
            $sql = "Insert into `user_files` (id, user_id, file_id, practik_id, hashe, task_id) VALUES ( null, " . $uID . "," . $fileID . "," . $prID . ",'" . $ownerID ."',". $task_id .")";
        }

        $db = DB::query(Database::INSERT, $sql)->execute();
    }

    //привязать комментарий к практ.заданию
    public function link_comment($practikID = null, $commentID = null, $tID = null) {
        if ($practikID == null OR $commentID == null)
            return false;

        $db = DB::query(Database::INSERT, "Insert Into practik_comments (id, practik_id, comment_id, task_id) VALUES (null," . $practikID . ", " . $commentID . ", ". $tID . ")")->execute();
    }

    //проверить, существует ли комментарий юзера к этой практической
    public function is_comment($practikID = null, $userID = null) {
        if ($practikID == null OR $userID == null)
            return false;

        $sql = "SELECT comments.id as id FROM `practik_comments`, comments WHERE
            comment_id = comments.id AND
            user_id = " . $userID . " AND
            practik_id = " . $practikID;
        $db = DB::query(Database::SELECT, $sql)->execute();
        foreach ($db as $count) {
            return $count['id'];
        }
    }

    //список всех практических заданий
    public function get_all() {
        $data = array();
        $db = ORM::factory('practik')->order_by('oid')->find_all();
        foreach ($db as $el) {
            $temp = array();
            $temp['name'] = $el->name;
            $temp['text'] = $el->text;
            $temp['id'] = $el->id;
            array_push($data, $temp);
        }
        return $data;
    }
    
    /**
     * Массив всех практических заданий. для ACTIONS
     * @return array массив практических вида $temp['id'] = name;
     */
    public function all_practiks(){
        $sql = "SELECT name, id FROM practiks WHERE id >0 Order By `oid` DESC";
        $db = DB::query(Database::SELECT,$sql)->execute();        
        $temp = array();
        
        foreach($db as $el){            
            $temp[$el['id']] = $el['name'];
        }
        return $temp;
    }

    // info by practik_id
    public function get_practik($id = null) {
        if ($id == null)
            return false;
        $db = ORM::factory('practik', $id);
        if ($db->loaded()) {
            $temp['name'] = $db->name;
            $temp['text'] = $db->text;
            $temp['id'] = $db->id;

            //files
            $dbf = DB::query(Database::SELECT, 'Select * FROM practik_files WHERE practik_id=' . $id)->execute();
            $files = array();
            foreach ($dbf as $f) {
                $file = Model::factory('file')->get_file_info($f['file_id']);
                array_push($files, $file);
            }
            $temp['files'] = $files;
            return $temp;
        }
    }

    //список файлов, прикрепленных пользователем
    public function get_user_files($uID = null, $prID = null) {
        if ($uID == null OR $prID == null)
            return false;

        //files
        $dbf = DB::query(Database::SELECT, 'Select * FROM user_files WHERE user_id=' . $uID . ' AND practik_id =' . $prID)->execute();
        $files = array();
        foreach ($dbf as $f) {
            $file = Model::factory('file')->get_file_info($f['file_id']);
            if ($f['hashe'] != "") {                
               $file['owner'] = $this->find_by_hashe($uID, $f['hashe']);
            }
            array_push($files, $file);
        }
        return array('files' => $files);
    }
     
    //поиск совпадений файлов АНТИПЛАГИАТ
    public function find_by_hashe($uid,$hashe = null){
    	if($hashe == null) return false;
    	$sql = "SELECT users.id, `surname`, `name` FROM `user_files`, users WHERE 
    	file_id < (Select file_id From user_files Where hashe = '".$hashe."' AND user_id = $uid Group by user_id)
    	AND users.id = user_id 
    	AND hashe = '".$hashe."' 
    	AND user_id <> $uid
    	Order by user_files.id";
    	$db = DB::query(Database::SELECT, $sql)->execute();
    	
    	$user = "";
    	foreach($db as $u){
    		$user .= $u['surname'].mb_substr($u['name'],0,1).".=>";
    	}
    	return $user;
    }

    /**
     * удалить практическое задание
     * id = код практического
     */
    public function _delete($id = null) {
        if ($id == null)
            return false;

        #--TASKS
        //get task ids
        $sql1 = "SELECT id FROM `tasks` WHERE practik_id = $id";
        $res1 = DB::query(Database::SELECT, $sql1)->execute();
        foreach ($res1 as $el1) {
            // del task_users
            $sql2 = "DELETE FROM task_users WHERE task_id = {$el1['id']}";
            DB::query(Database::DELETE, $sql2)->execute();

            #RESULTS
            $sql4 = "DELETE FROM results WHERE task_id = {$el1['id']}";
            DB::query(Database::DELETE, $sql4)->execute();
        }
        //del task
        $sql3 = "DELETE FROM tasks WHERE practik_id = $id";
        DB::query(Database::DELETE, $sql3)->execute();

        #--Файлы пользователей
        $this->delete_link_u_files($id);
        #--комментарии к лабам 
        $this->delete_comment($id);
        #--файлы практического задания
        $this->delete_link_files($id);
        #--практическое задание
        $pr = DB::delete('practiks')->where('id', '=', $id)->execute();

        return true;
    }

    /**
     * Удалить файлы практического задания
     * @param type $practikID - id практического задания     
     */
    public function delete_link_files($practikID = null) {
        if ($practikID == null)
            return false;
        //нахожу все файлы практической
        $db = DB::query(Database::SELECT, 'Select * FROM practik_files WHERE practik_id =' . $practikID)->execute();
        foreach ($db as $file) {
            //удаляю файлы
            Model::factory('file')->_del($file['file_id']);
        }
        //удаляю записи
        $dl = DB::delete('practik_files')->where('practik_id', '=', $practikID)->execute();
    }
    
    //Удалить все файлы, прикрепленные юзерами к заданию (по ид задания)
    public function delete_task_u_files($tID = null){
        if ($tID == null)
            return false;
        //нахожу все файлы задания
        $db = DB::query(Database::SELECT, 'Select * FROM user_files WHERE task_id =' . $tID)->execute();
        foreach ($db as $file) {
            //удаляю файлы
            Model::factory('file')->_del($file['file_id']);
        }
        //удаляю записи
        $dl = DB::delete('user_files')->where('task_id', '=', $tID)->execute();
    }

    //Удалить файлы прикрепленные пользователями для ид практического (При удалении практического задания)
    public function delete_link_u_files($practikID = null) {
        if ($practikID == null)
            return false;
        //нахожу все файлы практической
        $db = DB::query(Database::SELECT, 'Select * FROM user_files WHERE practik_id =' . $practikID)->execute();
        foreach ($db as $file) {
            //удаляю файлы
            Model::factory('file')->_del($file['file_id']);
        }
        //удаляю записи
        $dl = DB::delete('user_files')->where('practik_id', '=', $practikID)->execute();
    }

    //Удалить прикрепленный admin файл к практическому
    public function delete_link_file($fileID = null) {
        if ($fileID == null)
            return false;

        $db = DB::delete('practik_files')->where('file_id', '=', $fileID)->execute();
        Model::factory('file')->_del($fileID);
    }

    //удалить прикрепленный пользователем файл к практ.
    public function delete_u_link_file($fileID = null) {
        if ($fileID == null)
            return false;

        $db = DB::delete('user_files')->where('file_id', '=', $fileID)->execute();
        Model::factory('file')->_del($fileID);
    }

    /**
     * Удалить файлы юзера по ид юзера и практик ид
     * @param type $practikID
     * @param type $userID
     */
    public function delete_u_pr_files($practikID = null, $userID = null) {
        //удаляем файлы
        $dbf = DB::query(Database::SELECT, 'Select * FROM user_files WHERE user_id=' . $userID . ' AND practik_id =' . $practikID)->execute();
        foreach ($dbf as $f) {
            Model::factory('file')->_del($f['file_id']);
            DB::delete('user_files')->where('file_id', '=', $f['file_id'])->execute();
        }
    }
    
    //удалить прикрепленные пользователями комментарии (для удаления задания task)
    public function delete_task_comments($taskID = null) {
        if ($taskID == null)
            return false;

        //нахожу все записи текущего задания
        $db = DB::query(Database::SELECT, 'Select * FROM practik_comments WHERE task_id =' . $taskID)->execute();
        foreach ($db as $pr) {
            //удаляю комменты
            Model::factory('comment')->del($pr['comment_id']);
        }
        //удаляю связи
        $db = DB::delete('practik_comments')->where('task_id', '=', $taskID)->execute();
    }
    

    //удалить прикрепленные пользователями комментарии (для удаления практического задания)
    public function delete_comment($practikID = null) {
        if ($practikID == null)
            return false;

        //нахожу все записи текущей практической
        $db = DB::query(Database::SELECT, 'Select * FROM practik_comments WHERE practik_id =' . $practikID)->execute();
        foreach ($db as $pr) {
            //удаляю комменты
            Model::factory('comment')->del($pr['comment_id']);
        }
        //удаляю связи
        $db = DB::delete('practik_comments')->where('practik_id', '=', $practikID)->execute();
    }

    /**
     *  Удалить комментарий одного задания одного пользователя
     * @param type $practikID
     * @param type $userID
     */
    public function delete_u_comment($practikID = null, $userID = null) {
        if ($practikID == null OR $userID == null)
            return false;

        //нахожу все записи текущей практической
        $db = DB::query(Database::SELECT, 'Select `comment_id` FROM `practik_comments`, `comments` WHERE  comment_id = comments.id AND practik_id =' . $practikID . ' AND user_id = ' . $userID)->execute();
        foreach ($db as $pr) {
            //удаляю комменты
            Model::factory('comment')->del($pr['comment_id']);

            //удаляю связи
            $db = DB::delete('practik_comments')->where('comment_id', '=', $pr['comment_id'])->execute();
        }
    }

    //get practik_id by task_id
    public function get_practik_id($task_id = null) {
        if ($task_id == null)
            return 'no task id';
        $db = ORM::factory('task', $task_id);
        return $db->practik_id;
    }
    
    //sort practiks
    public function sort($ids = null){
        if($ids == null) return false;
        $ids = explode(" ",$ids);
        $i = 0;
        $sql = "Update practiks SET `oid` = :i WHERE `id` = :pid";
        $db = DB::query(Database::UPDATE, $sql)->bind(":i", $i)->bind(":pid", $pid);
        foreach ($ids as $pid) {
            $db->execute();
            $i++;
        }
    }
    

}

