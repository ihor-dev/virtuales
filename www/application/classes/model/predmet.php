<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Predmet extends ORM {

    protected $_table_name = 'predmets';

#======================================================================
    /**
     * удалить предмет
     */

    public function _del($id = null) {
        if ($id == null)
            return false;

        $db = ORM::factory('predmet', $id)->delete();
        return true;
    }

    //добавить предмет
    public function _add($name = null) {
        if ($name == null)
            return false;

        $db = ORM::factory('predmet')
                ->set('name', $name)
                ->save();
        return $db->id;
    }

    //вернуть все
    public function _all() {
        $db = ORM::factory('predmet')->find_all();
        $data = array();
        
        foreach ($db as $el) {
            $temp = array();
            $temp['name'] = $el->name;
            $temp['id'] = $el->id;
            array_push($data, $temp);
        }
        return $data;
    }

    //add users to predmet
    public function _add_user($data = null) {
        if (count($data) < 1)
            return false;

        $ids = explode(" ", trim($data['ids']));
        
        foreach ($ids as $id) {
            $sql = $this->insert_unique('predmet_users', array('predmet_id'=>$data['pr'],'user_id'=> $id));
            DB::query(Database::INSERT, $sql)->execute();
        }
    }
    
    /**
     * Ид юзеров, которые уже добавлены на этот предмет
     * @param type $prID ид предмета
     * @param type $grID ид группы
     * @return boolean
     */
    public function get_pr_users($prID = null, $grID = null){
        if($prID == null || $grID == null) return false;
        $sql = "SELECT predmet_id, user_id, predmet_users.id AS id
                    FROM `predmet_users` , `users`
                    WHERE users.id = predmet_users.user_id
                    AND group_id =$grID
                    AND predmet_id=$prID";
        $db = DB::query(Database::SELECT, $sql)->execute();
        
        $data = array();
        
        foreach($db as $el){            
            $temp = array();
            $temp['id'] = $el['id'];
            //$temp['prID'] = $el['predmet_id'];
            $temp['user_id']= $el['user_id'];
            array_push($data, $temp);
        }
        
        return $data;        
    }
    
    /**
     * Get user's predmets (name and id)
     * @param type $uid user id
     * @return boolean|array data of false
     */
    public function get_predmets($uid = null){
        if($uid == null) return false;
        
        $sql = "SELECT `predmet_id`, `name`  FROM `predmet_users`, `predmets` WHERE predmet_id = predmets.id AND `user_id` = $uid";
        $db = DB::query(Database::SELECT, $sql)->execute();
        
        $data = array();
        
        foreach($db as $el){
            $temp['id'] = $el['predmet_id'];
            $temp['name'] = $el['name'];
            array_push($data, $temp);
        }
        
        return $data;
    }
    
    //имя предмета по ид
    public function get_by_id($id = null){
        if($id == null) return false;
        $db = ORM::factory('predmet',$id);
        return $db->name;
    }
    
    //удалить запись из таблицы `predmet_users`
    public function del_user($id = null){
        if($id == null) return false;
        
        $id = mysql_real_escape_string($id);
        
        $sql = "DELETE FROM `predmet_users` WHERE id = $id";
        DB::query(Database::DELETE, $sql)->execute();
        return true;
    }

    
    /**
     * Функция, которая добавляет только уникальные записи
     * @param type $table table_name
     * @param type $vars array('field_name'=>'value', 'fiels_name2'=>'value2')
     * @return boolean true
     */
    function insert_unique($table, $vars) {
        if (count($vars)) {
            $table = mysql_real_escape_string($table);
            $vars = array_map('mysql_real_escape_string', $vars);

            $req = "INSERT INTO `$table` (`" . join('`, `', array_keys($vars)) . "`) ";
            $req .= "SELECT '" . join("', '", $vars) . "' FROM DUAL ";
            $req .= "WHERE NOT EXISTS (SELECT 1 FROM `$table` WHERE ";

            foreach ($vars AS $col => $val)
                $req .= "`$col`='$val' AND ";

            return $req = substr($req, 0, -5) . ") LIMIT 1";            
            
        }

        return False;
    }

}

