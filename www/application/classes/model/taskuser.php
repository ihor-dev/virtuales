 <?php

defined('SYSPATH') or die('No direct script access.');

class Model_Taskuser extends ORM {

    protected $_table_name = 'task_users';
    protected $_belongs_to = array(
        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        )
    );



#======================================================================
    /**
     *  Отметить задание как выполнено
     * @param type $task_id
     * @return type 
     */
    public function report_task($task_id = null) {
        if ($task_id == null)
            return FALSE;
        $db = ORM::factory('taskuser')
                ->where('task_id', '=', $task_id)
                ->and_where('user_id', '=', Engine_User_U::uid())
                ->find();
        $db->date = date('d.m.Y H:i:s');
        $db->status = 'complete';
        $db->save();
    }
    
    /**
     * Пометить задание "запрос на пересдачу
     * $task_id = id задания, которое юзер хочет пересдать
     * $type = "try_retask" - статус выставляется юзером, при запросе на пересдачу/ "retask" - выставляется
     * преподавателем, при подтверждении пересдачи задания
     * retasked - задание перевыполнено, ждет подтверждения
     * "wait" - ожидает проверки
     * "complete" выполнено
     */
    public function try_retask($task_id = null, $type = null, $uid = null){
        if ($task_id == null OR $type == null OR $uid == null)
            return FALSE;
        
        $db = ORM::factory('taskuser')
                ->where('task_id', '=', $task_id)
                ->and_where('user_id', '=', $uid)
                ->find();
        $db->status = $type;
        $db->date = date("d.m.Y H:i:s");
        $db->save();
    }
    
    //удаление результатов юзера
    public function delete_results($uid = null){
        if($uid == null) return false;
        $sql = "DELETE FROM task_users WHERE user_id = $uid";
        
        return DB::query(Database::DELETE, $sql)->execute();
    }
    
    /**
     * Добавляю запись в таскюзер. Пользователь привязал себе задание
     * @param type $uID
     * @param type $taskID
     * @return boolean
     */
    public function sign_task($uID = null, $taskID = null){
        if($uID == null OR $taskID == null) return false;
           
        $sql = Engine_User_API::insert_unique('task_users', array('task_id'=>$taskID, 'user_id'=>$uID));
        DB::query(Database::INSERT, $sql)->execute();
    }

}

