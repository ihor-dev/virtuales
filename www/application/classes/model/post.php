<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Post extends ORM {

    protected $_table_name = 'posts';
    protected $_belongs_to = array(
        'category' => array(
            'model' => 'category',
            'foreign_key' => 'category_id',
        ),
        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        )
    );

#======================================================================
    //Add post

    public function add_post($data = null) {
        if (!$data)
            return false;

        extract($data);

        $user = Auth::instance()->get_user();
        $uid = $user->id;

        $content = Engine_User_API::del_br($content);

        $db = ORM::factory('post')
                ->set('category_id', $category)
                ->set('text', $content)
                ->set('date', date('d-m-Y'))
                ->set('type', $type)
                ->set('name', $name)
                ->save();
    }

    //обновление записи
    public function post_update($data = null) {
        if ($data == null)
            return false;
        extract($data);

        $content = Engine_User_API::del_br($content);

        $db = ORM::factory('post', $id)
                ->set('category_id', $category)
                ->set('text', $content)
                ->set('type', $type)
                ->set('name', $name)
                ->save();
        return true;
    }

    /**
     * @TODO дописать удаление расшареных постов, при удалении поста!
     * удаление записи
     */
    public function post_delete($id = null) {
        if ($id == null)
            return false;

        $db = ORM::factory('post', $id)->delete();
        //shared_posts !!!!!!!!!!!!!!!!!!!!!
        
    }

    //посты по типу (для расписания, можно и для новостей)
    public function get_posttype($type = null) {
        if ($type != null) {
            $db = ORM::factory('post')->where('type', '=', $type)->find_all();
        }
        if (count($db) > 0) {
            $data = array();
            $i = 0;

            foreach ($db as $el) {
                $data[$i] = array(
                    'id' => $el->id,
                    'text' => $el->text,
                    'date' => $el->date,
                    'name' => $el->name,
                    'category' => $el->category->name,
                    'type' => $el->type
                );
                $i++;
            }
            return $data;
        }
    }

    //возвращает пост по заданному ид
    public function get_post($id = null) {
        if (!$id)
            return false;

        $db = ORM::factory('post', $id);


        if (count($db->id) > 0) {
            $data = array(
                'id' => $db->id,
                'category' => $db->category->name,
                'text' => $db->text,
                'date' => $db->date,
                'type' => $db->type,
                'name' => $db->name,
            );
        } else {
            $data = false;
        }
        return $data;
    }

    /**
     * Запрос список лекций. Параметры: категория, тип. Если ничего не задано, выводятся все лекции
     * @param type $cat_id - категория
     * @param string $type - Тип записи
     * @return type массив лекций
     */
    public function get_posts($cat_id = null, $type = null) {

        if ($cat_id == null AND $type == null) {
            $db = ORM::factory('post')->where('id', '>=', 0)->Order_by("oid")->find_all();
        } elseif ($type) {
            $db = ORM::factory('post')->where('id', '>=', 0)->and_where('type', '=', $type)->Order_by("oid")->find_all();
        }
        if (count($db) > 0) {
            $data = array();
            $i = 0;

            foreach ($db as $el) {
                $data[$i] = array(
                    'id' => $el->id,
                    'text' => $el->text,
                    'date' => $el->date,
                    'name' => $el->name,
                    'category' => $el->category->name,
                    'type' => $el->type
                );
                $i++;
            }
            return $data;
        }
    }

    /**
     * Массив всех постов для actions
     * @return array вида $arr['id'] = name;
     */
    public function all_posts() {
        $sql = "SELECT id, name FROM posts WHERE id >0";
        $db = DB::query(Database::SELECT, $sql)->execute();

        $data = array();
        foreach ($db as $el) {
            $data[$el['id']] = $el['name'];
        }
        return $data;
    }

    /**
     * Подсчет количества записей
     */
    public function count_posts($id = null) {

        if ($id == null) {
            return DB::query(Database::SELECT, "SELECT `id` FROM `posts` WHERE `id` >= 0")->execute()->count();
        } else {
            return DB::query(Database::SELECT, "SELECT `id` FROM `posts` WHERE `category_id` = '" . $id . "'")->execute()->count();
        }
    }

    /**
     * Разрешить доступ к лекциям (Не задание!! Доступно на главной в списке лекций)
     */
    public function share_post($data = null) {
        if ($data == null)
            return "no input data share_posts()";
        $sql = Engine_User_API::insert_unique('posts_share', array('post_id'=>$data['post_id'], 'group_id'=>$data['group_id']));         
        $db = DB::query(Database::INSERT, $sql)->execute();       
    }

    /**
     * возпращает группы, кому расшарен данный пост
     * @param type $post_id - ид поста     
     */
    public function get_shared_post_groups($post_id = null) {
        if ($post_id == null)
            return 'no input data';
        $sql = "SELECT group_id as id, name FROM `posts_share`, groups WHERE groups.id = group_id AND post_id = $post_id";
        $db = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $db;
    }



//shared посты юзера 
    public function get_shared_posts($uid = null) {
        if ($uid == null)
            return 'no uid';
        $ugroups = Model::factory('group')->get_usergroups($uid);
        $ugroups = join(', ', $ugroups);
        $sql = "SELECT post_id, text, date, name FROM `posts_share`, `posts` WHERE
                group_id IN ($ugroups)
                AND `posts_share`.post_id = posts.id
                order by oid DESC";
        $db = DB::query(Database::SELECT, $sql)->execute();
        if (count($db) > 0) {
            $data = array();
            $i = 0;

            foreach ($db as $el) {
                $data[$i] = array(
                    'id' => $el['post_id'],
                    'text' => mb_substr($el['text'], 0, 255),
                    'date' => $el['date'],
                    'name' => $el['name'],
                );
                $i++;
            }
            return $data;
        }
    }

//end get_shared

    //удалить запись расшареной записи группе
    public function delete_shared_group($post_id = null, $group_id = null){
        if($post_id == null OR $group_id == null) return 'error';
        $sql = ' Delete from posts_share WHERE post_id = '.$post_id.' AND group_id ='.$group_id;
         $db = DB::query(Database::DELETE, $sql)->execute();
    }
    

//Сортировка постов
    public function sort_posts($id = null) {
        if ($id == null)
            return false;
        $ids = explode(" ", $id);
        $i = 0;
        $sql = "Update posts SET `oid` = :i WHERE `id` = :pid";
        $db = DB::query(Database::UPDATE, $sql)->bind(":i", $i)->bind(":pid", $pid);
        foreach ($ids as $pid) {
            $db->execute();
            $i++;
        }
    }

}

//End post.php
