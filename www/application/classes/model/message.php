<? # MODEL Messages #   ?>
<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Message extends ORM {

    protected $_table_name = 'messages';

#------------------------------------------------------------------------------------------------

    /**
     * добавление нового сообщения
     */
    public function _add($data = null) {
        if ($data == null OR $data['to'] == "undefined")
            return false;
        $db = ORM::factory('message');
        $db->text = trim($data['text']);
        $db->from = Auth::instance()->get_user()->id;
        $db->to = $data['to'];
        $db->date = date("d-m-y H:i:s");
        $db->save();
        return true;
    }

    //ставлю статус "прочитал" для сообщений от юзера, которого сообщения щас буду просматривать
    public function _set_status($from=null) {
        if ($from == null)
            return false;

        $uid = Auth::instance()->get_user()->id;
        $sql = "Update `messages` SET `status` ='1' WHERE `from` = '" . $from . "' AND `to` = '" . $uid . "'";
        $db = DB::query(Database::UPDATE, $sql)->execute();
    }

    //прочитать Все!!! сообщения по юзер ид (Переписка с юзер ид)
    public function _get($data = null) {
        $uid = Auth::instance()->get_user()->id;        
        $users = Model::factory('user')->users_array();
        $sql = "SELECT * FROM `messages` WHERE 
                (`from` = '" . $data['from'] . "' and `to` = '" . $uid . "') OR
                (`from` = '" . $uid . "' and `to` = '" . $data['from'] . "') Order By id ASC";
        $db = DB::query(Database::SELECT, $sql)->execute();
        $temp = $result = array();

        foreach ($db as $el) {
            $temp['id'] = $el['id'];
            $temp['from'] = $users[$el['from']];
            $temp['text'] = $el['text'];
            $temp['data'] = $el['date'];            
            array_push($result, $temp);
            $temp = null;
        }
        $img =  Engine_User_U::avatar($data['from']);
        //отметка "сообщения прочитал"
        $this->_set_status($data['from']);
        return array('messages' => $result, 'img'=>$img);
    }

    //получить все мои непрочитанные сообщения от юзера ид
    public function new_message($uid = null) {
        if ($uid == null)
            return false;
        $myid = Auth::instance()->get_user()->id;
        $sql = "SELECT * FROM `messages` WHERE `to` = $myid AND `from` = $uid AND `status` <> '1'  Order by id ASC";

        $db = DB::query(Database::SELECT, $sql)->execute();
        $result = array();
        foreach ($db as $el) {
            $temp['id'] = $el['id'];
            $temp['text'] = $el['text'];
            $temp['data'] = $el['date'];
            array_push($result, $temp);
            $temp = null;
        }
        //отметка "сообщения прочитал"
        $this->_set_status($uid);
        if(count($result)>0){
            return array('messages' => $result);
        }else{
            return array(0);
        }
    }

    /**
     * Удаление сообщений
     */
    public function _delete($id=null) {
        if ($id == null)
            return false;
        $uid = Auth::instance()->get_user()->id;
        $sql = "DELETE FROM messages WHERE status = '1' AND(
        (`to` = $uid AND `from` = $id) OR 
        (`from` = $uid AND `to` = $id))";
        $db = DB::query(Database::DELETE, $sql)->execute();
    }

    //Подсчет непрочитанных сообщений (добавить проверку по полю статус)
    public function _count() {
        $uid = Auth::instance()->get_user()->id;
        return $db = ORM::factory("message")->where('to', '=', $uid)->and_where('status', '<>', '1')->count_all();
    }

    //Новые сообщения от разных юзеров. Групируются по юзеру. Отображаются на главной сообщений
    //
    public function _get_new_msgs() {
        $uid = Auth::instance()->get_user()->id;
        $users = Model::factory('user')->users_array();        

        $sql = "Select * from(SELECT * FROM `messages` WHERE `to` = $uid AND `status` <> '1'  Order by id DESC) grouped Group by grouped.`from` Order By id DESC";
        $db = DB::query(Database::SELECT, $sql)->execute();
        $result = array();
        foreach ($db as $el) {
            $temp['id'] = $el['id'];
            $temp['from'] = $users[$el['from']];
            $temp['fromID'] = $el['from'];
            $temp['text'] = mb_substr($el['text'], 0, 80);
            $temp['data'] = $el['date'];
            array_push($result, $temp);
            $temp = null;
        }

        return array('messages' => $result);
    }

}
