<?php defined('SYSPATH') or die('No direct script access.');

class Model_Key extends ORM
{
	protected $_table_name = 'keys';
        
	protected $_belongs_to = array(
		'quetion' => array(
			'model' => 'quetion',
			'foreign_key' => 'quetion_id',
		),
		
		'answer' => array(
			'model' => 'answer',
			'foreign_key' => 'answer_id',
		)
	);
	

#======================================================================
        /**
         * Возвращает список ключей вопроса
         * @param type $id вопроса
         * @return массив ключей
         */
        public function get_quetion_keys($id=null){
            if($id == null) return false;
            
            $db = ORM::factory('key')->where('quetion_id','=',$id)->find_all();
            $keys = array();
            foreach($db as $el){
                $keys[$el->answer_id] = $el->answer_id;
            }
            return $keys;
        }
        
        /**
         *  Добавление ключа
         * @param type $qw_id  - id вопроса
         * @param type $answer_id id ответа
         * @return type 
         */
        public function add_key($qw_id = null, $answer_id = null){
            if($qw_id == null OR $answer_id == null) return false;
            
            $db = ORM::factory('key')
                    ->set('quetion_id',$qw_id)
                    ->set('answer_id',$answer_id)
                    ->save();
        }
}

