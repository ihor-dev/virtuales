<?php

defined('SYSPATH') or die('No direct script access.');

class Model_File extends ORM {

    protected $_table_name = 'files';
    protected $file_path = "assets/files";

    #======================================================================

    /**
     * Загрузка файла и регистрация в БД
     * @param type $filename имя файла
     */
    public function _add($path = null, $antiplag = null) {
        //загрузка файла
        $filename = Engine_User_API::uploader("/assets/files" . "/" . $path, $antiplag);

        $db = ORM::factory('file')->set('url', $filename['name'])->set('path', $path)->set('name', $filename['old_name'])->save();
        return array('fID' => $db->id, 'hashe' => $filename['hashe']);
    }

    /**
     * Удаление файла
     */
    public function _del($fileID = null) {

        if ($fileID == null)
            return false;

        $db = ORM::factory('file', $fileID);
        if ($db->loaded()) {
            //Удалить файл из ФС
            if (isset($db->path)) {
                unlink($this->file_path . "/user/" . $db->url);
            } else {
                unlink($this->file_path . "/" . $db->url);
            }
            $db->delete();
        }
    }

    /**
     * возвращает информацию о файле
     * @param type $fileID - id файла
     */
    public function get_file_info($fileID = null) {
        if ($fileID == null)
            return false;

        $db = ORM::factory('file', $fileID);
        return array(
            'id' => $db->id,
            'url' => $db->url,
            'name' => $db->name,
            'path' => $db->path
        );
    }

    /**
    *Выдача файла на загрузку
    * первый параметр - имя файла, второй (не обязательно) папка. Если не указать, стандартно будет использоваться папка files
    */
    
    public function download($file = null) {
        if ($file == null)
            return false;
        $file = str_replace("_", "", $file);
	
	    $fullpath = "assets/files/" . $file;
	    
        if (!is_file($fullpath))
            return false;

        $dn = Model::factory('download');
        $dn->set_byfile($fullpath);
        $dn->use_resume = true;
//        $dn->mime="application/zip"; //Стандартный для всех MIME-тип
        $dn->download();
    }

    /**
     * Удаление файлов юзера
     * @param type $uid user id    
     */
    public function delete_user_files($uid = null) {
        if ($uid == null)
            return false;

        $sql = "Select file_id as fid FROM user_files WHERE user_id = $uid";
        $db = DB::query(Database::SELECT, $sql)->execute();

        foreach ($db as $fid) {
            Model::factory('practik')->delete_u_link_file($fid['fid']);
        }
    }

}

