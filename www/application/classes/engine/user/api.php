<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * class Engine_User_API
 */
class Engine_User_API {

    public static function instance() {
        return new self;
    }

    /**
     * Проверка, залогинен ли пользователь
     */
    public static function islogged() {
        if (Auth::instance()->logged_in()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Очистка данных формы от мусора
     * @param type $data - массив или строка
     * @param type $type - array | single (одиночный, массив)
     * @return type clear text
     */
    public static function clear($data = null, $type = 'array') {
        if ($type == 'array') {
            if (isset($data)) {
                $result = array();
                foreach ($data as $key => $val) {
                    $val = strip_tags(trim($val));

                    $result[$key] = $val;
                }
                return $result;
            }
        } elseif ($type == 'single') {
            return strip_tags(trim($data));
        }
    }

    /**
     * Возвращает хеш пароля для отправки ключа пользователю
     * при восстановлении пароля.
     * так же используется для проверки с полученными данными от юзера
     * параметром передается username
     */
    public static function pwhashe($str = null) {
        if (!isset($str) and $str == null)
            return false;
        return substr(md5($str), 0, 10);
    }

    /**
     * Pagination
     * $page  - текущая страница
     * $count - количество элементов в базе
     * $ppp   - (Posts per page) количество элементов на страницу
     */
    public static function pagination($page = 1, $count = null, $ppp = 5) {
        if ($count == null)
            return;

        $pages = ceil($count / $ppp); //количество страниц
        $offset = ($page * $ppp) - $ppp; // сколько элементов пропустить в запросе к БД

        if ($pages > 1) {
            $data = array();

            for ($i = 1; $i <= $pages; $i++) {
                if ($i == 1 OR $i == $pages OR $i == $page OR $i == $page - 1 OR $i == $page + 1 OR $i == $page + 2 OR $i == $page + 3)
                    if (!in_array($i, $data))
                        $data[] = $i;
            }

            $arr = array('pages' => $pages, 'offset' => $offset, 'posts' => $ppp, 'current' => $page, 'pages' => $pages, 'data' => $data);

            return $arr;
        }else {
            return;
        }
    }

    /**
     * Проверяет, является ли юзер админом
     */
    public static function is_admin() {
        return $auth = Auth::instance()->logged_in('admin');
    }

    /**
     * Проверяет, является ли юзер лаборантом
     */
    public static function is_lab() {
        return $auth = Auth::instance()->logged_in('laborant');
    }

    /**
     * Проверяет количество незарегистрированных пользователей онлайн
     * @param timeout - время до проверки
      @TODO: Дописать статистику: максимальное кол-во за день
     */
    public static function online($timeout = 10) {

        $no_write = 0;
        $newarr = array();
        $file_name = $_SERVER['DOCUMENT_ROOT'] . '/assets/online.txt';
        $f = fopen($file_name, "r");

        while (!feof($f)) {
            $str = fgets($f);
            $str_arr = explode("|", $str);

            if ($str_arr[0] !== "" AND isset($str_arr[1])) {
                $time = Engine_User_Api::diff($str_arr[0]);

                //если мой ip и меньше 10 минут - не записывать
                if (isset($str_arr[1]) AND (trim($str_arr[1]) == $_SERVER['REMOTE_ADDR']) AND $time['minutes'] < $timeout) {
                    $no_write = 1;
                }

                //Если больше 10 минут - удалить
                if ($time['minutes'] < $timeout) {
                    $newarr[] = $str_arr[0] . "|" . $str_arr[1];
                }
            }
        }
        fclose($f);
        $f = fopen($file_name, "w");

        //запись своих данных, если их нет в файле или просрочены
        if ($no_write == 0) {
            $text = date('d.m.Y H:i:s') . "|{$_SERVER['REMOTE_ADDR']}\r\n";
            fwrite($f, $text);
        }
        //перезапись непросроченных данных
        fputs($f, implode("", $newarr));
        fclose($f);

        unset($no_write, $newarr, $text, $f, $str_arr, $time);
        return count(file($file_name));
    }

    /**
     * разница между датами
     * дату передавать в виде '18.05.2012 05:23:47'
     * $d1 - начальная дата
     * $d2 - конечная дата
     */
    public static function diff($d1 = null, $d2 = null) {
        if ($d1 == null)
            return false;
        if ($d2 == null)
            $d2 = date('d.m.Y H:i:s');

        if (($t1 = strtotime(str_replace('/', '.', $d1))) && ($t2 = strtotime(str_replace('/', '.', $d2)))) {
            $td = abs($t1 - $t2);
            $y = floor($td / 31557600);
            $m = floor(($td - $y * 31557600) / 2629800);
            $d = floor(($td - $y * 31557600 - $m * 2629800) / 86400);
            $h = floor(($td - $y * 31557600 - $m * 2629800 - $d * 86400) / 3600);
            $min = floor(($td - $y * 31557600 - $m * 2629800 - $d * 86400 - $h * 3600) / 60);
            $s = $td - $y * 31557600 - $m * 2629800 - $d * 86400 - $h * 3600 - $min * 60;

            return array(
                'years' => $y,
                'months' => $m,
                'days' => $d,
                'hours' => $h,
                'minutes' => $min,
                'seconds' => $s
            );
        }
        return false;
    }

    /**
     *  Удаляет повторяющиеся пробелы типа:
     * <p>&nbsp;</p> И <p><br /><br></p>
     * @param type $str - текст неформатированный
     * @return type отформатированную строку
     */
    public static function del_br($text) {
        return $text = preg_replace(array("/(<p>(&nbsp;)+<\/p>(\r\n)?)+/", "/(<p>)?(<br( )?(\/)?>( )?)+(<\/p>)?/"), array("<br/>", "<br/>"), $text);
    }

    # -------------- KОНФИГУРАЦИИ ----------------
    /**
     *  Записать конфиг
     * @param type $name
     * @param type $value 
     */

    public static function set_config($name = null, $value = null) {
        Model::factory('config')->set_config($name, $value);
    }

    /**
     *  Возвратить конфиг
     * @param type $name имя конфига
     * @return type значение
     */
    public static function get_config($name = null) {
        return Model::factory('config')->get_config($name);
    }

    /**
     * Проверить, свободный ли конфиг
     * @param type $name
     * @return type 1/0
     */
    public static function is_config($name = null) {
        return Model::factory('config')->is_config($name);
    }

    /**
     *  Удалить конфиг
     * @param type $name имя
     */
    public static function del_config($name = null) {
        Model::factory('config')->del_config($name);
    }

    # -------------- ================= ----------------

    /**
     *  Транслит русского текста
     * @param type $string - строка на латинице
     * @return string строка в транслите
     */
    public static function translit($string = null) {
        if ($string == null)
            return false;

        $table = array(
            "А" => "a", "Б" => "b", "В" => "v",
            "Г" => "g", "Д" => "d", "Е" => "e",
            "Ё" => "yo", "Ж" => "zh", "З" => "z",
            "И" => "i", "Й" => "j", "К" => "k",
            "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r",
            "С" => "s", "Т" => "t", "У" => "u",
            "Ф" => "f", "Х" => "h", "Ц" => "c",
            "Ч" => "ch", "Ш" => "sh", "Щ" => "csh",
            "Ь" => "»", "Ы" => "y", "Ъ" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "yu",
            "а" => "a", "б" => "b", "в" => "v",
            "г" => "g", "д" => "d", "е" => "e",
            "ё" => "yo", "ж" => "zh", "з" => "z",
            "и" => "i", "й" => "j", "к" => "k",
            "л" => "l", "м" => "m", "н" => "n",
            "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u",
            "ф" => "f", "х" => "h", "ц" => "c",
            "ч" => "ch", "ш" => "sh", "щ" => "csh",
            "ь" => "", "ы" => "y", "ъ" => "",
            "э" => "e", "ю" => "yu", "я" => "ya",
            " " => "_", "(" => "", ")" => "",
        );

        return str_replace(array_keys($table), array_values($table), $string);
    }

    # -------------- ================= ----------------
    /**
     *  Uploader для загрузки через Uploadify.
     * Для каждого загружаемого файла флеш вызывает данную функцию.
     * @param type $path - путь, куда сохранять файл( папка от корня сайта)
     * @return string имя загруженого файла
     */

    public static function uploader($path = null, $antiplag = null) {
        if ($path == null)
            return false;

        //полный путь к папке        
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . $path . "/";
        $fileElementName = 'qqfile';       
        $uid = Engine_User_U::uid();
        $filename = $_FILES[$fileElementName]['name'];
        $hashe = null;
                
        
        //расширение файла
        $ext = substr($filename, strrpos($filename, "."));
        //имя нового файла
        $rand_name = date('ymdHis') . rand(1, 99) . $ext;
        //полный путь и имя перемещаемого файла
        $targetFile = str_replace('//', '/', $targetPath) . $rand_name;

        if (move_uploaded_file($_FILES[$fileElementName]['tmp_name'], $targetFile)) {
            echo json_encode(array('success' => true));
            //генерирую хэш файла
            $hashe = md5_file($targetFile);
        } else {
            echo $filename . " не загружено!<br>";
        }
        return array('name' => $rand_name, 'hashe' => $hashe, 'old_name' => $filename);
    }

//--end!

    /**
     *  ПРоверяет мобильную версию браузера
     * @return type 1\0
     */
    public static function is_mobile() {
        return Request::user_agent('mobile');
    }

    /**
     * Возвращает код авторизации по ид пользователя. НУжно передать ид юзера Для форс-логина
     * @param type $id ид пользователя
     * Код должен иметь вид "ИдЮзера_МД5(идЮзера)"
     */
    public static function check_forcecode($id = null) {
        if ($id == null)
            return false;
        return $id . "-" . substr(md5($id), 0, 4);
    }

    /**
     * Зачистка всех файл-куков, которые уже устарели.
     */
    public static function delete_ses_files() {
        $path = $_SERVER['DOCUMENT_ROOT'] . "/sessions_directory/";
        $life = 4; // время жизни файла
        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                if ($file !== "." && $file !== "..") {
                    $ftime = date("d.m.Y H:i:s", filectime($path . "/" . $file));
                    $curtime = date("d.m.Y H:i:s");
                    $t = Engine_User_API::diff($ftime);
                    if ($t['days'] > 0 OR $t['hours'] > $life) {
                        @unlink($path . "/" . $file);
                    }
                }
            }
        }
    }//end--
    
    
    /**
     * Хелп для записи действия. передаем тип и ид объекта<br/>
     * try-retask - запрос на пересдачу(taskID!!!bad!)<br/>
     * test - прошел тест(tID)<br/>
     * login - авторизовался<br/>
     * exit - покинул учетную запись<br/>
     * save-pr - сохранил практическое(коммент)(prID)<br/>
     * add-file - юзер добавил файл (prID)<br/>
     * points - просмотр табеля <br/>
     * lesson - просмотр лекции <br/>
     * msg - сообщения <br/>
     * forum - просмотр форума <br/>
     * @param type $type type of actions
     * @param type $id - object id
     * @return boolean false if not params
     */
    public static function act($type = null, $id = null){
        if($type==null || $id == null) return false;
        
        $uid = Engine_User_U::uid();
        if(!Engine_User_API::is_admin())
            Model::factory('action')->_new(array($type, $uid,  $id, date('d.m.y H:i')));
    }
    
    
        /**
     * Функция, которая добавляет только уникальные записи
     * @param type $table table_name
     * @param type $vars array('field_name'=>'value', 'fiels_name2'=>'value2')
     * @return string строку SQL запроса
     */
     public static function insert_unique($table, $vars) {
        if (count($vars)) {
            $table = mysql_real_escape_string($table);
            $vars = array_map('mysql_real_escape_string', $vars);

            $req = "INSERT INTO `$table` (`" . join('`, `', array_keys($vars)) . "`) ";
            $req .= "SELECT '" . join("', '", $vars) . "' FROM DUAL ";
            $req .= "WHERE NOT EXISTS (SELECT 1 FROM `$table` WHERE ";

            foreach ($vars AS $col => $val)
                $req .= "`$col`='$val' AND ";

            return $req = substr($req, 0, -5) . ") LIMIT 1";            
            
        }

        return False;
    }
    
    //запись хэша всех файлов
    public static function find_file($dir,$file){
    	$hashe = md5_file($dir.$file);
    	$sql = "UPDATE `user_files` SET `hashe`= '".$hashe."' WHERE 
    	file_id IN (Select id from files Where url = '".$file."')";
		$db = DB::query(Database::UPDATE, $sql)->execute();


    }

}
