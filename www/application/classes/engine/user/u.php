<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * class Engine_User_API
 */
class Engine_User_U {

    public static function instance() {
        return new self;
    }

    //возвращает ид юзера
    public static function uid() {
        $login = Engine_User_API::islogged();
        if (!$login)
            return false;
        return Auth::instance()->get_user()->id;
    }

    /**
     * Количество заданий пользователя
     * @return type int
     */
    public static function task_counter() {
        $login = Engine_User_API::islogged();
        if (!$login)
            return false;

        $count = Model::factory('task')->count_user_tasks(self::uid());
        if ($count > 0)
            echo " (" . $count . ")";
    }

    /**
     *
     * @return type Массив юзеров по группам
     */
    public static function users_list() {
        return Model::factory('user')->get_users();
    }

    //количество новых сообщений
    public static function msg_counter() {
        $login = Engine_User_API::islogged();
        if (!$login)
            return false;
        echo Model::factory('message')->_count();
    }
    
    /**
     * Возвращает либо юзера аватарку, либо пустую картинку
     * @param type $uid ид юзера
     * @return boolean путь к аватарке
     */
    public static function avatar($uid = null, $size ='-min'){
        if($uid == null) return false;
        $path =  $_SERVER['DOCUMENT_ROOT'] .'/assets/files/avatar/'.$uid.".jpg";
        $dir = '/assets/files/avatar/';
        $noava = $dir .'no' . $size . '.jpg';
        $ava = $dir . $uid .$size . '.jpg';
        return file_exists($path) ? $ava : $noava;
    }
}