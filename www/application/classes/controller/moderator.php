<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Moderator extends Controller_Template_User {

    /**
     * Создание расписания
     */
    public function action_index() { 
        if(!Engine_User_API::is_lab()) $this->request->redirect("/");
        $this->template->scripts[] = 'assets/js/schedule.js';
        $this->template->scripts[] = 'assets/js/tinyinit.js';
        $this->template->scripts[] = 'assets/js/tiny_mce/tiny_mce_gzip.js';                
        $this->template->content = View::factory('pages/schedule')->bind('data', $data);
        //очищаю сессии

        $data = Model::factory('post')->get_posttype('schedule');
    }

}

// End  Index
