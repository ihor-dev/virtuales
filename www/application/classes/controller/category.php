<? # CONTROLLER CATEGORY # ?>
<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Category extends Controller_Template_Main {

    //количество записей на страницу
    var $posts = 5;

    public function action_try() {
        $db = ORM::factory('coockid')
                ->set('status', 0)
                ->save();
    }

    /**
     * Все записи без сортировки
     */
    public function action_page() {
        $page = $this->request->param('page', 1);
        $categories = Model_Category::get_cat_names();

        if (isset($_POST['category']) AND $_POST['category'] != '') {
            $id = Engine_User_Api::clear($_POST);
            $title = Model_Category::get_cat_name($id['category']);
            $category_id = $id['category'];
        } else {
            $title = null;
            $category_id = null;
        }
        $count = Model::factory('post')->count_posts($category_id);
        $pagin = Engine_User_Api::pagination($page, $count, $this->posts);

        $data = Model::factory('post')->get_posts($category_id, $pagin['offset'], $pagin['posts']);

        $this->template->title = "Категории";
        $this->template->content = View::factory('pages/category-all-posts')
                ->bind('data', $data)
                ->bind('categories', $categories)
                ->bind('title', $title)
                ->bind('pagin', $pagin);
    }

    //Свободный просмотр записи
    public function action_view() {

        $id = $this->request->param('page', 0);
        $data = Model::factory('post')->get_post($id);

        $this->template->title = $data['name'];
        $this->template->content = View::factory('pages/view-post')
                ->bind('data', $data);
    }

    
    /**
     * удаление записи админом
     */
    public function action_del() {
        if (!Engine_User_Api::is_admin())
            die('Protected! Go to the main page!');

        $id = $this->request->param('page');
        if (isset($id)) {
            Model::factory('post')->post_delete($id);
        }

        Request::initial()->redirect('/category/page/');
    }

}

// End  Category
