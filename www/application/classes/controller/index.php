<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Controller_Template_Main {

    /**
     * Главная страничка
     */
    public function action_index() {

        $this->template->content = View::factory('pages/index');
    }

    public function action_schedule() {   
        $template = "/templates/dafault";
        $data = Model::factory('post')->get_posttype('schedule');
        $this->template->title="Расписание";
        $this->template->content = View::factory('pages/view-post')
                ->bind('data', $data[0]);
    }
    
    public function action_test(){
	$dir = $_SERVER['DOCUMENT_ROOT'].'/assets/files/user/'; 
		if (is_dir($dir)) {

		   if ($dh = opendir($dir)) {
			   while (($file = readdir($dh)) !== false) {
				   Engine_User_API::find_file($dir,$file);
			   }
			   closedir($dh);
		   }
		} 
    }

    public function action_clear_temp_files() {            
        //перебор файлов внутри открытой папки
        Engine_User_API::delete_ses_files();            
    }
       

    //Выдача файлов на загрузку
    public function action_dn() {
        $file = $this->request->param('id');

        Model::factory('file')->download($file);
    }
    


}

// End  Index
