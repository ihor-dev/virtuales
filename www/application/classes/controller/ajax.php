<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller_Template_Ajax {

    private static function _bool2string($bool) {
        return $bool ? 'success' : 'failure';
    }

    private static function _data2json($data) {
        return json_encode($data);
    }

    public function action_send() {

        $page = $this->request->param();

        switch ($page['page']) {
            //Авторизация -----------
            case('auth'):
                $login = Arr::get($_POST, 'login');
                $password = Arr::get($_POST, 'password');

                $result = Auth::instance()->login($login, $password);
                if ($result) {                    
                    //add new action                                    
                    Engine_User_API::act('login', '0');
                    
                    $url = Model::factory('user')->role_redirect();
                    $this->set_ajax_response($url);
                } else {
                    $this->set_ajax_response($result);
                }
                break;
            //проверяю, авторизован ли юзер
            case('islogged'):
                if (!Engine_User_API::islogged())
                    echo "0";
                break;

            //запись последнего времени пребывания на сайте
            case('ionline'):
                Model::factory('user')->ionline($_POST['i']);
                break;
            //проверить юзер онлайн?(месседжер)
            case('is_online'):
                echo Model::factory('user')->is_online($_POST['from']);
                break;
            case('users-online'):
                $data = Model::factory('user')->online_users();
                echo json_encode(array('u'=>$data, 'count'=> count($data)));
                break;

// REGISTER ------------
            //валидация
            case('register_checkvalidation'):
                $res = Model::factory('user')->register_valid($_POST);
                $this->set_ajax_response($res);
                break;

            //регистрация
            case('register'):
                $res = Model::factory('user')->register($_POST);
                echo $res;
                break;

            case('delete_user'):
                Model::factory('user')->delete_user($_POST['uid']);

//TEST - запрос вопроса для юзера
            case('quetion'):
                $data = Model::factory('test')->test();
                echo json_encode($data);
                break;
            //юзер дал ответ
            case('answer'):
                $result = Model::factory('quetion')->check_answer($_POST);
                $this->set_ajax_response($result);
                break;
            //вывод оценки пользователя
            case('point'):
                //add new action                                    
                Engine_User_API::act('test', Session::instance()->get('test_id'));
                echo Model::factory('result')->result();
                break;


            //создать тест
            case('test-cr'):
                $result = Model::factory('test')->test_create($_POST['name']);
                $this->set_ajax_response($result);
                break;
            //количество вопросов в тесте
            case('count-quetions'):
                $result = Model::factory('test')->count_quetions();
                $this->set_ajax_response($result);
                break;
            //добавить вопрос
            case('add_answer'):
                $result = Model::factory('quetion')->add_answer($_POST);
                break;
            //возвратить ключи теста
            case('get_true_keys'):
                $test_id = $_POST['id'];
                $result = Model::factory('quetion')->get_true_keys($test_id);
                echo json_encode($result);
                break;

            //удалить вопрос
            case('delete_quetion'):
                $id = $_POST['id'];
                $result = Model::factory('quetion')->delete_quetion($id);
                echo $result;
                break;

            //Сохранить редактируемый вопрос
            case('save_edit_quetion'):
                Model::factory('quetion')->update_quetion();
                break;
            //загружает ключи правильных ответов для редактируемого вопроса и размечает вопросы
            case('edit_quetion_keys'):
                $id = $_POST['id'];
                $result = Model::factory('key')->get_quetion_keys($id);
                echo json_encode($result);
                break;
            //удалить вариант ответа при редактировании
            case('delete_answer'):
                $id = $_POST['id'];
                Model::factory('answer')->remove_answer($id);
                break;
            //редакт.вопроса/добавление нового варианта ответа
            case('add_new_answer'):
                Model::factory('answer')->add_answer($_POST);
                break;
            //редакт.вопроса/запрос вопроса и вар.ответов
            case('edit_get_answers'):
                $id = $_POST['id'];
                $result = Model::factory('quetion')->get_quetion($id, false);
                echo json_encode($result);
                break;
            //запрос инфы для редактирования свойств теста
            case('edit_get_test_info'):
                $result = Model::factory('test')->get_test_info($_POST['id']);
                echo json_encode($result);
                break;
            //обновление информации о тесте
            case('edit_save_test_info'):
                Model::factory('test')->upd_test_info($_POST);
                break;
            //удалить тест
            case('test_delete'):
                Model::factory('test')->test_delete($_POST['id']);
                break;

#ГРУППЫ----
            //Обновление данных
            case('group_upd'):
                $result = Model::factory('group')->upd($_POST);
                $this->set_ajax_response($result);
                break;

            //добавить группу
            case('group_add'):
                $result = Model::factory('group')->_add($_POST);
                $this->set_ajax_response($result);
                break;

            //удалить группу
            case('group_del'):
                $result = Model::factory('group')->_delete($_POST['id']);
                $this->set_ajax_response($result);
                break;
            
            //добавить в группу пользователей
            case('group_addusers'):
                $result = Model::factory('group')->add_users($_POST);
                break;
            
            //список пользователей группы
            case('group_users'):
                $result = Model::factory('group')->get_groupusers($_POST['grid']);
                echo json_encode(array('d'=>$result));
                break;
            
            //удалить пользователя с группы
            case('group_deluser'):
                Model::factory('group')->del_userfromgroup($_POST['uid'], $_POST['group_id']);
                break;

# АДМИН-Пользователи
            //Новые регистрации--------
            //удалить заявки на регистрацию
            case('new-user-active'):
                Model::factory('user')->active_new_users($_POST['data']);
                break;

            //одобрить заявки
            case('new-user-delete'):
                Model::factory('user')->delete_new_users($_POST['data']);
                break;
            //установить новый пароль юзеру
            case('admin-set-user-pw'):
                Model::factory('user')->set_password($_POST['password'], $_POST['uID']);
                break;
            //запрос логин юзера
            case('admin-get-user-login'):
                echo Model::factory('user')->get_login($_POST['uID']);
                break;
            //Visitors
            case('admin-visitors'):
                $data = Model::factory('user')->visitors($_POST['time']);
                echo json_encode(array('u' => $data));
                break;

# АДМИН - задания
            //список юзеров по выбранному ид-группы
            case('task-get-users'):
                $result = Model::factory('user')->get_group_users($_POST['id']);
                echo json_encode($result);
                break;
            //добавление задания
            case('tasks-set'):
                $result = Model::factory('task')->create_task($_POST);
                $this->set_ajax_response($result);
                break;
            //удалить задания
            case('task_delete'):
                $result = Model::factory('task')->delete_task($_POST['id']);
                $this->set_ajax_response($result);
                break;
            //добавить в задание юзеров
            case('add-users-task'):
                Model::factory('task')->add_users_to_task($_POST);
                break;
            case('get_task_info'):
                $result = Model::factory('task')->get_task_info($_POST['tid']);
                echo json_encode($result);
                break;
            //удаление юзера из задания
            case('del-pr_user'):
                Model::factory('task')->delete_taskuser($_POST);
                break;
            //оценка за лекцию
            case('set_lesson_mark'):
                $arr = explode("_", $_POST['data']);
                $data = array(
                    'task_id' => $arr[0],
                    'result' => $arr[2],
                    'user_id' => $arr[1]
                );
                Model::factory('result')->add_result($data);
                #отмечаю как выполнено                
                Model::factory('taskuser')->try_retask($data['task_id'], "complete", $data['user_id']);
                break;
            //Все задания
            case('get-all-tasks'):
                $result = Model::factory('task')->get_by_group($_POST['grID']);
                echo json_encode(array('data'=>$result));
                break;
            
            //скачать все результаты задания на проверку
            case('download_task');
            	$result = Model::factory('task')->get_html($_POST['tID']);
            	echo json_encode(array('url'=>"http://".$_SERVER['SERVER_NAME']."/index/dn/".$result));

            break;
            
# АДМИН - журнал -----------
            case('get-gr_tasks'):
                $result = Model::factory('task')->get_bygroupid($_POST['grid']);
                echo json_encode(array('ts' => $result));
                break;
            case('get_journal'):
                $result = Model::factory('task')->get_journal($_POST);
                echo json_encode(array('jr' => $result));
                break;

# АДМИН - лекции------------
            //новая лекция
            case('create_post'):
                Model::factory('post')->add_post($_POST);
                break;
            case('update_post'):
                $result = Model::factory('post')->post_update($_POST);
                $this->set_ajax_response($result);
                break;
            case('delete_post'):
                Model::factory('post')->post_delete($_POST['id']);
                break;
            case('share_post'):
                $result = Model::factory('post')->share_post($_POST);
                break;
            //список групп, в которых доступен пост
            case('get_post_shared_groups'):
                $result = Model::factory('post')->get_shared_post_groups($_POST['post_id']);
                echo json_encode(array('g' => $result));
                break;
            case('delete_share_post'):
                Model::factory('post')->delete_shared_group($_POST['pid'], $_POST['grid']);
                break;
            //sort posts
            case('sort_posts'):
                Model::factory('post')->sort_posts($_POST['id']);
                break;

# АДМИН - категории----
            //Обновление данных
            case('cat_upd'):
                $result = Model::factory('category')->update_category($_POST);
                $this->set_ajax_response($result);
                break;

            //добавить категорию
            case('cat_add'):
                $result = Model::factory('category')->add_category($_POST['name']);
                $this->set_ajax_response($result);
                break;

            //удалить категорию
            case('cat_del'):
                $result = Model::factory('category')->del_category($_POST['id']);
                $this->set_ajax_response($result);
                break;

#Галерея-----------
            //загрузить файл
            case('gallery_add_file'):
                echo Model::factory('image')->upload_file($page['id']);
                break;

            //новый альбом
            case('gallery_add'):
                echo Model::factory('gallery')->_add($_POST['name']);
                break;
            //все альбомы
            case('gallery_get-all'):
                $result = Model::factory('gallery')->get_all();
                echo json_encode($result);
                break;
            //обновить альбом
            case('gallery_upd'):
                Model::factory('gallery')->upd($_POST);
                break;
            //Удалить альбом
            case('gallery_del'):
                Model::factory('gallery')->del($_POST['id']);
                break;

            //добавить ид альбома и названия картинок
            case('gallery_add-galllery-image'):
                Model::factory('image')->add_image($_POST);
                break;
            //галерея: запрос галерей и картинок в них
            case('gallery_get-galleries'):
                $json = Model::factory('gallery')->get_galleries();
                echo json_encode($json);
                break;

            case('gallery_del-img'):
                Model::factory('image')->del_image($_POST['id']);
                break;

//АДМИН:Практические
            //добавить файл админ
            case('upload_files'):
                $file_id = Model::factory('file')->_add();
                $pr_id = $page['id'];
                Model::factory('practik')->link_file($file_id['fID'], $pr_id);
                break;
            //сохранить практическое
            case('practic_save'):
                echo Model::factory('practik')->_add($_POST['name']);
                break;

            case('practic_update'):
                echo Model::factory('practik')->_upd();
                break;
            //удалить практ.задание
            case('practic_delete'):
                echo Model::factory('practik')->_delete($_POST['id']);
                break;
            //удалить прикрепленный файл к практ.заданию
            case('unlink_pr_file'):
                Model::factory('practik')->delete_link_file($_POST['id']);
                break;
            //пользователь прикрепляет файлы к практ.заданию
            case('upload_user_files'):
                $str = explode("_", $page['id']);
                $pr_id = $str[0];
                $file_id = Model::factory('file')->_add('user', "1");
                $uID = $str[1];
                $tID = $str[2];
                Model::factory('practik')->link_user_file($file_id['fID'], $pr_id, $uID, $file_id['hashe'], $tID);
                                
                //add new action                                    
                Engine_User_API::act('add-file', $pr_id);
                break;
            //список прикрепленных пользователем файлов
            case('update_user_file_list'):
                $result = Model::factory('practik')->get_user_files($_POST['uid'], $_POST['prid']);
                echo json_encode($result);
                break;
            //удалить прикрепленный пользователем файл к практ.заданию
            case('unlink_u_pr_file'):
                Model::factory('practik')->delete_u_link_file($_POST['id']);
                break;

            //добавить комментарий пользователя
            case('link_comment'):
                $commentID = Model::factory('practik')->is_comment($_POST['prid'], Engine_User_U::uid());
                
                //add new action                                    
                Engine_User_API::act('save-pr', $_POST['prid']);
                
                if (isset($commentID)) {
                    //обновить комментарий
                    $commentID = Model::factory('comment')->_add($_POST['text'], Engine_User_U::uid(), $commentID);
                    Model::factory('taskuser')->try_retask($_POST['tid'], "wait", Engine_User_U::uid());
                } else {
                    //создать комментарий
                    $commentID = Model::factory('comment')->_add($_POST['text'], Engine_User_U::uid(), $commentID);
                    Model::factory('practik')->link_comment($_POST['prid'], $commentID,  $_POST['tid']);
                    //отметить задание выполнено
                    Model::factory('taskuser')->try_retask($_POST['tid'], "wait", Engine_User_U::uid());
                }
                break;

            //запрос ответа к практ.заданию юзера
            case('get_pr_comment'):
                $result = Model::factory('comment')->get_comment($_POST['prID'], $_POST['uID']);
                echo $result;
                break;

            //вывод в заданиях практического задания для проверки
            case('get_user_practik_to_admin'):
                $pr_id = Model::factory('practik')->get_practik_id($_POST['taskID']);
                $files = Model::factory('practik')->get_user_files($_POST['userID'], $pr_id);
                $comment = Model::factory('comment')->get_comment($pr_id, $_POST['userID']);
                $data['files'] = $files;
                $data['comment'] = $comment;
                $data['taskID'] = $_POST['taskID'];
                $data['userID'] = $_POST['userID'];
                echo json_encode($data);
                break;
            //оценка за практическое
            case('set_practik_mark'):
                $data = array(
                    'task_id' => $_POST['taskID'],
                    'result' => $_POST['mark'],
                    'user_id' => $_POST['userID']
                );
                #отмечаю как выполнено                
                Model::factory('taskuser')->try_retask($_POST['taskID'], "complete", $_POST['userID']);
                
                echo Model::factory('result')->add_result($data);
                break;
            
            //сортировка практических в админке
            case('sort_practiks'):
                Model::factory('practik')->sort($_POST['ids']);
                break;
#Profile--------------
            //обновить информацию пользователя
            case('profile_upd'):
                Model::factory('user')->upd_user_info($_POST);
                break;
            case('change_password'):
                echo Model::factory('user')->change_password($_POST);
                break;
            //avatar-upload
            case('avatar_upl'):
               echo  Model::factory('user')->ad_avatar($_FILES);
                break;
            //avatar-resize
            case('resize-avatar'):
                Model::factory('user')->resize_avatar($_POST);
                break;
#Points---------------
            //запрос на пересдачу (юзер)
            case('try_retask'):
                Model::factory('taskuser')->try_retask($_POST['task_id'], "try_retask", Engine_User_U::uid());
                //add new action                                    
                Engine_User_API::act('try-retask', $_POST['task_id']);
                break;
            //препод подтвердил пересдачу
            case('confirm_teacher_retask'):
                Model::factory('taskuser')->try_retask($_POST['tid'], "retask", $_POST['uid']);
                break;
            //открыть/закрыть практическую
            case('practik_status'):

                if ($_POST['status'] == "1") {
                    Model::factory('taskuser')->try_retask($_POST['tid'], "retask", $_POST['uid']);
                } else {
                    Model::factory('taskuser')->try_retask($_POST['tid'], "complete", $_POST['uid']);
                }
                break;

            //оценка
            case('check_mark'):
                Model::factory('result')->retask_result($_POST);
                break;
#messages-------------
            case('send_message'):
                echo Model::factory('message')->_add($_POST);
                break;
            case('get_messages'):
                echo json_encode(Model::factory('message')->_get($_POST));
                break;
            case('get_new_messages'):
                echo json_encode(Model::factory('message')->new_message($_POST['id']));
                break;

            case("delete_messages"):
                Model::factory('message')->_delete($_POST['from']);
                break;

# forum------------------
            //новый форум
            case('add-forum'):
                Model::factory('forum')->_new($_POST['fname']);
                break;
            //новая тема
            case('add-topic'):
                Model::factory('topic')->_new($_POST);
                break;
            //новое сообщение
            case('add-message'):
                echo Model::factory('fmessage')->_new($_POST);
                break;
            //удалить сообщение
            case('del-fmessage'):
                Model::factory('fmessage')->_del($_POST['pid']);
                break;
            //удалить топик
            case('del-topic'):
                Model::factory('topic')->_del($_POST['tid']);
                break;
            //удалить форум
            case('del-forum'):
                Model::factory('forum')->_del($_POST['fid']);
                break;
            //загрузка следующих сообщений
            case('get_fmessages'):
                $res = Model::factory('topic')->read($_POST['tid'], "5", $_POST['offset']);
                echo json_encode(array('m' => $res));
                break;
            //редактируем топик
            case('update-topic'):
                Model::factory('topic')->_update($_POST['id'], $_POST['name'], $_POST['text']);
                break;
            //новости
            case('set-news'):
                Model::factory('topic')->set_news($_POST['id'], $_POST['type']);
                break;


# Predmets -------------------------------------
            //new predmet
            case('add_predmet'):
                $result = Model::factory('predmet')->_add($_POST['name']);
                echo $result;
                break;
            //del predmet
            case('del-predmet'):
                Model::factory('predmet')->_del($_POST['id']);
                break;
            //add users
            case('pr-add-users'):
                Model::factory('predmet')->_add_user($_POST);
                break;
            //get user's list & linked users to predmet
            case('predmet-get-users'):
                $data['users'] = Model::factory('user')->get_group_users($_POST['grID']);
                $data['uids'] = Model::factory('predmet')->get_pr_users($_POST['prID'], $_POST['grID']);
                echo json_encode($data);
                break;
            //remove user from predmet
            case('predmet-del-user'):
                Model::factory('predmet')->del_user($_POST['id']);
                break;
            
# Actions ------------------------------------------
            //get all actions admin
            case('get_actions'):
                $data = Model::factory('action')->get_all();
                echo json_encode(array('data'=>$data));
                break;
            //clear actions
            case('clear_actions'):
                Model::factory('action')->_clear();
        }
    }

    public function action_getmodal() {
        //ПРАВИЛЬНЫЙ ЗАПРЕТ КЕШИРОВАНИЯ
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: " . date("r"));

        $page = $this->request->param();

        switch ($page['page']) {

            case('edit_quetion'):
                $this->set_ajax_response(View::factory('blocks/ajax/test-edit'));
                break;

            case('edit_test_info'):
                $this->set_ajax_response(View::factory('blocks/ajax/test-info-edit'));
                break;

            case('gallery'):
                $this->set_ajax_response(View::factory('blocks/ajax/gallery'));
                break;
            case('check_practik'):
                $this->set_ajax_response(View::factory('blocks/ajax/check_practik'));
                break;
            case('share_post'):
                $groups = Model::factory('group')->get_all();
                $this->set_ajax_response(View::factory('blocks/ajax/share_lessons')->bind('group', $groups));
                break;
            case('adm-user-info'):
                $this->set_ajax_response(View::factory('blocks/ajax/admin-user-info')->bind('group', $groups));
                break;
            case('modal-auth'):
                $this->set_ajax_response(View::factory('pages/meta/auth'));
                break;
        }
    }

    public function action_config() {
//ПРАВИЛЬНЫЙ ЗАПРЕТ КЕШИРОВАНИЯ
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: " . date("r"));

        $page = $this->request->param();

        switch ($page['page']) {

            case('get_config'):
                $result = Engine_User_API::get_config($_POST['name']);
                $this->set_ajax_response($result);
                break;

            case('set_config'):
                $result = Engine_User_API::set_config($_POST['name'], $_POST['value']);
                $this->set_ajax_response($result);
                break;
        }
    }

}

//--END AJAX
