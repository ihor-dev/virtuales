<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Template_Engine_Basic extends Controller_Template {

    public $template = '';

    /**
     * Initialize properties before running the controller methods (actions),
     * so they are available to our action.
     */
    public function before() {
        //сначала проверяю мобильный или нет
        $this->is_mobile();

        // Run anything that need ot run before this.        
        parent::before();
        
        if ($this->auto_render)
        {
            // Initialize empty values
            $this->template->title = '';
            $this->template->meta_keywords = '';
            $this->template->meta_description = '';
            $this->template->meta_copywrite = '';
            $this->template->header = '';
            $this->template->content = '';
            $this->template->footer = '';
            $this->template->styles = array();
            $this->template->scripts = array();
        }
    }
    
    //экшн, в котором разные темы выводятся для мобильного и стационарника
    public function is_mobile(){                
        if(Request::user_agent('mobile')){
            $this->template = 'templates/mobile';
        }else{
            $this->template = 'templates/default';
        }
        
    }

    /**
     * Fill in default values for our properties before rendering the output.
     */
    public function after() {
        if ($this->auto_render)
        {
            // Define defaults
            $styles = array(
                'assets/css/style.css',         //2
                'assets/css/framework-min.css'  //1
            );
            
    // if(Request::user_agent('mobile'));
            $scripts = array(
                'assets/js/scriptTooltip.js',
                'assets/js/iepng.js',//4
                'assets/js/modal.js',//3
                'assets/js/script.js',//2
                'assets/js/jQuery.js' //1
            );

            // Add defaults to template variables.
            $this->template->styles = array_reverse(array_merge($this->template->styles, $styles));
            $this->template->scripts = array_reverse(array_merge($this->template->scripts, $scripts));
        }

        //запись юзера онлайн
        //Engine_User_Api::online(5);

        // Run anything that needs to run after this.
        parent::after();
    }

}