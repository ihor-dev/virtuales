<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Template_Engine_Ajax extends Controller_Template {

    public $template = null;
    public $content = null;

    private function _configure() {
        $this->template = 'templates/ajax';
    }

    private function _initialize() {
        $this->template->data = array('content' => null);
    }

    private function _extended() {
        $this->template->data['content'] = $this->content;
    }

    public function before() {
        $this->_configure();
        parent::before();

        if ($this->auto_render) {
            $this->_initialize();
        }
    }

    public function after() {
        if ($this->auto_render) {
            $this->_extended();
        }

        parent::after();
    }

    public function set_ajax_response($content) {
        $this->content = $content;
    }

}