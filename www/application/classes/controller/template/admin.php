<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Template_Admin extends Controller_Template_Engine_Basic {

    public $template = 'templates/default';

    /**
     * Initialize properties before running the controller methods (actions),
     * so they are available to our action.
     */
    public function before() {
        // Run anything that need ot run before this.
        parent::before();

        $auth = Auth::instance();
        if ($auth->logged_in('admin') == 0)
        //Session::instance()->set('uri',$this->request->uri());
            Request::initial()->redirect('/auth');

        if ($this->auto_render)
        {
            // Initialize empty values
            $this->template->title = '';
            $this->template->meta_keywords = '';
            $this->template->meta_description = '';
            $this->template->meta_copywrite = '';
            $this->template->header = '';
            $this->template->content = '';
            $this->template->footer = '';
            $this->template->styles = array();
            $this->template->scripts = array();
        }
    }

    /**
     * Fill in default values for our properties before rendering the output.
     */
    public function after() {
        if ($this->auto_render)
        {
            // Define defaults
            $styles    = array();
            $scripts = array(         
            );

            // Add defaults to template variables.
            $this->template->styles  = array_reverse(array_merge($this->template->styles, $styles));
            $this->template->scripts = array_reverse(array_merge($this->template->scripts, $scripts));
        }

        //Вывожу заголовки
        $this->template->title = $this->title;

        // Run anything that needs to run after this.
        parent::after();
    }

}