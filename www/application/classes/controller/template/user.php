<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Template_User extends Controller_Template_Engine_Basic {

    /**
     * Шаблон для страниц с авторизацией
     */
    public $template = 'templates/default';

    /**
     * Initialize properties before running the controller methods (actions),
     * so they are available to our action.
     */
    public function before() {
        // Run anything that need ot run before this.
        parent::before();

        //Проверка авторизации юзера
        if (!Auth::instance()->logged_in())
            Request::initial()->redirect('auth');
    }

    /**
     * Fill in default values for our properties before rendering the output.
     */
    public function after() {

        if ($this->auto_render)
        {
            // Define defaults
            //$styles    = array();
//            $scripts = array(                
//                
//            );

            // Add defaults to template variables.
            //$this->template->styles  = array_reverse(array_merge($this->template->styles, $styles));
            //$this->template->scripts = array_reverse(array_merge($this->template->scripts, $scripts));
        }

        parent::after();
    }

}