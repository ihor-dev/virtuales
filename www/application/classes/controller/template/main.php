<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Template_Main extends Controller_Template_Engine_Basic {

    /**
     * Шаблон для страниц, не требующих авторизации
     */
    public $template = 'templates/default';

    /**
     * Initialize properties before running the controller methods (actions),
     * so they are available to our action.
     */
    public function before() {
        // Run anything that need ot run before this.
        parent::before();
        
    }

    /**
     * Fill in default values for our properties before rendering the output.
     */
    public function after() {
        // Run anything that needs to run after this.
        parent::after();
    }

}