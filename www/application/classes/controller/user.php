<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Template_User {
    
    
    public function action_index() {
        $data = Model::factory('post')->get_shared_posts(Engine_User_U::uid());
       
        $res   = Model::factory('topic')->get_all(4,"news");
        $this->template->content = View::factory('pages/read')
                ->bind('data', $data)
                ->bind('news', $res['topics']);
    }

    /**
     * Задания пользователя
     */
    public function action_task() {
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/user-task.js';
        $this->template->content  = View::factory('pages/tasks')->bind('data', $data);
        $prID = $this->request->param('id', 0);

        //очищаю сессии
        Model::factory('test')->clear_sessions();
        $data     = Model::factory('task')->get_tasks(Engine_User_U::uid(), 'test',$prID);
    }

    //табель оценок
    public function action_points() {
        $this->template->content  = View::factory('pages/points')->bind('data', $data)->bind('groups', $ugroups);
        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';
        $this->template->scripts[] = 'assets/js/user-points.js';
        $uid = Engine_User_U::uid();
        $ugroups = Model::factory('group')->get_usergroups($uid, 'name');
        $data = Model::factory('result')->get_results($uid);
        
        //add new action                                    
        Engine_User_API::act('points', "0");
    }

    //просмотр лекций
    public function action_read() {
        $id         = $this->request->param('id', 0);
        $task_id = $this->request->param('task_id', 0);
        $data     = Model::factory('post')->get_post($id);
        $get = $this->request->param('param',0);
        
        if($get == 'get')            
            Model::factory ('taskuser')->sign_task(Engine_User_U::uid(), $task_id);

        if ($task_id == 0)
            die('отсуствует id леции. Обратитесь к администратору');

        //Отметка о выполнении задания
        Model::factory('taskuser')->report_task($task_id);
        
        //add new action                                    
        Engine_User_API::act('lesson', $id);

        $this->template->title = $data['name'];
        $this->template->content = View::factory('pages/view-post')
                ->bind('data', $data);
    }

    //просмотр расшареной лекции
    public function action_lesson() {
        $id = $this->request->param('id', 0);
        $data = Model::factory('post')->get_post($id);
        $this->template->title = $data['name'];
        $this->template->content = View::factory('pages/view-post')
                ->bind('data', $data);
        
        //add new action                                    
        Engine_User_API::act('lesson', $id);
    }

    //просмотр практических заданий
    public function action_practik() {
        $this->template->scripts[]     = 'assets/js/user-practik.js';
        if (!Engine_User_API::is_mobile()) {
            $this->template->styles[]  = 'assets/css/jHtmlArea.css';
            $this->template->scripts[] = 'assets/js/visiv-min/jHtmlArea-0.7.0.min.js';
            $this->template->scripts[] = 'assets/js/jquery.form.js'; // NEW jquery uplod
            $this->template->styles[]  = 'assets/css/fineuploader.css';
            $this->template->scripts[] = "assets/js/jquery.fineuploader.js";

            $this->template->scripts[] = 'assets/js/jquery.blockUI.js'; //messages
        }

        $practikID = $this->request->param('id', 0);
        $taskID = $this->request->param('task_id', 0);
        $get = $this->request->param('param',0);
        
        if($get == 'get')            
            Model::factory ('taskuser')->sign_task(Engine_User_U::uid(), $taskID);

        $data = Model::factory('practik')->get_practik($practikID);
        $data['task_id'] = $taskID;
        $this->template->content = View::factory('pages/practik')->bind('data', $data);
    }

    public function action_profile() {
        $this->template->styles[]  = 'assets/css/jquery.Jcrop.min.css';
        $this->template->styles[]  = 'assets/css/fineuploader.css';  
        $this->template->scripts[] = 'assets/js/user-profile.js';
        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';
        $this->template->scripts[] = 'assets/js/jquery.Jcrop.min.js';
        $this->template->scripts[] = 'assets/js/jquery.fineuploader.js';        


        $data = Model::factory('user')->get_user(Engine_User_U::uid());
        $this->template->content = View::factory('pages/meta/profile')->bind('data', $data);
    }


    //список сообщений юзеру
    public function action_messagelist() {
        $this->template->scripts[] = 'assets/js/new_message.js';
        $data = Model::factory('message')->_get_new_msgs();
        $this->template->content = View::factory('pages/messageslist')->bind('data', $data);
        
        //add new action                                    
        Engine_User_API::act('msg', "0");
    }

    //переписка с юзером/новое сообщение
    public function action_messages() {
        $id = $this->request->param('id', null);
        $data['curID'] = $id;
        $this->template->scripts[] = 'assets/js/new_message.js';
        $this->template->content = View::factory('pages/messages')->bind('data', $data);
    }

}

// End  Index
