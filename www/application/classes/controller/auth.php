<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Template_Engine_Basic {

    public $template = 'templates/default';

    /**
     * авторизация
     */
    public function action_index() {
        $this->template->title = "Авторизация";
        //вывод формы авторизации
        $err = $this->request->param('id', 0);
        $tr = 'auth-page';
        $this->template->content = View::factory('pages/meta/auth')
                ->bind('error', $err)
                ->bind('authpage',$tr);
    }

    /**
     * ajax-авторизация
     */
    public function action_ajax() {
        $this->template->content = View::factory('pages/auth');
    }

    public function action_logout() {        
        //add new action                                    
        Engine_User_API::act('exit', '0');
        
        Auth::instance()->logout();                        
        Request::initial()->redirect('/');
    }

    //регистрация
    public function action_register() {
        $this->template->scripts[] = 'assets/js/register.js';

        $groups = Model::factory('group')->get_all();

        $this->template->content = View::factory('pages/meta/register')
                ->bind('groups', $groups);
    }

    /**
     * Восстановление пароля: ввод данных
     */
    public function action_passrecovery() {

        if (isset($_POST['send']) AND trim($_POST['password']) != "") {
            $to = Engine_User_Api::clear($_POST);
            $result = Model::factory('user')->rise_password($to['password']);
            $error = $result == '01' ? '1' : null;
            $data = array('part' => 'result', 'error' => $error);
        } else {
            $data = array('part' => 'form');
        }
        $this->template->content = View::factory('pages/meta/password-recovery')->bind('data', $data);
    }

    /**
     * Восстановление пароля: подтверждение
     */
    public function action_recovery() {

        $code = $this->request->param('id');
        $uid = $this->request->param('uid');

        if (isset($code) AND isset($uid)) {
            $result = Model::factory('user')->chack_code($uid, $code);

            $error = $result == 'no data' ? '2' : null;
            $data = array('part' => 'newpassword', 'error' => $error);

            $sess = Session::instance()->set('code', $code);
            $sess->set('uid', $uid);
        } elseif (isset($_POST['pwrdadd'])) {
            extract($_POST);

            if ($password !== $password1) {
                $error = 'passwords';
                $data = array('part' => 'newpassword', 'error' => $error);
            } else {
                $ses = Session::instance();
                $uid = $ses->get('uid');
                $code = $ses->get('code');
                Model::factory('user')->set_password($password, $uid);

                $ses->delete('uid');
                $ses->delete('code');
                $data = array('part' => 'complete');
                ?><script type='text/javascript'>setTimeout(function(){location.replace('/auth/');},5000)</script><?
            }
        }

        $this->template->content = View::factory('pages/meta/password-recovery')->bind('data', $data);
    }

    public function action_forcelogin() {
        $code = $this->request->param('code', false);
        $uid = $this->request->param('id', false);
        $true_code = Engine_User_API::check_forcecode($uid);

        if ($uid . "-" . $code == $true_code) {
            $uname = Model::factory('user')->get_login($uid);
            $auth = Auth::instance()->flogin($uname);
            $url = Model::factory('user')->role_redirect();
                                    
            //add new action                                    
            Engine_User_API::act('login', '0');
            
            $this->request->redirect($url);            
        }else{
            die('error');
        }


        die;
    }

}

// End auth
