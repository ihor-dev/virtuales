<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Test extends Controller_Template_Admin {

    public $title = "Тесты-";

    public function before() {
        parent::before();

        if (!Engine_User_Api::is_admin())
            $this->request->redirect('/');
        
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/admin-test.js';
        
    }

    /**
     * Список тестов
     */
    public function action_index() {
        $this->title .= "главная";
        Session::instance()->delete('test_id');
        $data = Model::factory('test')->get_all_tests();
        $this->template->content = View::factory('pages/admin/test/index')
                    ->bind('data',$data);
    }
    
    /**
     * Создать тест
     */
    public function action_create() {
        $this->title .= "новый тест";

        $this->template->content = View::factory('pages/admin/test/test-create');
    }

       public function action_get() {
          
       }
    /**
     * Просмотр созданного теста
     */
    public function action_view() {
        
        $test_id = Session::instance()->get('test_id');
        if($test_id == null){         
            //обычный просмотр теста со списка тестов
            $test_id = $this->request->param('id');
        }
        $this->title .= "просмотр созданного теста";
        

        $data = Model::factory('test')->get_test($test_id);

        $this->template->content = View::factory('pages/admin/test/test-preview')
                ->bind('data', $data['data'])
                ->bind('name', $data['info']['name'])
                ->bind('test_id', $data['info']['test_id']);
    }

    public function action_continue(){
        $this->title .= "добавить вопрос в тест";
        $id = $this->request->param('id');
        Model::factory('test')->register_test_id($id);
        
        $this->template->content = View::factory('pages/admin/test/test-create');
    }
}

// End Admin
