<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Task extends Controller_Template_Admin {

    public $title = "Админ-задания";

    public function before() {
        parent::before();

        if (!Engine_User_Api::is_admin())
            $this->request->redirect('/');

        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/admin-task.js';
    }

    /**
     * главная админки
     */
    public function action_index() {
        die('chiz');
        $this->title .= "-главная";
        echo Model::factory('task')->test();
        $this->template->content = View::factory('pages/admin/task/index');
    }

}

// End Task
