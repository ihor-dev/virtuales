<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Admin extends Controller_Template_Admin {

    public $title = "Админ-";

    public function before() {
        parent::before();

        if (!Engine_User_Api::is_admin())
            $this->request->redirect('/');
    }

    /**
     * главная админки
     */
    public function action_index() {
        $this->title .= "главная";

        $this->template->content = View::factory('pages/admin/index');
    }

    //-------------ПОЛЬЗОВАТЕЛИ---------------

    /**
     * Список пользователей
     */
    public function action_users() {
        $this->title .= "пользователи";
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/admin-users.js';

        $data     = Model::factory('user')->get_users();
        $new_users = Model::factory('user')->get_new_users();   
        

        $this->template->content = View::factory('pages/admin/users')
                ->bind('data', $data)
                ->bind('new_users', $new_users);
    }



    //-------------Группы----------------------------------
    public function action_groups() {
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/admin-groups.js';        
        $this->template->scripts[] = 'assets/js/jquery-ui.min.js';
        $this->title .= "группы";

        $result = MODEL::factory('group')->get_all();
        
        $users = Model::factory('user')->users_array();
        
        $this->template->content = View::factory('pages/admin/groups')
                ->bind('data', $result)
                ->bind('users',$users);
    }

    //-----------Категории/темы лекций----------------------------------
    public function action_categories() {
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/admin-categories.js';
        $this->title .= "Категории";

        $result = Model_Category::get_cat_names();
        $this->template->content = View::factory('pages/admin/categories')
                ->bind('data', $result);
    }

    //-------------Лекции-------------------
    //готовые лекции
    public function action_lessons() {
        $this->title .= "Список лекций";
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';
        $this->template->scripts[] = 'assets/js/jquery-drag-sort.min.js';
        $this->template->scripts[] = 'assets/js/admin-posts.js';

        $data = Model::factory('post')->get_posts(null, 'lesson');
        $this->template->content = View::factory('pages/admin/posts')
                ->bind('data', $data);
    }

    //создание записи
    public function action_postwrite() {
        $this->template->scripts[] = 'assets/js/tiny_mce/tiny_mce_gzip.js';
        $this->template->scripts[] = 'assets/js/tinyinit.js';
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/gallery.js';
        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';
        
        $this->template->styles[] = 'assets/css/fineuploader.css';       
        $this->template->scripts[] = "assets/js/jquery.fineuploader.js";

        $this->template->scripts[] = 'assets/js/admin-posts.js';

        $this->template->title = "Кабинет-> Создание записи";

        $cat = Model_Category::get_cat_names();

        $this->template->content = View::factory('pages/write-post')
                ->bind('categories', $cat);
    }

    //редактирование записи
    public function action_postedit() {
        $id = $this->request->param('id', 'no');
        if ($id == 'no')
            $this->request->redirect('/admin_lessons');

        $this->template->scripts[] = 'assets/js/tiny_mce/tiny_mce_gzip.js';
        $this->template->scripts[] = 'assets/js/tinyinit.js';
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/gallery.js';

        $this->template->styles[] = 'assets/css/fineuploader.css';       
        $this->template->scripts[] = "assets/js/jquery.fineuploader.js";

        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';

        $this->template->scripts[] = 'assets/js/admin-posts.js';

        $data = Model::factory('post')->get_post($id);
        $cat = Model_Category::get_cat_names();

        $this->template->content = View::factory('pages/write-post')
                ->bind('categories', $cat)
                ->bind('data', $data);
    }

    //просмотр записи
    public function action_view() {
        $id = $this->request->param('id', 0);
        $data = Model::factory('post')->get_post($id);

        $this->template->title       = $data['name'];
        $this->template->content = View::factory('pages/view-post')
                ->bind('data', $data);
    }

    #----------- Настройки ----------

    public function action_settings() {
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/admin-settings.js';

        $this->template->content = View::factory('pages/admin/settings')
                ->bind('data', $data);
    }

    #---------- Задания -------------

    //Новое задание. С подгрузкой через ajax заданий с выбором предмета
    public function action_tasks() {
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';        
        $this->template->scripts[] = 'assets/js/admin-tasks.js';
        
        //новое задание
        $data['predmets'] = Model::factory('predmet')->_all();
        $data['tests']        = Model::factory('test')->get_all_tests();
        $data['lessons']    = Model::factory('post')->get_posts();
        $data['practiks']    = Model::factory('practik')->get_all();
        $data['groups']     = Model::factory('group')->get_all();

        $this->template->content = View::factory('pages/admin/tasks')
                ->bind('data', $data);
    }

//====Практические================
    public function action_practiks() {
        $this->template->scripts[] = 'assets/js/admin-practik.js';
        $this->template->scripts[] = 'assets/js/jquery-drag-sort.min.js';        
        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';

        $data = Model::factory('practik')->get_all();
        $this->template->content = View::factory('pages/admin/practiks')
                ->bind('data', $data);
    }

//СОздать практ
    public function action_practiks_add() {
        $this->template->scripts[] = 'assets/js/tiny_mce/tiny_mce_gzip.js';
        $this->template->scripts[] = 'assets/js/tinyinit.js';
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/gallery.js';
      
        $this->template->styles[]  = 'assets/css/fineuploader.css';       
        $this->template->scripts[] = "assets/js/jquery.fineuploader.js";
        
        $this->template->scripts[] = 'assets/js/admin-practik.js';

        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';

        $this->template->content  = View::factory('pages/admin/practik_edit');
    }

//Редакт практ
    public function action_practiks_edit() {
        $this->template->scripts[] = 'assets/js/tiny_mce/tiny_mce_gzip.js';
        $this->template->scripts[] = 'assets/js/tinyinit.js';
        $this->template->scripts[] = 'assets/js/modal.js';
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $this->template->scripts[] = 'assets/js/gallery.js';

        $this->template->styles[] = 'assets/css/fineuploader.css';       
        $this->template->scripts[] = "assets/js/jquery.fineuploader.js";
        
        $this->template->scripts[] = 'assets/js/admin-practik.js';
        
        $this->template->scripts[] = 'assets/js/jquery.blockUI.js';

        $id = $this->request->param('id');
        //Session::instance()->set('prid',$id);

        $data = Model::factory('practik')->get_practik($id);
        $this->template->content = View::factory('pages/admin/practik_edit')
                ->bind('data', $data);
    }

    # ====== Журнал ========

    public function action_journal() {
        $this->template->scripts[] = 'assets/js/jquery.fixedheadertable.js';
        $this->template->scripts[] = 'assets/js/admin-journal.js';        
        
        $data['groups'] = Model::factory('group')->get_all();
        
        $this->template->content = View::factory('pages/admin/journal')
                ->bind('data', $data);
    }
    
    # ====== Предметы ========
    
    public function action_predmet(){
        $this->template->scripts[] = 'assets/js/admin-predmet.js';
        $this->template->scripts[] = 'assets/js/toolbar.js';
        $data['pr'] = Model::factory('predmet')->_all();
        $data['groups'] = Model::factory('group')->get_all();
        
        $this->template->content = View::factory('pages/admin/predmet')
                ->bind('data', $data);
    }
    
   # ====== Статистика ========    
    public function action_actions(){
        $this->template->scripts[] = 'assets/js/admin-actions.js';        
        
        $data['groups'] = Model::factory('group')->get_all();
        
        $this->template->content = View::factory('pages/admin/actions')
                ->bind('data', $data);
    }
}

// End Admin
