<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Forum extends Controller_Template_User {

    /**
     * Все форумы
     */
    public function action_index() {
        $this->template->scripts[] = 'assets/js/forum.js';
        $this->template->content = View::factory('pages/forum/index')->bind('data', $data);

        $data = Model::factory('forum')->get_all();
         
        //add new action                                    
        Engine_User_API::act('forum', "0");
    }

    //список тем форума
    public function action_view() {
        $this->template->scripts[] = 'assets/js/forum.js';
        $forum_id = $this->request->param('id');
        $res = Model::factory('topic')->get_all($forum_id);
        $this->template->content = View::factory('pages/forum/topics')
                ->bind('data', $res['topics'])
                ->bind('fname', $res['fname'])                
                ->bind('fid', $forum_id);
    }

    //просмотр темы
    public function action_topic() {
        $this->template->scripts[] = 'assets/js/forum.js';
        $topic_id = $this->request->param('id');
        //Если исправлять количество выводимых сообщений - то исправлять также и в вьюхе
        $data = Model::factory('topic')->read($topic_id,"15", "0");
        $topic = Model::factory('topic')->get_info($topic_id);
        $forum_name = Model::factory('forum')->get_name($topic['forum_id']);
        $topic['fname'] = $forum_name;        
        $this->template->content = View::factory('pages/forum/messages')
                ->bind('data', $data)                
                ->bind('tid', $topic_id)
                ->bind('tinfo',$topic);
    }   

}

// End  Index
