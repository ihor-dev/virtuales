<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Test extends Controller_Template_Main {

    public function before() {
        parent::before();
    }

    /**
     * Главная страничка
     */
    public function action_index() {
        $this->template->title = "Информация о тесте";
        $this->template->scripts[] = 'assets/js/test.js';
        
        $task_id = $this->request->param('task', 'error');
        $test_id = $this->request->param('id', 'error');
        $get = $this->request->param('param',0);
        
        if($get == 'get')            
            Model::factory ('taskuser')->sign_task(Engine_User_U::uid(), $task_id);
        
        Model::factory('test')->set_task_id($task_id);

        //Если прямой заходи без указания ид теста или ид задания
        if ($test_id == "error" OR $task_id == "error")
            $this->request->redirect("/tasks/");

        $data['name'] = Model::factory('test')->get_test_info($test_id);
        $data['count'] = Model::factory('test')->count_quetions($test_id);
        $data['id'] = $test_id;
        $this->template->content = View::factory('pages/test/index')
                ->bind('data', $data);
    }

    public function action_run() {
        $this->template->title = "TESTING";
        $this->template->scripts[] = 'assets/js/test.js';
        
        $test_id = $this->request->param('id', 'error');

        //Если прямой заходи без указания ид теста
        if ($test_id == "error")
            $this->request->redirect("/test/");

        Model::factory('test')->register_test_id($test_id);

        $this->template->content = View::factory('pages/test');
    }

}

// End  Index
